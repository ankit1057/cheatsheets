---
tags:
  - Versioning
  - Git
  - Dotfiles
  - Configuration Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Bare Git

This cheat sheet present a simple yet effective way of managing one's configuration files. It is
only based on Git, and it is a very nice setup (IMHO).

!!! Note "Prerequisite(s)"
    * [Git](https://git-scm.com/)

???+ Note "Reference(s)"
    * <https://www.atlassian.com/git/tutorials/dotfiles>
    * <https://github.com/twpayne/chezmoi/blob/master/docs/COMPARISON.md>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [for the first time](#for-the-first-time)
    * [for another computer](#for-another-computer)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

* Initialize a bare git repository, e.g. in `$HOME/.config/bare`:
```console
$ mkdir -p $HOME/.config/bare
$ git init --bare $HOME/.config/bare
$ alias config='/usr/bin/git --git-dir=$HOME/.config/bare/ --work-tree=/'
$ config config --local status.showUntrackedFiles no
```

* Add the alias to your `$HOME/.bashrc` (or `${ZDOTDIR:-${HOME}}/.zshrc` or wherever):
```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
    > ...
    > alias config='/usr/bin/git --git-dir=$HOME/.config/bare/ --work-tree=/'
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
```

* Optionally configure your git user name and email if not already set globally:
```console
$ config config --local user.name "User Name"
$ config config --local user.email "user.mail@mail.mail"
```

* Now you can link your newly created repository to the Git server of your choice (e.g. GitLab,
  GitHub, your own Git server, etc):
```console
$ config remote add origin git@git.server:path.to/config.git
```

---
## Config

### for the first time

Add a file and remove it, just to initialize your repository:
```console
$ touch init
$ config add init
$ config commit -m "initial commit"
$ rm init
$ config add init
$ config commit -m "remove init"
$ config push origin master
```

Create a "transit" branch while the master branch is empty. This branch has to be empty and to stay
empty: this way, on another computer when creating a bare repository and linking it to this one,
you will be able to switch to this branch without overriding any local configuration files. On
another computer this branch will just be a transitory place before creating and switching to the
branch you intend to use:
```console
$ config branch transit
$ config push origin transit
```

Apart from "master" and "transit", you can have a branch per GNU/Linux partition you own. So,
create a branch for the current computer you are using, e.g. for your home desktop, and switch to
it:
```console
$ config branch home-desktop
$ config switch home-desktop
```

Now your can start adding some configuration files, and set the `home-desktop` branch as the
default one:
```console
$ config add $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever (or any other file you want)
$ config commit -m "add bashrc"
$ config push --set-upstream origin home-desktop
```

!!! Warning "Warning"
    **Do not switch to another branch, or your tracked files will be "hidden" by Git!**


### for another computer

On another computer (let's say your home laptop computer), after following the previous [install
section](#install), you can pull the "transit" branch. From there, you can create a dedicated
branch (e.g. `home-laptop`) and switch to it:
```console
$ config pull origin transit
$ config branch home-laptop
$ config switch home-laptop
```

Then your can start adding some configuration files, and set this branch as the default one:
```console
$ config add $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever (or any other file you want)
$ config commit -m "add bashrc"
$ config push --set-upstream origin home-laptop
```

!!! Warning "Warning"
    **Do not switch to another branch, or your tracked files will be "hidden" by Git!**

---
## Use

* You can use this repository like any other git repository, e.g.:
  ```console
  $ config status
  $ config add .vimrc
  $ config commit -m "add vimrc"
  $ config push
  ```

* In the [config section](#config) you noticed that I warned you about not switching to another
  branch. Because you are using a "bare" repository if you switch to another branch then your
  tracked files will be hidden by Git. So, how to check a configuration file of another computer
  on another branch?  You can just do a regular clone (not a "bare" one) of your repository in a
  "mirror" folder:
  ```console
  $ cd $HOME/.config/bare
  $ git clone git@git.server:path.to/config.git mirror
  $ cd mirror
  ```
  In this mirror repository you can switch between branches as much as you want without any
  consequences, you will see your repository as seen from your Git server.

* You can list the currently tracked files like so:
  ```console
  $ cd /
  $ config ls-files
  ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
