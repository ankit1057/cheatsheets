---
tags:
  - Productivity
  - Task Managers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Taskwarrior

Taskwarrior is Free and Open Source Software that manages your TODO list from the command line,
with a multitude of features.

???+ Note "Reference(s)"
    * <https://taskwarrior.org/>
    * <https://github.com/GothenburgBitFactory/taskwarrior>
    * <https://gitlab.com/BlackEdder/caldavwarrior>


---
## Table of content

<!-- vim-markdown-toc GitLab -->

* [Avoid dotfile madness](#avoid-dotfile-madness)
* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Sync with Nextcloud through Caldavwarrior](#sync-with-nextcloud-through-caldavwarrior)

<!-- vim-markdown-toc -->

---
## Avoid dotfile madness

Prior to installation, [make sure you stay in control of your home
directory](https://web.archive.org/web/20210807080152/https://0x46.net/thoughts/2019/02/01/dotfile-madness/).

!!! Warning "Prerequisite(s)"
    * [XDG](../shells/xdg.md)

See [how to handle Taskwarrior related dotfiles](../admin/avoid_dotfile_madness.md#taskwarrior).

---
## Install

!!! Note ""

    === "pacman"
        ```console
        # pacman -S taskwarrior
        ```

    === "from source"
        No Portage package. Taskwarrior has to be build from
        [source](https://github.com/GothenburgBitFactory/taskwarrior)<br/>
        ```console
        $ mkdir -p ~/apps/src-apps
        $ cd ~/apps/src-apps
        $ git clone --recursive https://github.com/GothenburgBitFactory/taskwarrior.git
        $ cd taskwarrior
        $ git checkout 2.6.0 # ckeckout to the latest stable release, e.g. 2.6.0
        $ sed -i 's/git.tasktools.org\/TM/github.com\/GothenburgBitFactory/' .git/config
        $ git submodule update
        $ cmake -DCMAKE_BUILD_TYPE=release .
        $ make
        $ sudo make install
        ```

        !!! Warning "Before building, you need the following prerequisites:"
            * `git`
            * `cmake`
            * `make`
            * `gcc 5.0+`
            * `clang 3.4+`

    === "apt"
        **TODO**
        ```console
        ```

---
## Config

```console
$ task version # allow Taskwarrior to create a sampe taskrc file (yes)
```


---
## Use

WIP

### Sync with Nextcloud through Caldavwarrior

See [`khal`, `khard` and `todoman`](./khal_khard_todoman.md)


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
