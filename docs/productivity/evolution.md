---
tags:
  - Mails
  - Calendar
  - Address Book
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# evolution

## evolution-ews

* <https://sites.utexas.edu/glenmark/2021/02/01/how-to-setup-your-office-365-email-using-evolution-ews-linux/>
* <https://gitlab.gnome.org/GNOME/evolution-ews>


```console
$ sudo pacman -Ss evolution
$ sudo pacman -Ss evolution-ews
$ sudo pacman -Ss gnome-keyring
```

**TODO**


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
