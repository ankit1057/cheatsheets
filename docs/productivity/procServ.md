---
tags:
  - Wrapper
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# procServ

A wrapper to start arbitrary interactive commands in the background, with telnet access to
`stdin`/`stdout`.

???+ Note "Reference(s)"
    * <https://github.com/ralphlange/procServ>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

**TODO**

* Install `procServ` and telnet:
```console
$ sudo yum install procServ
$ sudo yum install telnet
```

!!! Note ""

    === "apk"
        Not available with `apk`.

    === "apt"
        ```console
        # apt install telnet procServ
        ```

    === "dnf"
        ```console
        # dnf install telnet procServ
        ```

    === "emerge"
        Not available with `emerge`.

    === "nix"
        Not available with `nix`.

    === "pacman"
        Not available with `pacman`.

    === "yum"
        ```console
        # yum install telnet procServ
        ```

    === "xbps"
        Not available with `xbps`.

    === "zypper"
        Not available with `zypper`.


---
## Config

**TODO**

---
## Use

**TODO**

* Start `procServ`:
```console
$ procServ -n "foo name ioc" -c /opt/epics/tops/iocBoot/iocfoo/boot -i ^D^C 20000 ../../../bin/linux-x86_64/foo foo.cmd
```

* Find `procServ` PID:
```console
$ pgrep procServ
$ ps -aux | grep "procServ"
```

* Find IOC (running inside `procServ`) PID:
```console
$ pgrep s7cea-apd
$ ps -aux | grep "s7cea-apd"
```

* Restart the IOC (running inside `procServ`) by just killing it: `procServ` will restart it
  automatically:
```console
$ pgrep s7cea-apd
    > 123123
$ sudo kill -9 123123
```

* Connect locally to `procServ`:
```console
$ telnet 127.0.0.1 20000
```

* Quit `procServ` - without stopping the IOC - by entering `Ctrl + AltGr + ]` and then enter
  `quit`.

* Connect remotely to `procServ`, e.g. to a computer (`192.168.1.42`) on the local sub-network of
  `gateway.address` (⚠️ make sure `gateway.address` has `telnet` installed ⚠️):
```console
$ ssh username@gateway.address -t telnet 192.168.1.42 20000
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
