
# GNOME keyring

See <https://wiki.archlinux.org/title/GNOME/Keyring>


## Install

```console
$ sudo pacman -S gnome-keyring
$ sudo pacman -S seahorse # for managing the contents of GNOME Keyring
```

## Config

When using a display manager, the keyring works out of the box for most cases. E.g. GDM, LightDM,
LXDM, and SDDM already have the necessary PAM configuration. For a display manager that does not
automatically unlock the keyring, you will have to edit the appropriate file instead of
`/etc/pam.d/login` as mentioned below.

When using console-based login, edit `/etc/pam.d/login` like so:

```console
$ sudo vi /etc/pam.d/login

    > #%PAM-1.0
    >
    > auth       required     pam_securetty.so
    > auth       requisite    pam_nologin.so
    > auth       include      system-local-login
  + > auth       optional     pam_gnome_keyring.so
    > account    include      system-local-login
    > session    include      system-local-login
  + > session    optional     pam_gnome_keyring.so auto_start
```

---


gnome keyring ne fonctionne pas avec evolution (les mdp sont redemandés non-stop):
```console
$ ps aux | grep keyring
    stephane  1301  0.0  0.0 385008  9004 ?        Sl   14:10   0:00 /usr/bin/gnome-keyring-daemon --daemonize --login
    stephane  1614  0.0  0.0 311024 11776 ?        Sl   14:10   0:00 /usr/bin/gnome-keyring-daemon --start --foreground --components=secrets
    stephane  8942  0.0  0.0   7204  2432 pts/6    S+   14:22   0:00 grep --color=auto keyring
```

mais si je kill tout les daemons keyring et que je ne relance que `gnome-keyring-daemon --daemonize --login`, alors ça fonctionne:
```console
$ pgrep -f "/usr/bin/gnome-keyring-daemon --daemonize --login"
    > 17193
$ pgrep -f "/usr/bin/gnome-keyring-daemon --start --foreground --components=secrets"
    > 16982
$ pkill 17193
$ pkill 16982
$ /usr/bin/gnome-keyring-daemon --daemonize --login
```

Pourtant au démarrage (même après `startx`), seulement `/usr/bin/gnome-keyring-daemon --daemonize
--login` est en marche. Mais: 

- j'ai remarqué que lancer `element-desktop` va kill `/usr/bin/gnome-keyring-daemon --daemonize
  --login`.

- j'ai remarqué que lancer `protonmail-bridge` va lancer `/usr/bin/gnome-keyring-daemon --start
  --foreground --components=secrets` en plus de `/usr/bin/gnome-keyring-daemon --daemonize --login`

- j'ai remarqué que lancer `evolution` va lancer `/usr/bin/gnome-keyring-daemon --start
  --foreground --components=secrets` en plus de `/usr/bin/gnome-keyring-daemon --daemonize --login`
