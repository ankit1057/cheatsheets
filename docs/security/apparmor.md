---
tags:
  - Security
  - Linux Security Modules
  - Mandatory Access Control
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# AppArmor

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/AppArmor>
    * <https://wiki.archlinux.org/index.php/AppArmor>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
