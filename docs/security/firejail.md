---
tags:
  - Security
  - Sandbox Programs
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Firejail

???+ Note "Reference(s)"
    * <https://firejail.wordpress.com/>
    * <https://github.com/netblue30/firejail>
    * <https://wiki.gentoo.org/wiki/Firejail>
    * <https://www.youtube.com/watch?v=0LbEl3oLKUs>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Alternatives](#alternatives)

<!-- vim-markdown-toc -->

**TODO**

---
## Alternatives

* [Bubblewrap](https://wiki.archlinux.org/title/Bubblewrap) (minimal and powerful alternative).


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
