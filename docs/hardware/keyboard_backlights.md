---
tags:
  - Hardware
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Keyboard back lights

???+ Note "Reference(s)"
    * <https://feiticeir0.wordpress.com/2014/11/06/gentoo-fluxbox-asus-keyboard-backlight-control/>
    * <https://wiki.gentoo.org/wiki/ACPI/ThinkPad-special-buttons#Brightness_up>
    * <https://wiki.archlinux.org/index.php/Backlight#Hardware_interfaces>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
