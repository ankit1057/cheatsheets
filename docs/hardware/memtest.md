---
tags:
  - Memory test
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


`memtest86+` : GPL

`memtest86` : Proprietary
* See <https://en.wikipedia.org/wiki/Memtest86>
* See <https://forums.tomshardware.com/threads/memtest86-is-back-new-version-released-after-9-years.3782587/#post-22836218>

`memtester` : GPL
* See <https://github.com/jnavila/memtester>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
