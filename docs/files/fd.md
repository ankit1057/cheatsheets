---
tags:
  - Files
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# fd

fd is a program to find entries in your file system. It is a simple, fast and user-friendly
alternative to `find`. While it does not aim to support all of `find`'s powerful functionality, it
provides sensible (opinionated) defaults for a majority of use cases.

???+ Note "Reference(s)"
    * <https://github.com/sharkdp/fd>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

**TODO**

---
## Config

**TODO**

---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
