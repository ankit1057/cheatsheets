---
tags:
  - Files
  - Sync
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `rsync`

`rsync` is a fast, versatile, remote (and local) file copying tool.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Rsync>
    * <https://wiki.gentoo.org/wiki/Rsync>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Restricted `rsync`](#restricted-rsync)

<!-- vim-markdown-toc -->

---
## Install

Install `rsync`:

!!! Note ""

    === "emerge"
        ```console
        # emerge -a net-misc/rsync
        ```

    === "pacman"
        ```console
        # pacman -S rsync
        ```

    === "apt"
        ```console
        # apt install rsync
        ```

    === "yum"
        ```console
        # apt yum rsync
        ```

    === "dnf"
        ```console
        # apt yum rsync
        ```


---
## Config

See <https://wiki.archlinux.org/index.php/Rsync#Trailing_slash_caveat>.


---
## Use

* Full local backup:
```console
$ sudo rsync -aAXv --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found"} / /path/to/backup/folder --delete --info=progress2
```

* Full remote backup:
```console
$ sudo rsync -aAXv --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found"} -e ssh / user@123.123.123.123:/path/to/backup/folder --delete --info=progress2
```

!!! Tip
    Make sure that the target folder, on the remote system, is owned by "user". If "user" is root,
    make sure to permit root login on the remote system:
    ```console
    # vi /etc/ssh/sshd_config
        > ...
        > PermitRootLogin yes
        > ...
    # systemctl restart sshd
    ```

### Restricted `rsync`

See <https://www.guyrutenberg.com/2014/01/14/restricting-ssh-access-to-rsync/>.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
