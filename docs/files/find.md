---
tags:
  - Files
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Find

`find` is a tool to search for files in a directory hierarchy.

???+ Note "Reference(s)"
    * `$ man find`

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Use](#use)
    * [search based on symlinks](#search-based-on-symlinks)
    * [search based on file or folder name](#search-based-on-file-or-folder-name)
    * [search based on file size](#search-based-on-file-size)
    * [search based on file rights](#search-based-on-file-rights)
    * [search based on file content](#search-based-on-file-content)
    * [search based on folder location](#search-based-on-folder-location)
    * [search based on owner and/or group](#search-based-on-owner-andor-group)
    * [tips and tricks](#tips-and-tricks)

<!-- vim-markdown-toc -->

---
## Use

### search based on symlinks

* Print the list of all the broken symbolic links in the current directory:
    ```console
    $ find . -xtype l
    ```

### search based on file or folder name

* Search files or directories containing `string` in it's name:
    ```console
    $ find ./path/where/to/search -name "*string*"
    ```

* Search files containing `string` in their name (case insensitive):
    ```console
    $ find ./path/where/to/search -type f -iname "*string*"
    ```

* Search files ending with `.test` in their name:
    ```console
    $ find ./path/where/to/search -type f -name "*.test"
    ```

* Search files containing `string` in their name, but not `test`:
    ```console
    $ find ./path/where/to/search -type f -name "*string*" ! -name "*test*"
    ```
    or
    ```console
    $ find ./path/where/to/search -type f -name "*string*" -not -name "*test*"
    ```

* Search files containing `string` or `gnirts` in their name, but not `test` or `tset`:
    ```console
    $ find ./path/where/to/search -type f \( -name "*string*" -o -iname "*gnirts*" \) ! \( -iname "*test*" -o -iname "*tset*" \)
    ```

* Search files containing `string` in their name, but not in the `dir` folder
    ```console
    $ find ./path/where/to/search -type f -name "*string*" -not -path "./dir/*"
    ```

* Search files containing `string` in their name, but not in the `dir` and the `rid` folder
    ```console
    $ find ./path/where/to/search -type f -name "*string*" -not -path "./dir/*" -not -path "./rid/*"
    ```

### search based on file size

* Search files smaller than 2 Mo:
    ```console
    $ find ./path/where/to/search -type f -size -2M
    ```

* Search files bigger than 1 Go:
    ```console
    $ find ./path/where/to/search -type f -size +1G
    ```

* Search files bigger than 1 Ko *and* smaller than 5 Ko:
    ```console
    $ find ./path/where/to/search -type f -size +1k -size -5k
    ```

### search based on file rights

* Search executable files:
    ```console
    $ find ./path/where/to/search -type f -executable
    ```

* Search `644` files:
    ```console
    $ find ./path/where/to/search -type f -perm 644
    ```

* Search files that are not `755` :
    ```console
    $ find ./path/where/to/search -type f -not -perm 755
    ```

### search based on file content

* Search files containing `string` in their name and `plop` in their content:
    ```console
    $ find ./path/where/to/search -name "*string*" -exec grep -H "plop" {} \; # print files name and lines
    $ find ./path/where/to/search -name "*string*" -exec rg -H "plop" {} \; # print files name and lines IN COLOR (with ripgrep)
    ```

### search based on folder location

* Search files containing `string` in their name, not farther than 2 levels of directories below:
    ```console
    $ find ./path/where/to/search -maxdepth 2 type f -name "*string*"
    ```

* Search files containing `string` in their name, at least farther than 2 levels of directories
  below:
    ```console
    $ find ./path/where/to/search -mindepth 2 type f -name "*string*"
    ```

### search based on owner and/or group

* Search files containing `string` in their name, belonging to the group `group-name`:
    ```console
    $ find ./path/where/to/search -group group-name type f -name "*string*"
    ```

* Search files containing `string` in their name, belonging to the user `user-name`:
    ```console
    $ find ./path/where/to/search -user user-name type f -name "*string*"
    ```

### tips and tricks

* Search all the broken symlinks of your system:
    ```console
    # find / -xtype l -print
    ```

!!! Warning " "
    You should not blindly delete all broken symbolic links, as some of them serve a
    [purpose](https://unix.stackexchange.com/questions/151763/is-there-a-downside-to-deleting-all-of-the-broken-symbolic-links-in-a-system).


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
