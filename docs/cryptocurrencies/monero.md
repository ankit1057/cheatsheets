---
tags:
  - Crypto
  - Cryptocurrencies
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Monero

Monero is a private, secure, untraceable, decentralized digital currency.

!!! Warning
    This cheat sheet will focus on the [CLI Monero
    application](https://github.com/monero-project/monero), but nothing stops you from using the
    [GUI one instead](https://github.com/monero-project/monero-gui) and/or to use any other
    [alternative](https://www.getmonero.org/downloads/).

???+ Note "Reference(s)"
    * <https://www.getmonero.org/>
    * <https://www.getmonero.org/resources/moneropedia/>
    * <https://www.getmonero.org/downloads/>
    * <https://forum.getmonero.org/>
    * <https://www.youtube.com/watch?v=qPNxca_KMww>
    * <https://github.com/gentoo-monero/gentoo-monero>
    * <https://localmonero.co/>

---
## table of contents

<!-- vim-markdown-toc GitLab -->

* [install](#install)
* [config](#config)
* [run](#run)
* [misc](#misc)

<!-- vim-markdown-toc -->

---
## install

!!! Note ""

    === "emerge (with overlay)"
        ```console
        # emerge --ask --noreplace eselect-repository
        # eselect repository enable monero
        # emaint sync -r monero
        # echo '*/*::monero ~amd64' >> /etc/portage/package.accept_keywords
        # emerge -a net-p2p/monero
        ```

    === "pacman"
        ```console
        # pacman -S monero
        ```

    === "apt"
        ```console
        # apt install monero
        ```

    === "xbps"
        ```console
        # xbps-install -S monero
        ```

---
## config

Default logs location: `$HOME/.bitmonero`


---
## run

* help:
```console
$ monerod --help
```

* run:
```console
$ monerod
```

* run as a daemon:
```console
$ monerod --detach
```

---
## misc

* list of Monero mining pools: <http://moneropools.com/>

* Monero mining pools statistics: <https://miningpoolstats.stream/monero>

* popular pools, appreciated by the community:

    * <https://xmrvsbeast.com/>
        * payout fee: 0%
        * payout: 0.01 XMR paid automatically (miners cannot adjust this)
        * mining fee: 0%
        * matrix: `#xmrvsbeast:matrix.org`
        * Reddit lounge: <https://www.reddit.com/r/xmrvsbeast/comments/mfqs2u/rxmrvsbeast_lounge/>
        * SubReddit: <https://www.reddit.com/r/xmrvsbeast/top/?t=all>
        * FAQ:
          <https://www.reddit.com/r/xmrvsbeast/comments/ms02zp/faq_please_read_before_posting/>
        * stats: <https://xmrvsbeast.com/stats>
        * alt: <https://beast.provsalt.xyz/> (same as <https://xmrvsbeast.com/provsaltui>)
        * open source: based on <https://github.com/jtgrassie/monero-pool>
        * <https://www.reddit.com/r/MoneroMining/comments/mzojap/algorithm_switching_or_bonus_hashrate/>

    * <https://moneroocean.stream/>
        * PPLNS to determine payouts (helps combat pool hopping and ensures good payout for miners)
        * 0% pool fee
        * withdrawal fee (that became zero after 4.0 XMR)
        * 0.4% - 0.6% exchange fee
        * web mining fee: 3% (payed to web miner software developer)
        * 0.003 XMR minimum payout
        * 30 block confirmation time
        * open source:
            * <https://github.com/MoneroOcean/moneroocean-gui>
            * <https://github.com/MoneroOcean/nodejs-pool>
        * <https://www.reddit.com/r/MoneroMining/comments/mvxcqz/appreciation_for_moneroocean/>
        * <https://www.reddit.com/r/MoneroMining/comments/mzojap/algorithm_switching_or_bonus_hashrate/>

* no pools / solo mining is also a thing:
    * <https://www.reddit.com/r/MoneroMining/comments/el8n5l/i_solo_mined_a_block/>

* popular mining rigs, appreciated by the community:
    * [XMRig](https://github.com/xmrig/xmrig): CPU and GPU (Nvidia and AMD)
        * XMRig without fee: <https://www.youtube.com/watch?v=d1Nfeob7NWU>
    * [MO XMRig](https://github.com/MoneroOcean/xmrig): MoneroOcean's fork of XMRig CPU and GPU
      (Nvidia and AMD)
    * [`XMR-Stak/RX`](https://github.com/fireice-uk/xmr-stak/releases): CPU and GPU (Nvidia and AMD)


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
