---
tags:
  - Games
  - Game Engines
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Amethyst

Amethyst is a data driven and data oriented game engine aiming to be fast and as configurable as
possible.

!!! Note "Prerequisite(s)"
    * A graphic server: in this cheat sheet I will be using [Xorg](../graphical/xorg.md)
    * [rust](../dev/rust/rust.md)

???+ Note "Reference(s)"
    * <https://github.com/amethyst/amethyst>
    * <https://book.amethyst.rs/master/>
    * <https://book.amethyst.rs/v0.12.0/print.html>
    * <https://github.com/amethyst/amethyst/tree/master/examples>
    * <https://github.com/amethyst/amethyst/tree/v0.12.0/examples>
    * <https://github.com/amethyst/amethyst/wiki/Frequently-Asked-Questions>
    * <http://arewegameyet.com/>
    * <https://kyren.github.io/2018/09/14/rustconf-talk.html>
    * <https://www.youtube.com/watch?v=aKLntZcp27M>
    * <https://kyren.github.io/2018/09/14/rustconf-talk.html>
    * <https://github.com/slide-rs/specs>
    * <https://slide-rs.github.io/specs/>
    * <https://wiki.gentoo.org/wiki/Vulkan>
    * <https://wiki.archlinux.org/index.php/Vulkan>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
    * [Test](#test)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

Install the amethyst tools:
```console
$ cargo install amethyst_tools
```

Then install the Vulkan graphics API:

!!! Note ""

    === "emerge"
        ```console
        # emerge -a media-libs/vulkan-loader
        ```

    === "pacman"
        ```console
        $ pacman -S vulkan-icd-loader
        ```

    === "apt"
        ```console
        $ apt install vulkan-loader
        ```

    === "yum"
        ```console
        $ yum install vulkan-loader
        ```

    === "dnf"
        ```console
        $ dnf install vulkan-loader
        ```

### Test

```console
$ git clone https://github.com/amethyst/amethyst.git
$ cd amethyst/example
$ cargo run --example pong --features "vulkan"
```


---
## Config

TODO


---
## Use

Create a new game:
```console
$ amethyst new your-game-name
```

Build the book from amethyst sources:
```console
$ cd path/to/amethyst/book
$ cargo install mdbook
$ mdbook build book
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
