---
tags:
  - System Administration
  - Sysadmin
  - SystemD
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Timers

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Systemd/Timers>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**

```console
$ sudo vi /etc/systemd/system/your.service

    > [Unit]
    > After=network.target
    > Description=Your description
    > Documentation=https://link.to.your.doc
    > Documentation=Other documentation msg
    >
    > [Service]
    > User=root
    > Group=root
    > ExecStart=/bin/bash -c "echo $(date) > /tmp/date.txt"
    >
    > [Install]
    > WantedBy=multi-user.target

$ sudo vi /etc/systemd/system/your.timer

    > [Unit]
    > Description=Your description
    > Documentation=https://link.to.your.doc  
    > Documentation=Other documentation msg
    > 
    > [Timer]
    > OnCalendar=daily
    > Persistent=true
    > #Unit=your.service # by default (link with the .service file)
    > 
    > [Install]
    > WantedBy=timers.target

$ systemctl daemon-reload
$ sudo systemctl enable your.timer
$ sudo systemctl enable your.service
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
