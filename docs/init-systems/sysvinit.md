---
tags:
  - Init Systems
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# SysVinit

SysVinit is a collection of System `V-style` init programs. It includes init, which is run by the
kernel as process 1, and is the parent of all other processes.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/SysVinit>
    * <https://savannah.nongnu.org/projects/sysvinit>
    * <https://wiki.gentoo.org/wiki/Sysvinit>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Services](#services)
    * [Basic service related commands](#basic-service-related-commands)
* [Run levels](#run-levels)

<!-- vim-markdown-toc -->

**TODO**

---
## Services

### Basic service related commands

* Start the `service_name` service:
  ```console
  # service service_name start
  ```

* Stop the `service_name` service:
  ```console
  # service service_name stop
  ```

* Restart the `service_name` service:
  ```console
  # service service_name restart
  ```

* Reload `service_name` configuration without stopping the `service_name` service:
  ```console
  # service service_name reload
  ```

* Check the `service_name` service status:
  ```console
  # service service_name status
  ```

* Enable the `service_name` service on system boot:
  ```console
  # chkconfig service_name on
  ```

* Disable the `service_name` service on system boot:
  ```console
  # chkconfig service_name off
  ```

* Check if the `service_name` service is enable or disable on system boot:
  ```console
  # chkconfig service_name
  ```

---
## Run levels

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
