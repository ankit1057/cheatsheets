---
tags:
  - Init Systems
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# init systems

An init system is the first program, other than the kernel, to be run after a Linux distribution is
booted. It is a daemon process that continues running until the system is shut down. Init is the
direct or indirect ancestor of all other processes, and automatically adopts all orphaned
processes. It is started by the kernel using a hard-coded filename; if the kernel is unable to
start it, panic will result. Init is typically assigned PID 1.


???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Init>
    * <https://wiki.gentoo.org/wiki/Init_system>
    * <https://wiki.gentoo.org/wiki/Comparison_of_init_systems>
    * <https://www.linuxtricks.fr/wiki/rosetta-tableau-comparatif-des-commandes-systemd-sysvinit-openrc>
    * <https://en.wikipedia.org/wiki/Init>
    * <https://wiki.gentoo.org/wiki/Rc>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [rc](#rc)
* [Popular init systems](#popular-init-systems)
* [Quick equivalents](#quick-equivalents)
    * [Basic service related commands](#basic-service-related-commands)
    * [System commands](#system-commands)

<!-- vim-markdown-toc -->

---
## rc

rc is an ancient abbreviation that stands for "run commands". It refers to a script (or a set of
scripts) containing "startup instructions for an application program (or an entire operating
system)". It would seem rc was the basis for modern init systems.

---
## Popular init systems

A lot of init systems are available. Here are maybe the four most popular:

* [SystemD](./systemd.md)
* [OpenRC](./openrc.md)
* [SysVinit](./sysvinit.md)
* [Runit](./runit.md)

---
## Quick equivalents

### Basic service related commands

* Start the `service_name` service:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-service service_name start
        ```

    === "Runnit"
        ```console
        # sv start service_name
        ```
        **or**
        ```console
        # sv up service_name
        ```

    === "SysVinit"
        ```console
        # service service_name start
        ```

    === "SystemD"
        ```console
        # systemctl start service_name
        ```

* Stop the `service_name` service:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-service service_name stop
        ```

    === "Runnit"
        ```console
        # sv stop service_name
        ```
        **or**
        ```console
        # sv down service_name
        ```

    === "SysVinit"
        ```console
        # service service_name stop
        ```

    === "SystemD"
        ```console
        # systemctl stop service_name
        ```

* Restart the `service_name` service:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-service service_name restart
        ```

    === "Runnit"
        ```console
        # sv restart service_name
        ```

    === "SysVinit"
        ```console
        # service service_name restart
        ```

    === "SystemD"
        ```console
        # systemctl restart service_name
        ```

* Reload the `service_name` service:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-service service_name reload
        ```

    === "Runnit"
        ```console
        # sv reload service_name
        ```

    === "SysVinit"
        ```console
        # service service_name reload
        ```

    === "SystemD"
        ```console
        # systemctl reload service_name
        ```

* Check the `service_name` service status:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-service service_name status
        ```

    === "Runnit"
        ```console
        # sv status service_name
        ```

    === "SysVinit"
        ```console
        # service service_name status
        ```

    === "SystemD"
        ```console
        # systemctl status service_name
        ```

* Enable the `service_name` service on system boot:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update add service_name
        ```

    === "Runnit"
        Depending on your `runit` implementation, either run:
        ```console
        # ln -s /etc/runit/sv/service_name /service
        ```
        **or**
        ```console
        # ln -s /etc/runit/sv/service_name /var/service
        ```
        **or**
        ```console
        # ln -s /etc/runit/sv/service_name /run/runit/service
        ```

    === "SysVinit"
        ```console
        # chkconfig service_name on
        ```

    === "SystemD"
        ```console
        # systemctl enable service_name
        ```

* Disable the `service_name` service on system boot:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update del service_name
        ```

    === "Runnit"
        Depending on your `runit` implementation, either run:
        ```console
        # unlink /service/service_name
        ```
        **or**
        ```console
        # unlink /var/service/service_name
        ```
        **or**
        ```console
        # unlink /run/runit/service/service_name
        ```

    === "SysVinit"
        ```console
        # chkconfig service_name off
        ```

    === "SystemD"
        ```console
        # systemctl disable service_name
        ```

* Check if the `service_name` service is enable or disable on system boot:

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update -v | grep service_name
        ```

    === "Runnit"
        ```console
        # service="service_name" && if [ -d /run/runit/service/"$service" ] && [ ! -f /run/runit/service/"$service"/down ]; then echo "$service is enable on boot"; else echo "$service is disable on boot"; fi
        ```

    === "SysVinit"
        ```console
        # chkconfig service_name
        ```

    === "SystemD"
        ```console
        # systemctl is-enabled service_name
        ```

### System commands

**TODO/TO CHECK**

* Halt the system:

!!! Note ""

    === "OpenRC"
        ```console
        # halt
        ```

    === "Runnit"
        ```console
        # halt
        ```

    === "SysVinit"
        ```console
        # halt
        ```

    === "SystemD"
        ```console
        # systemctl halt
        ```

* Power off the system:

!!! Note ""

    === "OpenRC"
        ```console
        # poweroff
        ```

    === "Runnit"
        ```console
        # poweroff
        ```

    === "SysVinit"
        ```console
        # poweroff
        ```

    === "SystemD"
        ```console
        # systemctl poweroff
        ```

* Reboot the system:

!!! Note ""

    === "OpenRC"
        ```console
        # reboot
        ```

    === "Runnit"
        ```console
        # reboot
        ```

    === "SysVinit"
        ```console
        # reboot
        ```

    === "SystemD"
        ```console
        # systemctl reboot
        ```

* Suspend the system: **TODO**

* Hibernate the system: **TODO**

* Print system logs: **TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
