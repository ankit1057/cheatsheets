---
tags:
  - Init Systems
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# OpenRC

OpenRC is a dependency based init system for Unix like systems that maintains compatibility with
the system provided init system, normally located in `/sbin/init`. OpenRC is maintained by the
Gentoo developers and is developed for Gentoo, but it is designed to be used in other Linux
distributions and BSD systems. By default, OpenRC is invoked by SysVinit.

???+ Note "Reference(s)"
    * <https://github.com/OpenRC/openrc>
    * <https://github.com/OpenRC/openrc/blob/master/service-script-guide.md>
    * <https://github.com/OpenRC/openrc/blob/master/user-guide.md>
    * <https://github.com/OpenRC/openrc/blob/master/supervise-daemon-guide.md>
    * <https://www.linuxtricks.fr/wiki/rosetta-tableau-comparatif-des-commandes-systemd-sysvinit-openrc>
    * <https://wiki.gentoo.org/wiki/OpenRC_to_systemd_Cheatsheet>
    * <https://wiki.gentoo.org/wiki/OpenRC>
    * <https://wiki.archlinux.org/index.php/OpenRC>
    * <https://wiki.gentoo.org/wiki/Comparison_of_init_systems>
    * <https://www.youtube.com/watch?v=sftnVwj8DDg>
    * <https://wiki.gentoo.org/wiki/OpenRC_to_systemd_Cheatsheet>
    * <https://wiki.gentoo.org/wiki/OpenRC/supervise-daemon>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Services](#services)
    * [Basic service related commands](#basic-service-related-commands)
* [Run levels](#run-levels)

<!-- vim-markdown-toc -->

**TODO**

---
## Services

### Basic service related commands

* Start the `service_name` service:
  ```console
  # rc-service service_name start
  ```

* Stop the `service_name` service:
  ```console
  # rc-service service_name stop
  ```

* Restart the `service_name` service:
  ```console
  # rc-service service_name restart
  ```

* Reload `service_name` configuration without stopping the `service_name` service:
  ```console
  # rc-service service_name reload
  ```

* Check the `service_name` service status:
  ```console
  # rc-service service_name status
  ```

* Enable the `service_name` service on system boot:
  ```console
  # rc-update add service_name
  ```

* Disable the `service_name` service on system boot:
  ```console
  # rc-update del service_name
  ```

* Check if the `service_name` service is enable or disable on system boot:
  ```console
  # rc-update -v | grep service_name
  ```

---
## Run levels

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
