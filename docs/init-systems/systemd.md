---
tags:
  - Init Systems
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# SystemD

SystemD is a suite of basic building blocks for a Linux system. It provides a system and service
manager that runs as PID 1 and starts the rest of the system.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Systemd>
    * <https://wiki.gentoo.org/wiki/Systemd>
    * <https://github.com/systemd/systemd>
    * <https://systemd.io/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Services](#services)
    * [Basic services control](#basic-services-control)
    * [Inspect services](#inspect-services)
    * [Override services](#override-services)
    * [Analyze services](#analyze-services)
* [Run levels](#run-levels)
* [Tips and tricks](#tips-and-tricks)

<!-- vim-markdown-toc -->

**TODO**

---
## Services

### Basic services control

afficher directement à la fin des logs
```console
$ journalctl -xeu service-name
```

afficher les derniers logs en direct
```console
$ journalctl -xefu service-name
```

editer le contenu d(un service systemd *existant* (très important, pour éviter d'écrire par dessus
les services gérés par le système avec les )

```console
$ systemctl edit service-name
```

afficher le contenu d(un service systemd (incluant aussi les modifications faites via `edit`, qui se retrouveront dans le dossier override.conf)
```console
$ systemctl cat service-name
```

afficher les valeurs de toutes les variables associées à un service :
```console
$ systemctl show service-name
```

afficher les variables d(environnement associées à un service :
```console
$ systemctl show-environment service-name
```

après avoir créé ou modifié un service, il faut penser à lancer :
```console
$ systemctl daemon-reload
```

affiche tous les emplacements de documentation pour trouver différentes informations
```console
$ man 7 systemd.directives
```

affiche les dépendances d'un service
```console
$ systemctl list-dependencies service-name
```

affiche les dépendances inverses d'un service
```console
$ systemctl list-dependencies --reverse service-name
```

affiche les services qui ont pris le plus de temps à s'activer
```console
systemd-analyze blame
```

affiche la chaine critique de démarrage du système, et mets en évidence qui est resté le plus
longtemps sur le chemin critique de démarrage:
```console
systemd-analyze critical-chain
```

créé un fichier .svg qui affiche graphiquement comment sont séquencés les services au démarrage et
en combien de temps:
```console
systemd-analyze plot
```

créé un fichier .svg qui affiche graphiquement les dépendences et reverses dépendances d'un
service:
```console
systemd-analyze dot "service-name.service" | dot  -Tx11
```



* Start the `service_name` service:
  ```console
  # systemctl start service_name
  ```

* Stop the `service_name` service:
  ```console
  # systemctl stop service_name
  ```

* Restart the `service_name` service:
  ```console
  # systemctl restart service_name
  ```

* Reload `service_name` configuration without stopping the `service_name` service:
  ```console
  # systemctl reload service_name
  ```

* Check the `service_name` service status:
  ```console
  # systemctl status service_name
  ```

* Enable the `service_name` service on system boot:
  ```console
  # systemctl enable service_name
  ```

* Disable the `service_name` service on system boot:
  ```console
  # systemctl disable service_name
  ```

* Check if the `service_name` service is enable or disable on system boot:
  ```console
  # systemctl is-enabled service_name
  ```

### Inspect services

* Check if any SystemD services have entered in a failed state:
    ```console
    $ systemctl --failed
    ```

* List SystemD services that have failed:
    ```console
    $ systemctl list-units --state=failed
    ```

### Override services

Let's you want the `ntpd` service to automatically restart if a failure happens. You could
"manually" edit the associated `.service` file (`/usr/lib/systemd/system/ntpd.service`), but this
file could be overritten after a system update or package install. This is where the `systemctl
edit` command is interresting, it will create an associated `override.conf` file that will always
apply:

* Edit the override file (e.g. of `ntpd.service`, located in
  `/etc/systemd/system/ntpd.service.d/override.conf` *and not* in
  `/usr/lib/systemd/system/ntpd.service`) :
    ```console
    $ EDITOR=vi systemctl edit ntpd.service
      + > [Service]
      + > Restart=on-failure
    ```

* Check the syntax e.g. of `/usr/lib/systemd/system/ntpd.service` *and*
  `/etc/systemd/system/ntpd.service.d/override.conf`:
    ```console
    $ systemd-analyze verify ntpd.service
    ```

* Print the full ntpd service (`ntpd.service` *and* `ntpd.servce.d/override.conf`) :
    ```console
    $ systemctl cat ntpd.service
        > # /usr/lib/systemd/system/ntpd.service
        > [Unit]
        > Description=Network Time Service
        > After=syslog.target ntpdate.service sntp.service
        >
        > [Service]
        > Type=forking
        > EnvironmentFile=-/etc/sysconfig/ntpd
        > ExecStart=/usr/sbin/ntpd -u ntp:ntp $OPTIONS
        > PrivateTmp=true
        >
        > [Install]
        > WantedBy=multi-user.target
        >
        > # /etc/systemd/system/ntpd.service.d/override.conf
        > [Service]
        > Restart=on-failure
    ```

### Analyze services

???+ Note "Reference(s)"
    - <https://man.archlinux.org/man/systemd-analyze.1>

* Create an SVG graphic detailing which system services have been started at what time,
  highlighting the time they spent on initialization:
    ```console
    $ systemd-analyze plot > systemd_analyze_plot.svg
    ```

---
## Run levels

**TODO**


---
## Tips and tricks

* Start system in a non-graphical mode:
    ```console
    $ systemctl get-default
        > graphical.target

    $ sudo systemctl set-default multi-user.target
    ```

* Check systemd `journal` for a service:
    ```console
    $ sudo journalctl -xeu service-name
    ```

    `-x, --catalog`:
        Augment log lines with explanation texts from the message catalog. This will add
        explanatory help texts to log messages in the output where this is available. These short
        help texts will explain the context of an error or log event, possible solutions, as well
        as pointers to support forums, developer documentation, and any other relevant manuals.

    `-e, --pager-end`
        Immediately jump to the end of the journal inside the implied pager tool.

    `-u, --unit=UNIT|PATTERN`
        Show messages for the specified systemd unit UNIT (such as a service unit), or for any of
        the units matched by PATTERN.


* Check systemd `journal` for every services:
    ```console
    $ sudo journalctl -xb -p 3
    ```

    `-x, --catalog`
        Augment log lines with explanation texts from the message catalog. This will add
        explanatory help texts to log messages in the output where this is available.

    `-b [[ID][±offset]|all], --boot[=[ID][±offset]|all]`
        Show messages from a specific boot. This will add a match for "_BOOT_ID=". The argument may
        be empty, in which case logs for the current boot will be shown.

    `-p 3, --priority=`
        Filter output by message priorities or priority ranges. Takes either a single numeric or
        textual log level (i.e. between 0 "emerg" and 7 "debug").

        The log levels are the usual syslog log levels as documented in syslog, i.e.:
        - "emerg" (0)
        - "alert" (1)
        - "crit" (2)
        - "err" (3),
        - "warning" (4)
        - "notice" (5)
        - "info" (6)
        - "debug" (7)

        If a single log level is specified, all messages with this log level or a lower (hence more
        important) log level are shown. If a range is specified, all messages within the range are
        shown.

---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
