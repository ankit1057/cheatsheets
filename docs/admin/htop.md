---
tags:
  - System Administration
  - Sysadmin
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# htop

htop is a cross-platform interactive process viewer.

htop allows scrolling the list of processes vertically and horizontally to see their full command
lines and related information like memory and CPU consumption. Also system wide information, like
load average or swap usage, is shown.

???+ Note "Reference(s)"
    * <https://github.com/htop-dev/htop>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

**TODO**

---
## Config

**TODO**

---
## Use

**TODO**

* Enter `?` in `htop` for a detailed shortcut list.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
