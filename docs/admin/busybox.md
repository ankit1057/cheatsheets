---
tags:
  - System Administration
  - Sysadmin
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# BusyBox

**TODO**

???+ Note "Reference(s)"
    * <https://linuxhandbook.com/what-is-busybox/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

Install BusyBox on your GNU/Linux machine in order to test its programs:

!!! Note ""

    === "emerge"
        ```console
        # emerge -a sys-apps/busybox
        ```

    === "pacman"
        ```console
        # pacman -S busybox
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.busybox
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.busybox
            ```

    === "apt"
        ```console
        # apt install busybox
        ```

    === "yum"
        ```console
        # yum install busybox
        ```

    === "dnf"
        ```console
        # dnf install busybox
        ```

---
## Use

**TODO**

* Run a BusyBox program (e.g. `top`):
```console
$ busybox top
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
