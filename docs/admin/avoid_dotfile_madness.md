---
tags:
  - System Administration
  - Sysadmin
  - Dotfiles
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Avoid dotfile madness

Look at how many dotfiles you have: `$ ls -d ~/.* | wc -l`.

Now [try to take back control of your home
directory](https://web.archive.org/web/20210807080152/https://0x46.net/thoughts/2019/02/01/dotfile-madness/).

!!! Note "Prerequisite(s)"
    * [XDG](../shells/xdg.md)

???+ Note "Reference(s)"
    * <https://web.archive.org/web/20210807080152/https://0x46.net/thoughts/2019/02/01/dotfile-madness/>
    * <https://0x46.net/thoughts/2019/02/01/dotfile-madness/>
    * <https://wiki.archlinux.org/index.php/XDG_user_directories>
    * <https://wiki.archlinux.org/title/XDG_Base_Directory>
    * <https://wiki.debian.org/DotFilesList>
    * <https://specifications.freedesktop.org/basedir-spec/latest/>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Dotfile madness remover](#dotfile-madness-remover)
* [ALSA (`~/.asoundrc`)](#alsa-asoundrc)
* [Conda (`~/.conda`)](#conda-conda)
* [Eclipse (`~/.eclipse`, `~/.p2`, `~/.swt`, `~/.tooling`)](#eclipse-eclipse-p2-swt-tooling)
    * [Reinstalling Eclipse](#reinstalling-eclipse)
    * [Not reinstalling Eclipse](#not-reinstalling-eclipse)
* [`fasd` (`~/.fasd`)](#fasd-fasd)
* [Gimp (`~/.gimp`)](#gimp-gimp)
* [Git (`~/.gitconfig`)](#git-gitconfig)
* [`gconf` (`.gconf`)](#gconf-gconf)
* [Gem (`~/.gem`)](#gem-gem)
* [GnuPG (`~/.gnupg`)](#gnupg-gnupg)
* [Go (`~/go`)](#go-go)
* [ICEauthority (`~/.ICEauthority`)](#iceauthority-iceauthority)
* [Java OpenJDK (`~/.java/fonts`, `~/.java/.userPrefs`)](#java-openjdk-javafonts-javauserprefs)
* [`less` (`~/.lesshst`)](#less-lesshst)
* [Maven (`~/.m2`)](#maven-m2)
* [`ncmpcpp` (`~/.ncmpcpp`, `~/.lyrics`)](#ncmpcpp-ncmpcpp-lyrics)
* [Npm and Node.js (`~/.npm`, `~/.npmrc`, `~/.node_repl_history`)](#npm-and-nodejs-npm-npmrc-node_repl_history)
* [Nvidia cache (`.nv`)](#nvidia-cache-nv)
* [PulseAudio (`~/.pulse-cokie`)](#pulseaudio-pulse-cokie)
* [`pylint` (`~/.pylint.d`)](#pylint-pylintd)
* [Rust (`~/.cargo`)](#rust-cargo)
* [Subversion (`~/.subbversion`)](#subversion-subbversion)
* [Taskwarrior](#taskwarrior)
* [Vagrant (`~/.vagrant.d`)](#vagrant-vagrantd)
* [WeeChat (`~/.weechat`)](#weechat-weechat)
* [`wget` (`~/.wget-hsts`)](#wget-wget-hsts)
* [`whohas` (`~/.whohas`)](#whohas-whohas)
* [Xorg (`~/.xscreensaver`, `~/.xsession-errors`, `~/.xsession-errors.old`)](#xorg-xscreensaver-xsession-errors-xsession-errorsold)
* [Xorg deprecated (`~/.Xauthority` and `~/.serverauth`)](#xorg-deprecated-xauthority-and-serverauth)
* [Zsh (`~/.zlogout`, `~/.zlogin`, `~/.zcompdump`, `~/.zcompdump.zwc`, `~/.zimrc`, `~/.zprofile`, `~/.zshrc.zwc.old`, `~/.zshrc.zwc`, `~/.zshrc`, `~/.zhistory`)](#zsh-zlogout-zlogin-zcompdump-zcompdumpzwc-zimrc-zprofile-zshrczwcold-zshrczwc-zshrc-zhistory)

<!-- vim-markdown-toc -->

---
## Dotfile madness remover

**TODO**: create a script to remove bad dotfiles, like this one:

* <https://github.com/lahwaacz/Scripts/blob/master/rmshit.py>


---
## ALSA (`~/.asoundrc`)

No clear solution here, but one workaround: copy the content of your `~/.asoundrc` in
`/etc/asound.conf`, this make the `.asoundrc` configuration all system wild but no one can delete
it.

**UPDATE**: It should be OK now with `XDG_CONFIG_HOME/alsa/asoundrc` (see
<https://github.com/alsa-project/alsa-lib/issues/49>).

---
## Conda (`~/.conda`)

```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
    > ...
  + > # CONDA
  + > export CONDA_ROOT=/opt/conda
  + > export CONDA_ENVS_PATH=/opt/conda/envs
  + > export CONDA_PKGS_DIRS=/opt/conda/pkgs
  + > export CONDARC="${XDG_CONFIG_HOME:-${HOME/.config}}/conda/condarc"
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever

$ sudo mkdir /opt/conda
$ sudo chown your-username:your-username /opt/conda
```

---
## Eclipse (`~/.eclipse`, `~/.p2`, `~/.swt`, `~/.tooling`)

The easiest way to get rid of all those dotfiles is to reinstall Eclipse with proper settings.
Alternatively, I also described how to do it without reinstalling Eclipse, but this is more tedious
and more a workaround than a real alternative.

### Reinstalling Eclipse

See
<https://stackoverflow.com/questions/37864572/using-different-location-for-eclipses-p2-tree-bundle-pool>.

Open the Eclipse installer -> press the menu bottom on the top (1), and then select Bundle pools

**TODO**

### Not reinstalling Eclipse

After an Eclipse install with the official Eclipse installer, you might end up with
`$HOME/.eclipse`, `$HOME/.p2`, `$HOME/.swt` and `$HOME/.tooling` dotfiles. Here is how to move
everything to `$HOME/.config/eclipse`:

(NOTE: this method might not work if you installed Eclipse with your package manager)
```console
$ mkdir -p ~/.config/eclipse
$ mkdir -p ~/projects/eclipse-workspaces/default-workspace
$ mv ~/.eclipse ~/.config/eclipse/
$ mv ~/.p2 ~/.config/eclipse/
$ mv ~/.swt ~/.config/eclipse/
$ mv ~/.tooling ~/.config/eclipse/
$ mv ~/artifacts.xml ~/.config/eclipse

$ cd /path/to/eclipse/installation/folder

$ vi eclipse.ini
    > ...
    > --launcher.library
  ~ > /home/user/.config/eclipse/.p2/pool/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.1200.v20200508-1552 ...
    > -showsplash
  ~ > /home/user/.config/eclipse/.p2/pool/plugins/org.eclipse.epp.package.common_4.16.0.20200615-1200
    > ...
    > -vmargs
  + > -Duser.home=/home/user/.config/eclipse/
    > ...
  ~ > -Dosgi.instance.area.default=/home/user/projects/eclipse-workspaces/default-workspace
    > #-Dosgi.configuration.area=@user.home/
    > ...

$ vi configuration/config.ini
  + > user.home=/home/user/.config/eclipse
    > ...
  ~ > osgi.instance.area.default=/home/user/projects/eclipse-workspaces/default-workspace
  ~ > osgi.framework=file\:/home/user/.config/eclipse/.p2/pool/plugins/org.eclipse.osgi_3.15.300.v20200520-1959.jar
    > ...
  ~ > osgi.splashPath=file\:/home/user/.config/eclipse/.p2/pool/plugins/org.eclipse.platform_4.16.0.v20200604-0540
    > ...
  ~ > eclipse.p2.data.area=file\:/home/user/.config/eclipse/.p2/
    > ...

$ vi configuration/org.eclipse.equinox.simpleconfigurator/bundles.info
    > # In this file replace every occurence of "*/user/.p2/*" by "*/user/.config/eclipse/.p2*"

$ grep "user/.p2" # find the last file(s) with bad .p2 configuration
$ vi configuration/org.eclipse.osgi/631/data/-621110115/content.xml
    > ...
  ~ > <repository name='update site: file:/home/user/.config/eclipse/.p2/pool.eclipseextension' type='org.eclipse.equinox.internal.p2.metadata.repository.LocalMetadataRepository' version='1'>
    > ...
```

But sadly, the `$HOME/.p2` dotfile will reappear after Eclipse execution... It can be safely
deleted though!

A workaround would be to launch Eclipse like this:
```console
$ ./path/to/eclipse/installation/folder/eclipse && rm -rf ~/.p2
```
(NOTE: you can alias this in your `$HOME/.bashrc` or `${ZDOTDIR:-${HOME}}/.zshrc` or wherever)


---
## `fasd` (`~/.fasd`)

* Define a `$_FASD_DATA`:
```console
$ mkdir -p $XDG_CONFIG_HOME/fasd
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
    > export _FASD_DATA="$XDG_CONFIG_HOME/fasd/fasd"
    > ...
$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
```

* Eventually move the `fasd` dotfile (if any) into `$_FASD_DATA`:
```console
$ mv $HOME/.fasd $_FASD_DATA
```

---
## Gimp (`~/.gimp`)

* Avoid the `$HOME/.gimp` dotfile with:
```console
$ mkdir $XDG_CONFIG_HOME/gimp
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
  + >
  + > # GIMP
  + > export GIMP2_DIRECTORY="$XDG_CONFIG_HOME/gimp"
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
```

* Eventually move the `$HOME/.gimp` dotfile (if any) into `$XDG_CONFIG_HOME/git/config`:
```console
$ mv ~/.gimp-x.x $XDG_CONFIG_HOME/gimp/
```

---
## Git (`~/.gitconfig`)

* Avoid the `$HOME/.gitconfig` dotfile simply with:
```console
$ mkdir -p ${XDG_CONFIG_HOME:-${HOME}}/git
```

* Eventually move the `$HOME/.gitconfig` dotfile (if any) to `$XDG_CONFIG_HOME/git/config`:
```console
$ mv ~/.gitconfig $XDG_CONFIG_HOME/git/config
```

---
## `gconf` (`.gconf`)

**TODO**

See <https://web.archive.org/web/20171207094430/https://projects.gnome.org/gconf/>.

```console
$ vi /etc/gconf/2/path
    >
```

---
## Gem (`~/.gem`)

```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
    > export GEM_HOME="$XDG_DATA_HOME"/gem
    > export GEM_SPEC_CACHE="$XDG_CACHE_HOME"/gem
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
```
(NOTE: also, make sure to remove gem: `--user-install` from `/etc/gemrc` if any)

---
## GnuPG (`~/.gnupg`)

* Avoid the `$HOME/.gnupg` dotfile with:
```console
$ mkdir -p $XDG_CONFIG_HOME/gnupg
$ chmod go-rwx $XDG_CONFIG_HOME/gnupg
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
  + >
  + > # GPG
  + > export GNUPGHOME="$XDG_CONFIG_HOME/gnupg"
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
```

* Eventually move the `$HOME/.gnupg` dotfile (if any) to `$XDG_CONFIG_HOME/gnupg`:
```console
$ mv $HOME/.gnupg $XDG_CONFIG_HOME/gnupg
```

* Reload:
```console
$ echo RELOADAGENT | gpg-connect-agent
```

---
## Go (`~/go`)

```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
  + >
  + > # GO
  + > export GOPATH="$XDG_DATA_HOME/go"
  + > export GOMODCACHE="$XDG_CACHE_HOME/go/mod"
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
$ mv $HOME/go ${XDG_DATA_HOME:-${HOME/.local/share}}
```

---
## ICEauthority (`~/.ICEauthority`)

**WIP** (actually not working)

* Avoid the `$HOME/.ICEauthority` dotfile with:
```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
    > export ICEAUTHORITY=$XDG_CONFIG_HOME/ICEauthority
    > ...
$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
```

* Eventually move the `$HOME/.ICEauthority` dotfile (if any) to `$XDG_CONFIG_HOME/ICEauthority`:
```console
$ mv $HOME/.ICEauthority $XDG_CONFIG_HOME/ICEauthority
```

---
## Java OpenJDK (`~/.java/fonts`, `~/.java/.userPrefs`)

```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
  + >
  + > # JAVA
  + > export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
$ mv $HOME/.java $XDG_CONFIG_HOME/java
```

---
## `less` (`~/.lesshst`)

In order to avoid the `~/.lesshst` dotfile:
```console
$ mkdir $XDG_CONFIG_HOME/less
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
  + >
  + > # LESS
  + > export LESSHISTFILE="$XDG_CONFIG_HOME/less/lesshst"
  + > export LESSKEY="$XDG_CONFIG_HOME/less/keys"
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever

$ mv $HOME/.lesshst $XDG_CONFIG_HOME/less/lesshst
$ mv $HOME/.keys $XDG_CONFIG_HOME/less/keys
```

---
## Maven (`~/.m2`)

> See <https://maven.apache.org/settings.html#Simple_Values>

```console
$ mkdir -p $XDG_CONFIG_HOME/maven
$ cd $XDG_CONFIG_HOME/maven
$ vi settings.xml
  + > <settings>
  + >   <localRepository>${user.home}/.config/maven/.m2/repository</localRepository>
  + > </settings>

$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
    > ...
  + >
  + > # mvn
  + > #
  + > # prerequisite:
  + > #   * subversion: <https://repology.org/project/maven/versions>
  + > alias mvn="mvn -gs "$XDG_CONFIG_HOME"/maven/settings.xml"
    > ...
    
$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
```

---
## `ncmpcpp` (`~/.ncmpcpp`, `~/.lyrics`)

In order to avoid the `~/.ncmpcpp` and `~/.lyrics` dotfiles:
```console
$ mkdir $XDG_CONFIG_HOME/ncmpcpp
$ mkdir $XDG_CONFIG_HOME/ncmpcpp_lyrics
```
Then copy the content of `/usr/shar/doc/ncmpcpp-x.x.x/config.bz2` in
`$XDG_CONFIG_HOME/ncmpcpp/config`

Replace `#ncmpcpp_directory = ~/.ncmpcpp` by `ncmpcpp_directory = ~/.config/ncmpcpp`

Replace `#lyrics_directory = ~/.lyrics` by `lyrics_directory = ~/.config/ncmpcpp_lyrics`

---
## Npm and Node.js (`~/.npm`, `~/.npmrc`, `~/.node_repl_history`)

```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > # nodejs
    > export NODE_REPL_HISTORY="$XDG_DATA_HOME"/node_repl_history
    >
    > # npm
    > export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
```

```console
$ vi $XDG_CONFIG_HOME/npm/npmrc
    > prefix=${XDG_DATA_HOME}/npm
    > cache=${XDG_CACHE_HOME}/npm
    > tmp=${XDG_RUNTIME_DIR}/npm
    > init-module=${XDG_CONFIG_HOME}/npm/config/npm-init.js
```

!!! Warning "Warning"
    `prefix` is unnecessary (and unsupported) if Node.js is installed by
    [`nvm`](https://github.com/nvm-sh/nvm).

!!! Tip "TIP"
    If you think you will need to use multiple versions of Node.js, then you might consider using
    [`nvm`](https://github.com/nvm-sh/nvm).

!!! Tip "Tip"
    If you want to configure this system-wide, the file to edit is `/usr/etc/npmrc`, not
    `/etc/npmrc`. You can confirm that the config is loaded by running `$ npm config list`.

---
## Nvidia cache (`.nv`)

After setting up `$XDG_CACHE_HOME`, run `$ glxgears` now the `.nv` folder should appear in `.cache`
(or whatever is `$XDG_CACHE_HOME`) so you can remove the `.nv` folder in your `$HOME` folder.


---
## PulseAudio (`~/.pulse-cokie`)

In order to avoid the `~/.pulse-cokie` dotfile:
```console
$ mkdir $XDG_CONFIG_HOME/pulse
$ cp /etc/pulse/client.conf $XDG_CONFIG_HOME/pulse/
$ vi $XDG_CONFIG_HOME/pulse/client.conf
    > ...
    > cookie-file = /tmp/pulse-cookie
    > ...
```

See `$ man pusle-client.conf` for more details.

---
## `pylint` (`~/.pylint.d`)

```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
    > # PYTON PYLINT
    > export PYLINTHOME="$XDG_CACHE_HOME"/pylint
    > export PYLINTHOME="${XDG_CACHE_HOME:-${HOME/.cache}}"/pylint
    > ...

$ mv ~/.pylint.d ~/.cache/pylint
$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
```

---
## Rust (`~/.cargo`)

```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
  + >
  + > # RUST
  + > export CARGO_HOME="${XDG_DATA_HOME:-${HOME/.local/share}}/cargo"
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
$ mv $HOME/.cargo ${XDG_DATA_HOME:-${HOME/.local/share}}/cargo
```

---
## Subversion (`~/.subbversion`)

Workaround:
```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
    > ...
    > alias svn="svn --config-dir "$XDG_CONFIG_HOME"/subversion"
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
```

---
## Taskwarrior

```console
$ mkdir -p $XDG_CONFIG_HOME/taskwarrior
$ mkdir -p ${XDG_DATA_HOME:-${HOME/.local/share}}/task
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
  + >
  + > # TASKWARRIOR
  + > export TASKRC="$XDG_CONFIG_HOME/taskwarrior/taskrc"
  + > export TASKDATA="${XDG_DATA_HOME:-${HOME/.local/share}}/task"
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
```

---
## Vagrant (`~/.vagrant.d`)

* Avoid the `~/.vagrant.d` dotfile, change his location e.g. to `~/vms/vagrant.d`:
```console
$ mkdir -p $HOME/vms/vagrant.d
$ mv $HOME/.vagrant.d $HOME/vms/vagrant.d
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
  + >
  + > # VAGRANT
  + > export VAGRANT_HOME="$HOME/vms/vagrant.d"
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
```

---
## WeeChat (`~/.weechat`)

* Since `v3.2`, WeeChat should follow the [XDG Base Directory
  Specifications](https://specifications.freedesktop.org/basedir-spec/latest/) (see
  <https://specs.weechat.org/specs/001285-follow-xdg-base-dir-spec.html>), so applying the
  [XDG](../shells/xdg.md) cheat sheet should be enough.

* Before `v3.2`, just export `$WEECHAT_HOME` like below:
    ```console
    $ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
        > ...
        > # WEECHAT
        > export WEECHAT_HOME="$XDG_DATA_HOME"/weechat
    
    $ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    ```

---
## `wget` (`~/.wget-hsts`)

**WIP** (still in progress)

In order to avoid the `~/.wget-hsts` dotfile:
```console
$ mkdir $XDG_CONFIG_HOME/wget
$ cp /etc/wgetrc $XDG_CONFIG_HOME/wget/
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
  + >
  + > # WGET
  + > export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
    > ...

$ source ${XDG_CONFIG_HOME:-${HOME}}/.zshenv

$ vi $XDG_CONFIG_HOME/wget/wgetrc
    > ...
    > # ?
    > ...
```

Considering that I haven't found any options to solve this problem from the `wgetrc` file, here is
a workaround:
```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
    > ...
    > alias wget="wget --hsts-file=$XDG_CONFIG_HOME/wget/wget-hsts"
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever

$ mv $HOME/.wget-hsts $XDG_CONFIG_HOME/wget/
```

---
## `whohas` (`~/.whohas`)

When using the `$ whohas` command, a `~/.whohas` dotfile appears to cache package lists for some
distributions.

Workaround:
```console
$ vi ~/.bash_profile # or ${ZDOTDIR:-${HOME}}/.zlogin or whatever
    > ...
    > rm -rf ~/.whohas
    > ...
```

**TODO**
Better workaround (to test)?
```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
    > ...
    > alias whohas="whohas && rm -rf ${XDG_CONFIG_HOME:-${HOME}}/.whohas"
    > ...
$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
```

---
## Xorg (`~/.xscreensaver`, `~/.xsession-errors`, `~/.xsession-errors.old`)

**WIP** (no "real" solution found yet)

```console
$ vi $HOME/.bash_profile # or ${ZDOTDIR:-${HOME}}/.zprofile or wherever
    > ...
    > rm ~/.xscreensaver
    > rm ~/.xsession-errors
    > rm ~/.xsession-errors.old
    > ...
```

---
## Xorg deprecated (`~/.Xauthority` and `~/.serverauth`)

**TODO**

**DEPRECATED**, don't do this or you might mess with X11 forwarding.

```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
  + >
  + > # XORG
  + > export XAUTHORITY="$XDG_CONFIG_HOME/Xauthority"
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
$ mv $HOME/.Xauthority $XDG_CONFIG_HOME/Xauthority
```

And
```console
# vi /usr/bin/startx
```
Replace `xserverauthfile=$HOME/.serverauth.$$` by `xserverauthfile=$XAUTHORITY`, see
<http://www.shallowsky.com/blog/linux/serverauth.html>

---
## Zsh (`~/.zlogout`, `~/.zlogin`, `~/.zcompdump`, `~/.zcompdump.zwc`, `~/.zimrc`, `~/.zprofile`, `~/.zshrc.zwc.old`, `~/.zshrc.zwc`, `~/.zshrc`, `~/.zhistory`)

* Define a `$ZDOTDIR`:
```console
$ mkdir $XDG_CONFIG_HOME/zdotdir
$ vi $HOME/.zshenv
    > ...
  + >
  + > # ZDOTDIR
  + > export ZDOTDIR="$XDG_CONFIG_HOME/zdotdir"
    > ...
$ source $HOME/.zshenv
```

* Eventually move any Zsh dotfiles (if any) into `$ZDOTDIR`:
```console
$ mv ~/.zlogout ~/.zlogin ~/.zcompdump ~/.zcompdump.zwc ~/.zimrc ~/.zprofile ~/.zshrc.zwc.old ~/.zshrc.zwc ~/.zshrc ~/.zhistory $ZDOTDIR
```

* Symbolic link `$HOME/.zshenv`:
```console
$ ln -s $HOME/.zshenv $ZDOTDIR/.zshenv
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
