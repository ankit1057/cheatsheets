---
tags:
  - System Administration
  - Sysadmin
  - Processes Commands
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `ps`

`ps` (process status) is a program that displays the currently running processes. A related Unix
utility named `top` provides a real-time view of the running processes.


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Use](#use)

<!-- vim-markdown-toc -->

---
## Use

???+ Note "Reference(s)"
    * <https://www.binarytides.com/linux-ps-command/>
    * <https://www.geeksforgeeks.org/ps-command-in-linux-with-examples/>

* Print all processes:
```console
$ ps -e
```

* Print all processes with more details (additional columns):
```console
$ ps -ef | less
```

* Print processes with custom columns (see "STANDARD FORMAT SPECIFIERS" section of `$ man ps` for
  more column header to display):
```console
$ ps ax -o pid,uname,pcpu,pmem,comm
```

* Print processes owned by a user:
```console
$ ps -u user-name
```

* Print processes by "exact" name, an "exact" process name is needed (no partial name or wildcard),
  e.g. `sshd`:
```console
$ ps -f -C sshd
```

* Print processes by "partial" name, to search the process list more flexibly:
```console
$ ps -ef | grep ssh
```

* Print processes by process id:
```console
$ ps -f -p 1036,1569,11006
```

* Print the 10 processes that have the biggest CPU usage:
```console
$ ps -ef --sort=-pcpu | head -10
```

* Print processes by memory usage:
```console
$ ps -ef --sort=-pmem
```

* Print processes hierarchy in a tree style:
```console
$ ps -ef --forest
```

* Print the threads of a process, e.g. threads of process id 3150:
```console
$ ps -p 3150 -L
```

* Print all processes owned by you:
```console
$ ps -x
```

* Print all processes associated with current terminal:
```console
$ ps -T
```

* Print all processes associated with a terminal:
```console
$ ps -a
```

* Print all processes NOT associated with a terminal (negation command):
```console
$ ps -a -N
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
