---
tags:
  - Logs
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# System loggers

Here are the most used system loggers:

* [`rsyslog`](./rsyslog.md)
* [`syslog-ng`](./syslog-ng.md)
* [`sysklogd`](./sysklogd.md)
* [`metalog`](./metalog.md)
* [`journal` (SystemD)](./journal.md)


???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Category:Logging>
    * <https://wiki.gentoo.org/wiki/Security_Handbook/Logging>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Syslog ("historical" parenthesis)](#syslog-historical-parenthesis)

<!-- vim-markdown-toc -->


---
## Syslog ("historical" parenthesis)

Syslog was a tool to manage systems logs, which has been adopted by other applications and has
since become the standard logging solution on Unix like systems. A variety of implementations also
exist on other operating systems and it is commonly found in network devices, such as routers.

Syslog is no longer used now, but it's specification is the standard. Many implementations existed,
some of which were incompatible. The Internet Engineering Task Force documented the status quo in
RFC 3164. It was standardized by RFC 5424.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
