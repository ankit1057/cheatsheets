---
tags:
  - Logs
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `logrotate`

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Logrotate>
    * <https://wiki.gentoo.org/wiki/Logrotate>
    * <https://github.com/logrotate/logrotate>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
