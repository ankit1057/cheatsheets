---
tags:
  - Networks
  - Firewalls
  - Security
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `iptables`

`iptables` is a user-space utility program that allows a system administrator to configure the IP
packet filter rules of the Linux kernel firewall. The filters are organized in different tables,
which contain chains of rules for how to treat network traffic packets. Different kernel modules
and programs are currently used for different protocols: `iptables` applies to IPv4, `ip6tables` to
IPv6, `arptables` to ARP, and `ebtables` to Ethernet frames.

???+ Note "Reference(s)"
    * <https://www.tecmint.com/basic-guide-on-iptables-linux-firewall-tips-commands/>
    * <https://www.tecmint.com/linux-iptables-firewall-rules-examples-commands/>
    * <https://fedoraproject.org/wiki/How_to_edit_iptables_rules>
    * <https://wiki.gentoo.org/wiki/Iptables>
    * <https://wiki.archlinux.org/title/Iptables>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**

```console
$ curl cheat.sh/iptables
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
