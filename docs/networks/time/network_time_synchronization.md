---
tags:
  - Networks
  - Timing
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Network time synchronization

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Category:Network_Time_Protocol>
    * <https://wiki.gentoo.org/wiki/Ntp>
    * <https://en.wikipedia.org/wiki/Network_Time_Protocol>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Use](#use)

<!-- vim-markdown-toc -->

---
## Use

Main network time synchronization projects (and associated cheat sheets):

* [`Network Time Protocol` (`ntp`) project](./ntp.md)
* [`systemd-timesyncd`](./systemd_timesyncd.md)
* [`Chrony`](./chrony.md)
* [`OpenNTPD`](./openntpd.md)

Other network time synchronization projects:

* `sntpd`
    * <https://github.com/troglobit/sntpd>
* `ntpsec`
    * <https://wiki.archlinux.org/title/NTPsec>
    * <https://ntpsec.org/>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
