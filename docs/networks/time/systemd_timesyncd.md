---
tags:
  - Networks
  - Timing
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# systemd timesyncd

**TODO**

???+ Note "Reference(s)"
    * [A simple SNTP daemon (client only implementation)](https://web.archive.org/web/20211122002449/https://lists.freedesktop.org/archives/systemd-devel/2014-May/019537.html).
    * <https://wiki.archlinux.org/title/Systemd-timesyncd>
    * <https://www.freedesktop.org/software/systemd/man/systemd-timesyncd.service.html>
    * <https://www.freedesktop.org/wiki/Software/systemd/timedated/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->


---
## Install

**TODO**


---
## Config

**TODO**


---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
