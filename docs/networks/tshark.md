---
tags:
  - Networks
  - Monitoring Programs
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `tshark`


???+ Note "Reference(s)"

    * <https://tshark.dev/>
    * <https://github.com/gcla/termshark>

    * <https://www.wireshark.org/docs/man-pages/wireshark-filter.html>
    * <https://www.wireshark.org/docs/wsug_html_chunked/>
    * <https://gitlab.com/wireshark/wireshark/-/wikis/home>
    * <https://ask.wireshark.org/questions/>
    * <https://www.tcpdump.org/manpages/pcap-filter.7.html>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->


---
## Install

!!! Note ""

    === "apk"
        ```console
        # apk add tshark
        ```

    === "apt"
        ```console
        # apt install tshark
        ```

    === "dnf"
        ```console
        # dnf install wireshark-cli
        ```

    === "emerge"
        TODO
        ```console
        ```

    === "nix"

        === "on NixOS"
            TODO
            ```console
            ```

        === "on non-NixOS"
            TODO
            ```console
            ```

    === "pacman"
        ```console
        # pacman -S wireshark-cli
        ```

    === "yum"
        ```console
        # yum install wireshark-cli
        ```

    === "xbps"
        TODO
        ```console
        ```

    === "zypper"
        TODO
        ```console
        ```

---
## Config

```console
$ sudo usermod -a -G wireshark $USER
```

**TODO**

---
## Use

**TODO**

```
$ sudo tshark -i interface-name -f "tcp and src host 127.0.0.1 and dst host 127.0.0.1 and src portrange 5064-5065"
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
