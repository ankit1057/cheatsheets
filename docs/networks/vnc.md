---
tags:
  - Networks
  - Remote Desktop Sharing
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# VNC

Virtual Network Computing (VNC) is an open source graphical desktop sharing system to remotely
control another computer. VNC is platform independent - there are clients and servers for many
GUI based operating systems.

!!! Note "Note"
    Two common alternative protocols are: RDP (a Microsoft proprietary protocol), and NX (a
    NoMachine proprietary protocol). But nothing proprietary will be discussed here.

At this point, you might think that `ssh` X11 forwarding (`$ ssh -X ...`) might be enough, but in a
lot of cases [it
isn't](https://superuser.com/questions/1217280/why-is-x11-forwarding-so-inefficient), a dedicated
tool is relevant.

* TODO: compare with [`xpra`](https://wiki.archlinux.org/index.php/Xpra)
* TODO: compare with [`x2go`](https://wiki.archlinux.org/index.php/X2Go)

A popular tool to use VNC is `tigervnc` which will be presented here.

This tool can be used in two ways:

1. By duplicating the screen of a remote computer in order to control a remote user's desktop: in
   this cheat sheet this will be referred as the **mirror** way.
2. By opening a new remote desktop on the remote computer, in order to use it potentially in
   parallel (like on another graphical server) of a user physically present on the computer without
   interfering with him: in this cheat sheet this will be referred as the **parallel** way.

???+ Note "Reference(s)"
    * <https://tigervnc.org/>
    * <https://wiki.gentoo.org/wiki/TigerVNC#Desktop_environments>
    * <https://gist.github.com/Musinux/8e6c4752ed3c143d49764311a504c1e9>
    * <https://wiki.archlinux.org/index.php/TigerVNC>
    * <https://wiki.gentoo.org/wiki/TigerVNC>
    * <https://help.ubuntu.com/community/VNC/Servers>
    * `tigervnc`
        * see `$ man xvnc`
        * see `$ man vncconfig`
        * see `$ man vncpasswd`
        * see `$ man vncserver`
        * see `$ man vncviewer`


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [on remote side](#on-remote-side)
* [Use](#use)
    * [on remote side](#on-remote-side-1)
    * [on client side](#on-client-side)
    * [comments](#comments)
    * [troubleshooting](#troubleshooting)

<!-- vim-markdown-toc -->

---
## Install

Install `tigervnc` on both local and remote PC:

!!! Note ""

    === "pacman"
        ```console
        # pacman -S tigervnc
        ```

    === "portage + emerge"
        ```console
        # vi /etc/portage/package.use/tigervnc
            > net-misc/tigervnc server
        # emerge -a net-misc/tigervnc
        ```

    === "apt"
        ```console
        # apt install tigervnc
        ```

    === "yum"
        ```console
        # yum install tigervnc
        ```

    === "dnf"
        ```console
        # dnf install tigervnc
        ```


---
## Config

### on remote side

Setup a VNC password:
```console
$ vncpasswd
```

Edit `~/.vnc/xstartup` (think of it as the
[`.xinitrc`](https://wiki.archlinux.org/index.php/Xinit#xinitrc) file that the graphical server
`vncserver` will run), depending on the remote side windows manager used:

!!! Note "`~/.vnc/xstartup`"

    === "gnome"
        ```console
        #!/bin/sh
        unset SESSION_MANAGER
        unset DBUS_SESSION_BUS_ADDRESS
        [ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
        xsetroot -solid blue
        gnome-session & # if not working try `gnome-session --session=gnome-classic &`
        gnome-terminal &
        ```

    === "kde"
        ```console
        #!/bin/sh
        startkde &
        ```

    === "xfce"
        ```console
        #!/bin/sh
        startxfce4 &
        ```

    === "lxde"
        ```console
        #!/bin/sh
        startlxde &
        ```

    === "unity"
        See <https://gist.github.com/Musinux/8e6c4752ed3c143d49764311a504c1e9>
        ```
        #!/bin/sh

        # Uncomment the following two lines for normal desktop:
        # unset SESSION_MANAGER
        # exec /etc/X11/xinit/xinitrc

        #[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
        #[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
        #xsetroot -solid grey
        #vncconfig -iconic &
        #x-terminal-emulator -geometry 80x24+10+10 -ls -title "$VNCDESKTOP Desktop" &
        #x-window-manager &

        export XKL_XMODMAP_DISABLE=1
        unset SESSION_MANAGER
        unset DBUS_SESSION_BUS_ADDRESS

        [ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
        [ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
        xsetroot -solid grey
        vncconfig -iconic &
        gnome-session & # if not working try `gnome-session --session=gnome-classic &`
        gnome-panel &
        gnome-settings-daemon &
        metacity &
        nautilus &
        gnome-terminal &
        ```

    === "flubox"
        ```console
        #!/bin/sh
        startfluxbox &
        ```

    === "dwm"
        ```console
        #!/bin/sh
        dwm &
        ```

Create a host white list (for secure mirror only):
```console
$ vi ~/.vnc/hosts
    > +127.0.0.1
```


---
## Use

> Let's consider that the remote user name is `resu` with IP `123.123.42.42`

### on remote side

Choose one way:

1. **parallel** secure way:
```console
$ vncserver -localhost :1
```

2. **parallel** unsecure way:
```console
$ vncserver :1
```

!!! Tip "Tip"
    Run `$ vncserver -kill :1` to gracefully shutdown the VNC server.

3. **mirror** secure way:
   ```console
   $ x0vncserver -display :0 -rfbauth ~/.vnc/passwd -HostsFile ~/.vnc/hosts
   ```
   with older versions of `x0vncserver` you might want to try this command instead:
   ```console
   $ x0vncserver -display :0 -PasswordFile ~/.vnc/passwd -localhost
   ```

2. **mirror** unsecure way:
   ```console
   $ x0vncserver -display :0 -rfbauth ~/.vnc/passwd
   ```
   with older versions of `x0vncserver` you might want to try this command instead:
   ```console
   $ x0vncserver -display :0 -PasswordFile ~/.vnc/passwd
   ```

!!! Tip "Tip"
    You can specify a window size for your parallel desktop when running the VNC server:
    `$ vncserver -geometry 1920x1080`.

!!! Tip "Tip"
    You can look at the currently running VNC servers:
    `$ vncserver -status`.

!!! Note "Note"
    If running `$ vncserver :42` you will get a message like the following:
    ```console
    > New 'HOSTNAME:42 (USERNAME)' desktop is HOSTNAME:42
    >
    > Starting applications specified in /home/USERNAME/.vnc/xstartup
    > Log file is /home/USERNAME/.vnc/servoche:1.log
    ```
    You just create a VNC server with id 42, you can now check it's logs in
    `~/.vnc/HOSTNAME:42.log`.

    If just run `$ vncserver` you will get similar message with automatically choosed `vncserver`
    id.

    Also note that `5900+id` (e.g. 5942 with id 42) is the default port to connect to the
    `vncserver`.


### on client side

Choose one way:

1. **parallel** secure way (through ssh tunneling):
```console
$ ssh -fL 9901:127.0.0.1:5901 resu@123.123.42.42 sleep 10; vncviewer 127.0.0.1:9901
```

2. **parallel** unsecure way:
```console
$ vncviewer 123.123.42.42:5901 # you might need to open port 5901 of the remote side
```

3. Mirror (for secure and unsecure `x0vncserver`) way:
```console
$ ssh -fL 9900:127.0.0.1:5900 resu@123.123.42.42 sleep 10; vncviewer 127.0.0.1:9900
```

### comments

* Using the previously presented secure ways have multiple advantages:
    * they are *secure*: they are not vulnerable to man in the middle attacks because they are
      ciphered (thanks to ssh tunneling).
    * they don't require to open new ports, just the `ssh` port will be used.

* `tigervnc` might not be available in the package manager and you might not want to install it
  manually (from <https://tigervnc.org/>) , in this case...
    * ... `x11vnc` is a common **mirror** alternative, it can be used as a drop in replacement for
      `x0vncserver`: just replace `x0vncserver` with `x11vnc` in the commands presented previously
      and it should run well.
    * ... `vnc4server` is a common **parallel** alternative, it can be used as a drop in
      replacement for `vncserver`: just use the same commands presented previously (`vncserver` is
      in deed the command used by `vnc4server`).
    * ... A lot of alternative can replace the `vncviewer` tool, like graphical ones: `gtk-vnc`,
      `krdc`, `vinagre`, `remmina`.

### troubleshooting

* No mouse pointer / no cursor: the cursor might seems invisible when using `vncviewer`, in this
  case, just add the `-DotWhenNoCursor=1` option to your `vncviewer` command (see
  <https://serverfault.com/a/560853>).

* After running the following:
  ```console
  $ x0vncserver -display :0 -PasswordFile ~/.vnc/passwd -localhost
  ```
  If you get a similar error:
  ```console
  Fri Jun  4 17:27:32 2021
   main:        XTest extension present - version 2.2
   main:        unable to bind listening socket: Address already in use (98)
  ~ImageCleanup called
  ```
  Then just restart `x0vncserver`:
  ```console
  $ killall x0vncserver
  $ x0vncserver -display :0 -PasswordFile ~/.vnc/passwd -localhost
  ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
