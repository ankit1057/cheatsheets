---
tags:
  - Networks
  - Bluetooth
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# BlueZ

BlueZ is an implementation of the Bluetooth protocol stack for Linux. It is a Linux standard for
the short-range wireless interconnection of cellular phones, computers, and other electronic
devices.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Bluetooth>
    * <https://wiki.gentoo.org/wiki/Bluetooth>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Pairing](#pairing)
    * [Connecting](#connecting)
    * [Troubleshooting](#troubleshooting)
        * [`Failed to connect: org.bluez.Error.Failed br-connection-profile-unavailable`](#failed-to-connect-orgbluezerrorfailed-br-connection-profile-unavailable)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "Gentoo kernel"
        ```console
        $ cd /usr/src/linux
        # make nconfig # or `make menuconfig`

            # BLUEHOOTH
            ###########
            # Double check here: <https://wiki.gentoo.org/wiki/Bluetooth>
            # Double check the prerequisites: <https://wiki.gentoo.org/wiki/Bluetooth#Prerequisites>
            #
            > [*] Networking support ---> # Symbol: NET [=y]
            >       <M>   Bluetooth subsystem support ---> # Symbol: BT [=m]
            >               [*]   Bluetooth Classic (BR/EDR) features # Symbol: BT_BREDR [=y]
            >               <*>     RFCOMM protocol support # Symbol: BT_RFCOMM [=y]
            >               [ ]       RFCOMM TTY support # Symbol: BT_RFCOMM_TTY [=n]
            >               < >     BNEP protocol support # Symbol: BT_BNEP [=n]
            >               [ ]       Multicast filter support # Symbol: BT_BNEP_MC_FILTER [=n]
            >               [ ]       Protocol filter support # Symbol: BT_BNEP_PROTO_FILTER [=n]
            >               <*>     HIDP protocol support # Symbol: BT_HIDP [=y]
            >               [*]     Bluetooth High Speed (HS) features # Symbol: BT_HS [=y]
            >                       ...
            >               [*]   Bluetooth Low Energy (LE) features # Symbol: BT_LE [=y]
            >                     Bluetooth device drivers --->
            >                       <M> HCI USB driver # Symbol: BT_HCIBTUSB [=m]
            >                       <M> HCI UART driver # Symbol: BT_HCIUART [=m]
            >       <*>   RF switch subsystem support --->  # Symbol: RFKILL [=y]
            >     Device Drivers --->
            >           HID support --->
            >             <*>   User-space I/O driver support for HID subsystem # Symbol: UHID [=y]
        ```

        !!! Warning ""
            After configuring the kernel don't forget to do a [kernel make and
            rebuild](../distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!


!!! Note ""

    === "emerge"
        * Add `bluetooth` to the `USE` variable of `/etc/portage/make.conf`:
        ```console
        $ sudo emerge --ask --changed-use --deep @world
        ```

        * `hciconfig` and other utilities are only available if `net-wireless/bluez` is installed
          with the deprecated USE flag enabled:
        ```console
        $ sudo vi /etc/portage/package.use/bluez
            > net-wireless/bluez deprecated

        $ sudo emerge --ask --noreplace net-wireless/bluez
        ```

    === "pacman"
        ```console
        $ pacman -S bluez bluez-utils
        ```

        !!! Tip "For Artix users"
            * **If** using `dinit`:
            ```console
            # pacman -S bluez bluez-dinit bluez-utils
            ```
            * **If** using `openrc`:
            ```console
            # pacman -S bluez bluez-openrc bluez-utils
            ```
            * **If** using `runit`:
            ```console
            # pacman -S bluez bluez-runit bluez-utils
            ```
            * **If** using `s6`:
            ```console
            # pacman -S bluez bluez-s6 bluez-utils
            ```

    === "apt"
        ```console
        $ apt install bluez
        ```

    === "yum"
        ```console
        $ yum install bluez
        ```

    === "dnf"
        ```console
        $ dnf install bluez
        ```

---
## Config

* Permissions for Bluetooth devices is handled automatically by D-Bus, and access is granted to all
users by default.

* Start Bluetooth:

    !!! Note ""

        === "OpenRC"
            ```console
            # rc-service bluetooth start
            ```

        === "Runit"
            Depending on your `runit` implementation, either run:
            ```console
            # ln -s /etc/runit/sv/bluetooth /service
            # touch /etc/runit/sv/bluetooth/down # prevent start at boot
            ```
            **or** run:
            ```console
            # ln -s /etc/runit/sv/bluetooth /var/service
            # touch /etc/runit/sv/bluetooth/down # prevent start at boot
            ```
            **or** run:
            ```console
            # ln -s /etc/runit/sv/bluetooth /run/runit/service
            # touch /etc/runit/sv/bluetooth/down # prevent start at boot
            ```
            In any case, finally run:
            ```console
            # sv up bluetooth
            ```

        === "SysVinit"
            TODO

        === "SystemD"
            ```console
            # systemctl start bluetooth
            ```

* Optionally start Bluetooth at boot:

    !!! Note ""

        === "OpenRC"
            ```console
            # rc-update add bluetooth default
            ```

        === "Runit"
            Depending on your `runit` implementation, either run:
            ```console
            # ln -s /etc/runit/sv/bluetooth /service
            ```
            **or** run:
            ```console
            # ln -s /etc/runit/sv/bluetooth /var/service
            ```
            **or** run:
            ```console
            # ln -s /etc/runit/sv/bluetooth /run/runit/service
            ```
            In any case, finally run:
            ```console
            # sv up bluetooth
            ```

        === "SysVinit"
            TODO

        === "SystemD"
            ```console
            # systemctl enable bluetooth
            ```

---
## Use

```console
$ bluetoothctl --help
```

1. (OPTIONAL) Select a default controller with `$ bluetoothctl select <MAC address>`.
2. (OPTIONAL) Enter `$ bluetoothctl power on` to turn the power to the controller on if the device
   is set to off. It is on by default.
3. Enter `$ bluetoothctl devices` to get the MAC address of the device with which to pair.
4. Enter device discovery mode with `$ bluetoothctl scan on` command if device is not yet on the
   list.
5. Turn the agent on with `$ bluetoothctl agent on` or choose a specific agent: if you press tab
   twice after agent you should see a list of available agents. A bluetooth agent is what manages
   the Bluetooth 'pairing code'. It can either respond to a 'pairing code' coming in, or can send
   one out. The default-agent should be appropriate in most cases.
6. Enter `$ bluetoothctl pair <MAC address>` to do the pairing (tab completion works).
7. If using a device without a PIN, one may need to manually trust the device before it can
   reconnect successfully. Enter `$ bluetoothctl trust <MAC address>` to do so.
8. Enter `$ bluetoothctl connect <MAC address>` to establish a connection.

### Pairing

First, power on your bluetooth device, and enable its pairing mode, then:
```console
$ bluetoothctl power on

$ bluetoothctl scan on
    Discovery started
    [CHG] Controller 05:EB:67:76:CD:64 Discovering: yes
    [NEW] Device 69:1B:AB:45:19:A5 69-1B-AB-45-29-A5
    [NEW] Device 7F:D7:15:68:B3:CB 7F-D7-15-68-B3-CB
    [NEW] Device 8C:B2:C4:A6:3C:4B 8C-B2-C4-A6-3C-4B
    [NEW] Device 31:85:DA:BF:57:A8 Your Device Name Here
    ...

$ bluetoothctl pair 31:85:DA:BF:57:A8

$ bluetoothctl devices
    ...
    Device 31:85:DA:BF:57:A8 Your Device Name Here

$ bluetoothctl info 31:85:DA:BF:57:A8

$ bluetoothctl trust 31:85:DA:BF:57:A8 # OPTIONAL if not already trusted
$ bluetoothctl unblock 31:85:DA:BF:57:A8 # OPTIONAL if blocked
```

### Connecting

First, pair your Bluetooth device (see previous section), then:
```console
$ bluetoothctl power on

$ bluetoothctl devices
    ...
    Device 31:85:DA:BF:57:A8 Your Device Name Here

$ bluetoothctl connect 31:85:DA:BF:57:A8
```

### Troubleshooting

#### `Failed to connect: org.bluez.Error.Failed br-connection-profile-unavailable`

```console
Attempting to connect to 31:85:DA:BF:57:A8
Failed to connect: org.bluez.Error.Failed br-connection-profile-unavailable
```

If you are facing the above error message on an Arch-based distros, then :
- if you just installed the `bluez` related packages, then a simple reboot might enough.
- if you installed the `bluez` related packages in a previous session and you are using
  `pulseaudio`, then you might want to install `pulseaudio-bluetooth` and reboot.

See <https://bbs.archlinux.org/viewtopic.php?id=270465>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
