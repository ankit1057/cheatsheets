---
tags:
  - Networks
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Netcat

Netcat (or `nc`) is a command line utility that reads and writes data across network connections,
using the TCP or UDP protocols. It is one of the most powerful tools in the network and system
administrators arsenal, and it as considered as a Swiss army knife of networking tools.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Network_tools>
    * <https://www.linuxfordevices.com/tutorials/netcat-command-in-linux>
    * <https://linuxize.com/post/netcat-nc-command-with-examples/>
    * `$ man nc`

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a netcat
        ```

    === "pacman"
        ```console
        # pacman -S netcat
        ```

    === "apt"
        ```console
        # apt install netcat
        ```

    === "yum"
        ```console
        # yum install nc
        ```

    === "dnf"
        ```console
        # dnf install nc
        ```

---
## Use

* Connect via TCP (by default) to a given port (e.g. `22`) on given target host (e.g.
  `192.168.123.123`):
```console
$ nc 192.168.123.123 22
```

* Connect via UDP to a given port (e.g. `22`) on given target host (e.g. `192.168.123.123`):
```console
$ nc -u 192.168.123.123 22
```

* Connect to multiple ports (e.g. `22`, `23`, `24`) on given target host (e.g. `192.168.123.123`):
```console
$ nc 192.168.123.123 22 23 24
```

* Connect to a range of ports (e.g. `22`-`32`) on given target host (e.g. `192.168.123.123`):
```console
$ nc 192.168.123.123 22-32
```

* Connect, with a timeout (e.g. 2 secs), to a given port (e.g. `22`) on given target host (e.g.
  `192.168.123.123`):
```console
$ nc -w 2 192.168.123.123 22
```

* Connect, in scan mode (i.e. scan for listening daemons without actually sending any data to
  them), to a given port (e.g. `22`) on given target host (e.g. `192.168.123.123`):
```console
$ nc -z 192.168.123.123 22
```

* Connect to a given port (e.g. `22`) on a given target host (e.g. `192.168.123.123`), and output
  the hex dump of the traffic to a file:
```console
$ nc -o /path/to/output/file 192.168.123.123 22
```

* Send data to a given port (e.g. `8080`) on a given target host (e.g. `192.168.123.123`):
```console
$ nc 192.168.123.123 8080 < /path/to/data.txt
```

* Listen to a local port (e.g. `8080`) and copy the incoming traffic to a file:
```console
$ nc -l 8080 > /path/to/file.txt
```

* Attach a local port (e.g. `4242`) to a local shell (e.g. `/bin/bash`), thanks to the `-e` option
  which allow to execute a specified program when a client connects to the port. ⚠️ == But be
  careful with this, anybody could connect to your `4242` port and gain access to your `/bin/bash`
  shell! Consider this like a system backdoor!==⚠️:
```console
$ nc -l 4242 -e /bin/bash
```
> Just run `$ nc 192.168.123.123 4242` to access the remote `/bin/bash` (e.g. on a remote target
> with `192.168.123.123` IP)

**WIP**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
