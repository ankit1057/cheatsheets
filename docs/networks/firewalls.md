---
tags:
  - Networks
  - Firewalls
  - Security
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Firewalls

A firewall is a network security system that monitors and controls incoming and outgoing network
traffic based on predetermined security rules. A firewall typically establishes a barrier
between a trusted network and an untrusted network.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Category:Firewalls>
    * <https://wiki.gentoo.org/wiki/Security_Handbook/Firewalls>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Popular firewalls](#popular-firewalls)

<!-- vim-markdown-toc -->

---
## Popular firewalls

A lot of firewalls are available. Here are maybe some of the most popular:

* [`ufw`](./ufw.md)
* [`firewalld`](./firewalld.md)
* [`iptables`](./iptables.md)


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
