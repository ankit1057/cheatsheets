---
tags:
  - Networks
  - VPN
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# OpenFortiVPN

OpenFortiVPN is a client for PPP + SSL VPN tunnel services. It spawns a `pppd` (PPP daemon) process
and operates the communication between the gateway and this process.

It is compatible with Fortinet VPNs.

???+ Note "Reference(s)"
    * <https://github.com/adrienverge/openfortivpn>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)
    * [Troubleshooting](#troubleshooting)
        * [`Peer refused to agree to his IP address`](#peer-refused-to-agree-to-his-ip-address)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "Gentoo kernel"
        A correct [kernel config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) is
        needed:
        ```console
        $ cd /usr/src/linux
        # make nconfig # or `# make menuconfig`

        TODO
        ```

        The following kernel modules are needed:
        - CONFIG_PPP
        - CONFIG_PPP_ASYNC
        - CONFIG_PPP_SYNC_TTY
        - CONFIG_PPP_DEFLATE
        - CONFIG_PPP_BSDCOMP
        - CONFIG_PPP_MPPE
        - CONFIG_PPPOE

    !!! Warning "Warning"
        After configuring the kernel don't forget to do a [kernel make and
        rebuild](../distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!

!!! Note ""

    === "emerge"
        ```console
        # emerge -a net-vpn/openfortivpn
        ```

    === "pacman"
        ```console
        # pacman -S openfortivpn
        ```

    === "apt"
        ```console
        # apt install openfortivpn
        ```

    === "yum"
        TODO

    === "dnf"
        TODO

---
## Use

* Simply connect to a VPN:
  ```console
  $ openfortivpn 123.123.1.2:1234 --username=foo
  ```

* Connect to a VPN using an authentication realm:
  ```console
  $ openfortivpn 123.123.1.2:1234 --username=foo --realm=bar
  ```

* Store password securely:
  ```console
  $ openfortivpn 123.123.1.2:1234 --username=foo --pinentry=pinentry-mac
  ```

* Don't set IP routes and don't add VPN name servers to `/etc/resolv.conf`:
  ```console
  $ openfortivpn 123.123.1.2:1234 -u foo --no-routes --no-dns --pppd-no-peerdns
  ```

* Using a configuration file:
  ```console
  $ openfortivpn -c /etc/openfortivpn/my-config
  ```
  
  With `/etc/openfortivpn/my-config` containing:
  ```console
  host = vpn-gateway
  port = 8443
  username = foo
  set-dns = 0
  pppd-use-peerdns = 0
  # X509 certificate sha256 sum, trust only this one!
  trusted-cert = e46d4aff08ba6914e64daa85bc6112a422fa7ce16631bff0b592a28556f993db
  ```

* For the full list of config options, see the CONFIGURATION section of:
  ```console
  $ man openfortivpn
  ```

### Troubleshooting

#### `Peer refused to agree to his IP address`

If you ever come accross an error like the following one:

```console
...
INFO:   Connected to gateway.
INFO:   Authenticated.
INFO:   Remote gateway has allocated a VPN.
Using interface ppp0
Connect: ppp0 <--> /dev/pts/9
INFO:   Negotiation complete.
Peer refused to agree to his IP address
Connect time 0.0 minutes.
Sent 1101 bytes, received 1081 bytes.
INFO:   Cancelling threads...
INFO:   Cleanup, joining threads...
Hangup (SIGHUP)
Modem hangup
Connection terminated.
```

Then you might want to try to uncomment the `ipcp-accept-local` and/or `ipcp-accept-remote` options
in `/etc/ppp/options`, like described here:
<https://github.com/adrienverge/openfortivpn/issues/920#issuecomment-1249267382>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
