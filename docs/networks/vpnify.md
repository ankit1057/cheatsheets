---
tags:
  - Networks
  - VPN
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `vpnifi`

This tool can be used to transparently route traffic of certain programs through VPN, while keeping
the rest of it routed normally. It is protocol agnostic and should work with any VPN software.

???+ Note "Reference(s)"
    * <https://github.com/laserbat/vpnify>
    * <https://www.youtube.com/watch?v=5Tls2LRKh-c>
    * <https://www.adamayala.com/blog/2019/08/06/split-tunneling-a-vpn-on-an-ubuntu-server/>
    * <https://gist.github.com/GAS85/4e40ece16ffa748e7138b9aa4c37ca52>
    * <https://www.privateinternetaccess.com/>
    * <https://www.privateinternetaccess.com/helpdesk/kb/articles/desktop-application-split-tunneling-feature>

!!! Warning ""
    PIA might not work because of this:

    * <https://www.privateinternetaccess.com/helpdesk/kb/articles/split-tunnel-app-examples#anchor-6>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

```console
$ git clone https://github.com/laserbat/vpnify.git
cd vpnify
```

---
## Use

* with OpenVPN:
```console
$ curl -4 ifconfig.me
$ ./vpnify sudo openvpn ~/.config/openvpn/client.ovpn # start the vpn
$ ./vpnify curl -4 ifconfig.me
$ curl -4 ifconfig.me
```

* with WireGuard:
```console
TODO
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
