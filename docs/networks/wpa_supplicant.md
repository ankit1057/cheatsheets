---
tags:
  - Networks
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `wpa_supplicant`

`wpa_supplicant` is a cross platform WiFi supplicant to handle network authentication. It is
suitable for desktops, laptops and embedded systems.

!!! Note "Note"
    Note that `wpa_supplicant` can be managed through `connman`. In this case you just
    need to install `wpa_supplicant` and refer to the [ConnMan cheat sheet](./connman.md).

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Wifi>
    * <https://wiki.gentoo.org/wiki/iwlwifi>
    * <https://wiki.gentoo.org/wiki/Wpa_supplicant>
    * <https://wiki.archlinux.org/index.php/Wpa_supplicant>
    * <https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Networking>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [TODO](#todo)
* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [1. `wpa_cli` interactive mode:](#1-wpa_cli-interactive-mode)
    * [2. `wpa_passphrase`](#2-wpa_passphrase)
    * [3. `wpa_supplicant.conf`](#3-wpa_supplicantconf)

<!-- vim-markdown-toc -->

---
## TODO

* [`wifish`](https://github.com/bougyman/wifish)


---
## Install

!!! Note "Gentoo kernel"
    During your [Gentoo installation](../distros/gentoo-based/gentoo_installation.md) you might
    have identified your modules driver (including WiFi driver) with `$ lspci -nnk` (see [3.1
    Installing installation files : stage 3 and portage
    tarballs](../distros/gentoo-based/gentoo_installation.md#31-installing-installation-files-stage-3-and-portage-tarballs)).
    
    <br/>
    
    **If** you don't have identified your modules driver, reboot on a
    [system rescue cd](http://www.system-rescue-cd.org/) and execute the following:
    ```console
    # lspci -nnk > tmp_systemrescuecd_lspci
    # lspci -nnk | less # search for "Network controller" -> now you identified your mofules driver
    ```
    **Then** reboot, without system rescue cd, on your installation.
    
    <br/>
    
    Now, the [kernel config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) can be
    modified appropriatly:
    ```console
    $ cd /usr/src/linux
    # make nconfig # or `# make menuconfig`
    
        # IEEE 802.11
        # Double check here: <https://wiki.gentoo.org/wiki/Wifi#IEEE_802.11>
        #
        > [*] Networking support ---> # Symbol: NET [=y]
        >    [*] Wirless ---> # Symbol: WIRELESS [=y]
        >        <M/*> cfg80211 - wireless configuration API # Symbol: CFG80211 [=y]
        >        ...
        >        [*]   enable powersave by default # Symbol: CFG80211_DEFAULT_PS [=y]
        >        ...
        >        <M/*> Generic IEEE 802.11 Networking Stack (mac80211) # Symbol: MAC80211 [=y]
        >              Default rate control algorithm (Minstrel)  --->
        >               <X> Minstrel # Symbol: MAC80211_RC_DEFAULT_MINSTREL [=y]
    
        # WEXT
        # Double check here: <https://wiki.gentoo.org/wiki/Wifi#WEXT>
        #
        > [*] Networking support  ---> # Symbol: NET [=y]
        >    [*] Wireless  ---> Symbol: WIRELESS [=y]
        >       [*]   cfg80211 wireless extensions compatibility # Symbol: CFG80211_WEXT [=y]
    
        # Enable LED support
        # Double check here: <https://wiki.gentoo.org/wiki/Wifi#Kernel>
        #
        > Device Drivers  --->
        >     [*] LED Support  --->
        >         <*>   LED Class Support
        >
        > [*] Networking support  ---> # Symbol: NET [=y]
        >   [*] Wireless  ---> Symbol: WIRELESS [=y]
        >       [*] Enable LED triggers # Symbol: MAC80211_LEDS [=y]
    ```
    
    **If** your wifi driver is `iwlwifi` (**else** configure accordingly):
    ```console
    $ cd /usr/src/linux
    # make nconfig # or `# make menuconfig`
    
        # Device drivers
        # <https://wiki.gentoo.org/wiki/Wifi#Device_drivers>
        #
        > Device Drivers --->
        >    [*] Networking device support ---> # Symbol: NETDEVICES [=y]
        >       [*] Wirless LAN ---> # Symbol: WLAN [=y]
        >           [*] Intel devices
        >           ...
        >           <M>   Intel Wireless Wifi Next Gen AGN - ... # Symbol: IWLWIFI [=m]
        >           <M>     Intel Wireless Wifi DVM Firmware support # Symbol: IWLDVM [=m]
        >           <M>     Intel Wireless Wifi MVM Firmware support # Symbol: IWLMVM [=m]
    
        > -*- Cryptographic API ---> # Symbol: CRYPTO [=y]
        >    -*- AES cipher algorithms # Symbol: CRYPTO_AES [=y]
        >    -*- AES cipher algorithms (x86_64) # Symbol: CRYPTO_AES_X86_64 [=y]
        >    <*> AES cipher algorithms (AES-NI) # Symbol: CRYPTO_AES_NI_INTEL [=y]
    
    ```

    !!! Warning " "
        After configuring the kernel don't forget to do a [kernel make and
        rebuild](../distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!


Now `wpa_supplicant` can be installed:

!!! Note ""

    === "emerge + portage"
        ```console
        # vi /etc/portage/package.accept_keywords
            > # (manual) last wpa_supplicant
            > net-wireless/wpa_supplicant ~amd64
            >
            > ...

        # emerge -a net-wireless/wpa-supplicant
        ```

    === "pacman"
        ```console
        # pacman -S wpa-supplicant
        ```

        !!! Tip "For Artix users"
            * **If** using `openrc`:
            ```console
            # pacman -S wpa_supplicant-openrc
            ```
            * **If** using `runit`:
            ```console
            # pacman -S wpa_supplicant-runit
            ```
            * **If** using `s6`:
            ```console
            # pacman -S wpa_supplicant-s6
            ```

    === "apt"
        ```console
        # apt install wpa
        ```

    === "yum"
        ```console
        # yum install wpa-supplicant
        ```

    === "dnf"
        ```console
        # dnf install wpa-supplicant
        ```


---
## Config

The `wpa_supplicant` configuration file does not exist by default, you can create one based on the
doc:
```console
# bzless /usr/share/doc/wpa_supplicant-x.x.x/wpa_supplicant.conf.bz2 > /etc/wpa_supplicant/wpa_supplicant.conf

# vi /etc/wpa_supplicant/wpa_supplicant.conf
    > ...
    > # make this file writable for wpa_gui / wpa_cli
    > update_config=1
    > ...
    > # allow users in the 'wheel' group to control wpa_supplicant
    > ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=wheel
    > ...

# vi /etc/conf.d/wpa_supplicant # setup for dhcpd as network manager
    > ...
    > wpa_supplicant_args="-B -M -c/etc/wpa_supplicant/wpa_supplicant.conf"

# vi /etc/conf.d/net # tell network script to use wpa_supplicant
    > ...
    > modules_wlan0="wpa_supplicant"
    > config_wlan0="dhcp"
    > ...

# chmod 600 /etc/wpa_supplicant/wpa_supplicant.conf
```

!!! Note ""

    === "OpenRC"
        ```console
        # rc-update add wpa_supplicant default
        # /etc/init.d/wpa_supplicant start
        ```

    === "Runit"
        Depending on your `runit` implementation, either run:
        ```console
        # ln -s /etc/runit/sv/wpa_supplicant /service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/wpa_supplicant /var/service
        ```
        **or** run:
        ```console
        # ln -s /etc/runit/sv/wpa_supplicant /run/runit/service
        ```
        In any case, finally run:
        ```console
        # sv up wpa_supplicant
        ```

    === "SysVinit"
        ```console
        # service wpa_supplicant start
        # chkconfig wpa_supplicant on
        ```

    === "SystemD"
        ```console
        # systemctl enable wpa_supplicant
        # systemctl start wpa_supplicant
        ```


---
## Use

Now **choose one** of the following three connection method (see
<https://wiki.gentoo.org/wiki/Wpa_supplicant>):

### 1. `wpa_cli` interactive mode:

```console
# wpa_cli -i wlp2s0

    > scan
    ...
    > scan_results
    ...
    > add_network
    0
    > set_network 0 ssid "Livebox-1234"
    ...
    > set_network 0 psk "liveboxpassword"
    ...
    > enable_network 0
    ...
    > save_config
    ...
    > quit
```

Useful interactive commands:
```console
    > select_network 0

    > remove_network 0

    > list_networks
```
(to get the list of all commands, press <TAB> two times)

### 2. `wpa_passphrase`

```console
# wpa_passphrase <ssid> [password]
```

E.g.
```console
# wpa_passphrase <Livebox-1234> [liveboxpassword]
```

### 3. `wpa_supplicant.conf`

Find the SSID and the BSSID (searching for `BSS`) of your connection (`ssid` and `BSS` fields):
```console
# emerge -a net-wireless/iw

# iw dev wlp2s0 scan | less
or
# wpa_cli -i wlp2s0
    > scan
    ...
    > scan_results
```

Now adjust `wpa_supplicant.conf`:
```console
# vi /etc/wpa_supplicant/wpa_supplicant.conf
    > ...
    > ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=wheel
    > ...
    > ap_scan=1
    > ...
    > network={
    >    bssid=00:01:02:03:04:05
    >    ssid="Livebox-1234"
    >    psk="liveboxpassword"
    >    scan_ssid=1
    >    proto=RSN # might be optionnal
    >    key_mgmt=WPA-PSK # might be optionnal
    >    group=CCMP TKIP # might be optionnal
    >    pairwise=CCMP TKIP # might be optionnal
    >    priority=5
    > }

$ wpa_cli (-i wlp2s0) reconfigure
$ wpa_cli (-i wlp2s0) reconnect
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
