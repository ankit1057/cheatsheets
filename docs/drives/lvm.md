---
tags:
  - Drives
  - Disks
  - RAID
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# LVM

VM (Logical Volume Manager) allows administrators to create meta devices that provide an
abstraction layer between a file system and the physical storage that is used underneath.


???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/LVM>
    * <https://wiki.archlinux.org/index.php/LVM>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

**TODO**


---
## Config

**TODO**


---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
