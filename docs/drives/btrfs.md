---
tags:
  - Drives
  - Disks
  - File Systems
  - RAID
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Btrfs

Btrfs CoW file system for Linux aimed at implementing advanced features while focusing on fault
tolerance, repair, and easy administration.

???+ Note "Reference(s)"
    * <https://btrfs.wiki.kernel.org/index.php/Status>
    * <https://btrfs.wiki.kernel.org/index.php/FAQ>
    * <https://wiki.gentoo.org/wiki/Btrfs>
    * <https://wiki.archlinux.org/index.php/Btrfs>
    * <https://btrfs.wiki.kernel.org/index.php/Main_Page>
    * <https://btrfs.wiki.kernel.org/index.php/SysadminGuide>
    * <https://www.reddit.com/r/linux/comments/9z7h7u/zfs_vs_btrfs/>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [Create OR convert a FS to a "simple" Btrfs](#create-or-convert-a-fs-to-a-simple-btrfs)
    * [Create a FS to RAID 1 Btrfs](#create-a-fs-to-raid-1-btrfs)
    * [Add an fstab entry](#add-an-fstab-entry)
* [Use](#use)
    * [CoW](#cow)
    * [Compression](#compression)
    * [Scrub](#scrub)
    * [Sub volumes](#sub-volumes)
    * [Quota](#quota)
    * [Commit interval](#commit-interval)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "Gentoo kernel"
        A correct [kernel config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) is
        needed:
        ```console
        $ cd /usr/src/linux
        # make nconfig # or `# make menuconfig`

            # Activate Btrfs in the kernel
            # Double check here: <https://wiki.gentoo.org/wiki/Btrfs#Kernel>
            #
            > File systems  --->
            >     <*> Btrfs filesystem support # Symbol: BTRFS_FS [=y]
        ```

        !!! Warning "Warning"
            After configuring the kernel don't forget to do a [kernel make and
            rebuild](..//distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!

!!! Note ""

    === "emerge"
        ```console
        # emerge -a sys-fs/btrfs-progs
        ```

    === "pacman"
        ```console
        # pacman -S btrfs-progs
        ```

    === "apt"
        ```console
        # apt install btrfs-progs
        ```

    === "yum"
        ```console
        # yum install btrfs-progs
        ```

    === "dnf"
        ```console
        # dnf install btrfs-progs
        ```


---
## Config

> `/dev/sdXN` will be the disk partition to format ("X" the disk number, "N" the partition letter)

### Create OR convert a FS to a "simple" Btrfs

* Create a Btrfs file system on the `/dev/sdXN` partition :
```console
# mkfs.btrfs /dev/sdXN # mkfs.btrfs irreversibly destroys any content of the formated partition
```

<br/> **OR** <br/>

* Convert a whole disk ext4 based file system to Btrfs file system:
```console
# umount /dev/sdX
# fsck.ext4 -f /dev/sdX # Check the integrity of the filesystem using the appropriate fsck tool
# btrfs-convert /dev/sdX # Use btrfs-convert to convert the ext* formatted device into a Btrfs-formatted device
```

### Create a FS to RAID 1 Btrfs

* Create a Btrfs file system on the `/dev/sdX` and `/dev/sdY` disks:
```console
# mkfs.btrfs -m raid1 /dev/sdX /dev/sdY -d raid1 /dev/sdX /dev/sdY # -m for metadata and -d for data
```

### Add an fstab entry

* Configure fstab to mount Btrfs disks (e.g. with `zstd` compression, other options are `zlib` or
  `lzo`):
```console
# sudo vi /etc/fstab
    > ...
    > UUID=...   /dev/sdX  btrfs  defaults,noatime,compress=zstd  0 0
    > ...
```

---
## Use

* Global usage: <https://wiki.archlinux.org/index.php/Btrfs#Usage>

* Re compress the whole file system (e.g. with `zstd` compression method):
```console
# btrfs filesystem defragment -r -v -czstd /path-to-mount-point/mount-point
```

* Report file space usage with Btrfs:
```console
# btrfs filesystem du -s /media/drive
```

* Report disk space usage with Btrfs:
```console
# btrfs filesystem df /media/drive
```

* Report full breakdown of device allocation and usage stats:
```console
# btrfs filesystem usage /media/raid
```

### CoW

* <https://wiki.archlinux.org/index.php/Btrfs#Copy-on-Write_(CoW)>

!!! Warning
    Turning off CoW also turns off checksums which puts your data "at risk" (as much as with ext4).
    Without checksums, Btrfs can't self heal from disk corruptions (even on RAID1 setups).  See
    <https://btrfs.wiki.kernel.org/index.php/FAQ#Can_I_have_nodatacow_.28or_chattr_.2BC.29_but_still_have_checksumming.3F>.

### Compression

* <https://wiki.archlinux.org/index.php/Btrfs#Compression>

### Scrub

A scrub is an online file system checking tool. Reads all the data and metadata on the file system,
and uses checksums and the duplicate copies from RAID storage to identify and repair any corrupt
data.

* Start a (background) scrub:
```console
# btrfs scrub start /media/drive
```

* Check the status of a running scrub:
```console
# btrfs scrub status /media/drive
```


### Sub volumes

* <https://wiki.archlinux.org/index.php/Btrfs#Subvolumes>

### Quota

* <https://wiki.archlinux.org/index.php/Btrfs#Quota>

### Commit interval

* <https://wiki.archlinux.org/index.php/Btrfs#Commit_interval>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
