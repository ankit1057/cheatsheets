---
tags:
  - Graphical
  - Xorg
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Suckless tools

Suckless tools is a group of quality software such as `dwm`, `dmenu`, `st` and plenty of other
tools, with a focus on simplicity, clarity and frugality. The Suckless philosophy is about keeping
things simple, minimal and usable (believing this should become the mainstream philosophy in the IT
sector). While unfortunately, the tendency for complex, error prone and slow software seems to be
prevalent in the present-day software industry. Suckless tools intend to prove the opposite.

!!! Note "Prerequisite(s)"
    - [Xorg graphic server](./xorg.md)

???+ Note "Reference(s)"
    - <https://suckless.org/>
    - <https://dwm.suckless.org/>
    - <https://st.suckless.org/>
    - <https://gitlab.com/stephane.tzvetkov/config/-/tree/master/home%2Fuser%2Fapps/src-apps%2Fsucklesstools>
    - <https://wiki.gentoo.org/wiki/Dwm>
    - <https://wiki.archlinux.org/index.php/Dwm>
    - <https://wiki.archlinux.fr/DWM>
    - <https://www.youtube.com/watch?v=xnREqY-oyzM>
    - <https://github.com/torrinfail/dwmblocks>
    - <https://gitlab.com/stephane.tzvetkov/sucklesstools>


---
## Table of content

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [apply a patch](#apply-a-patch)
    * [`st` 0.8.3 patches examples](#st-083-patches-examples)
    * [customization guide](#customization-guide)
* [Use](#use)
    * [`dwm`](#dwm)
    * [`st`](#st)
        * [`st` troubleshooting](#st-troubleshooting)
    * [`dmenu`](#dmenu)
    * [`slock`](#slock)
    * [Tips](#tips)
    * [`slock` troubleshooting](#slock-troubleshooting)

<!-- vim-markdown-toc -->

---
## Install

!!! Note "Needed dependency for building `dwm`"

    === "emerge"
        ```console
        # emerge -a x11-libs/libXinerama
        ```

    === "pacman"
        ```console
        # pacman -S libxinerama
        ```

    === "apt"
        ```console
        # apt install libxinerama
        ```

    === "yum"
        ```console
        # yum install libXinerama
        ```

    === "dnf"
        ```console
        # dnf install libXinerama
        ```

Create a `~/bin` folder to install locally (user wide) the Suckless tools and add it to your `PATH`:
```console
$ mkdir -p ~/bin/
$ echo 'PATH="/home/user/bin:$PATH"' >> $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
```

Create a `sucklesstools` folder and clone the Suckless tools you want (e.g. `dwm`, `st`, `dmenu`
and `slock`):
```console
$ mkdir -p ~/apps/src-apps/sucklesstools
$ cd ~/apps/src-apps/sucklesstools
$ git clone git://git.suckless.org/dwm
$ git clone git://git.suckless.org/st
$ git clone git://git.suckless.org/dmenu
$ git clone git://git.suckless.org/slock
```

Now build them, and symlink their binary to the previously created `~/bin` folder:
```console
$ cd ~/apps/src-apps/sucklesstools/dwm
$ git checkout 6.2 # checkout to the last stable release (e.g. 6.2)
$ make
$ ln -s /home/user/apps/src-apps/sucklesstools/dwm/dwm /home/user/bin/

$ cd  ~/apps/src-apps/sucklesstools/st
$ git checkout 0.8.3 # checkout to the last stable release (e.g. 0.8.3)
$ make
$ ln -s /home/user/apps/src-apps/sucklesstools/st/st /home/user/bin/

$ cd ~/apps/src-apps/sucklesstools/dmenu
$ git checkout 4.9 # checkout to the last stable release (e.g. 4.9)
$ make
$ ln -s /home/user/apps/src-apps/sucklesstools/dmenu/dmenu /home/user/bin/
$ ln -s /home/user/apps/src-apps/sucklesstools/dmenu/dmenu_run /home/user/bin/
$ ln -s /home/user/apps/src-apps/sucklesstools/dmenu/dmenu_path /home/user/bin/

$ cd ~/apps/src-apps/sucklesstools/slock
$ git checkout 1.4 # checkout to the last stable release (e.g. 1.4)
$ make
$ sudo make install # you will get errors if you don't install slock this way
```

!!! Note "Note"
    After building and symlinking `dwm`, you might want to add it to your `~/.xinitrc` file in
    order to execute it automatically when running `$ startx` (for starting the `xorg` graphic
    server).

    **If** using `elogind`:
    ```console
    $ echo "exec dbus-launch --exit-with-session dwm & # see <https://wiki.gentoo.org/wiki/Elogind>" >> ~/.xinitrc
    ```
    **Else**:
    ```console
    $ echo "exec dwm &" >> ~/.xinitrc
    ```

!!! Tip "Tip"
    If you want more Suckless tools: <https://tools.suckless.org/>

!!! Tip "Tip"
    If you are interested in my fork of the Suckless tools:
    <https://gitlab.com/stephane.tzvetkov/sucklesstools>


---
## Config

???+ Note "Reference(s)"
    See [my
    config](https://gitlab.com/stephane.tzvetkov/config/-/tree/master/home%2Fuser%2Fapps%2F%src-apps%2Fsucklesstools)
    as an example.

### apply a patch

One should keep track of patches by downloading them in a folder like this one:
```console
$ mkdir ~/apps/src-apps/sucklesstools/patches
```

Find a nice patch in <https://dwm.suckless.org/patches/>, download it in
`~/apps/src-apps/sucklesstools/patches` and apply it with:
```console
$ patch -p1 < ~/apps/src-apps/sucklesstools/patches/patchname.diff
```

Or with

```console
$ git apply ~/apps/src-apps/sucklesstools/patches/patchname.diff
```

Or "by hand" reading the `patchname.diff` patch, applying the diff line by line.

Finally, do not forget to:
```console
$ git add *
$ git commit
```
In order to keep track of the modifications

### `st` 0.8.3 patches examples

Download, e.g. `alpha`, `scrollback`, `ligatures`, `keyboard-select`, `font2`, `vertcenter` and
`anysize` patches:
```console
$ cd ~/apps/src-apps/sucklesstools/patches
$ curl https://st.suckless.org/patches/alpha/st-alpha-0.8.2.diff > st-alpha-0.8.2.diff
$ curl https://st.suckless.org/patches/scrollback/st-scrollback-20200419-72e3f6c.diff > st-scrollback-20200419-72e3f6c.diff
$ curl https://st.suckless.org/patches/ligatures/0.8.3/st-ligatures-alpha-scrollback-20200430-0.8.3.diff > st-ligatures-alpha-scrollback-20200430-0.8.3.diff
$ curl https://st.suckless.org/patches/keyboard_select/st-keyboard_select-0.8.2.diff > st-keyboard_select-0.8.2.diff
$ curl https://st.suckless.org/patches/font2/st-font2-20190416-ba72400.diff > st-font2-20190416-ba72400.diff
$ curl https://st.suckless.org/patches/vertcenter/st-vertcenter-20180320-6ac8c8a.diff > st-vertcenter-20180320-6ac8c8a.diff
$ curl https://st.suckless.org/patches/anysize/st-anysize-0.8.1.diff > st-anysize-0.8.1.diff
```

* Apply the `alpha` patch:
```console
$ cd st
$ patch -p1 < ~/apps/src-apps/sucklesstools/patches/st-alpha-0.8.2.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```

* Apply the `scrollback` patch:
```console
$ cd st
$ patch -p1 < ~/apps/src-apps/sucklesstools/patches/st-scrollback-20200419-72e3f6c.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```

* Apply the `ligatures` patch (after installing `harfbuzz` dependency):
```console
$ cd st
$ git git patch -p1 ~/apps/src-apps/sucklesstools/patches/st-ligatures-alpha-scrollback-20200430-0.8.3.diff
$ make clean && make
```

* Now one might notice that applying the `keyboard-select` patch isn't possible after all the
  previous patches. In this case, patching "by hand" is the only solution: open the
  `st-keyboard_select-0.8.2.diff` patch in one editor and apply the diff line by line in an
  other.<br/>
  With the following difference...<br/>
  In `st-st-keyboard_select-0.8.2.diff` Replace:<br/>
  ```console
        xdrawcursor(term.c.x, term.c.y, term.line[term.c.y][term.c.x],
                    term.ocx, term.ocy, term.line[term.ocy][term.ocx]);
  ```
  By:
  ```console
        xdrawcursor(term.c.x, term.c.y, term.line[term.c.y][term.c.x],
                term.ocx, term.ocy, term.line[term.ocy][term.ocx],
                term.line[term.ocy], term.col);
  ```

* Also apply `font2` patch `st-font2-20190416-ba72400.diff` by hand (no differences) (note that it
  won't compile until you merge the `config.def.h` diff in `config.h`)

* Also apply `vertcenter` patch `st-vertcenter-20180320-6ac8c8a.diff` by hand (no differences)

* Also apply `anysize` patch `st-anysize-0.8.1.diff` by hand (no differences)


### customization guide

???+ Note "Reference(s)"
    - <https://dwm.suckless.org/customisation/patches_in_git/>


---
## Use

### `dwm`

???+ Note "Reference(s)"
    - <https://dwm.suckless.org/>
    - <https://dwm.suckless.org/tutorial/>

### `st`

???+ Note "Reference(s)"
    - <https://st.suckless.org/>

Demo:
```console
$ curl https://www.cl.cam.ac.uk/\~mgk25/ucs/examples/UTF-8-demo.txt
```
#### `st` troubleshooting

On Ubuntu, one might encounter the following error when trying to build `st`: `Package fontconfig
was not found in the pkg-config search path.`

In this case install the following package:
```console
# apt install libxft-dev
```

### `dmenu`

???+ Note "Reference(s)"
    - <https://tools.suckless.org/dmenu/>

### `slock`

???+ Note "Reference(s)"
    - <https://tools.suckless.org/slock/>

### Tips

!!! Tip "Tip"
    To identify x11 input key codes, the `xev` utility can be used. Just run `$ xev` in your
    terminal and enter the key in order to get its details. A lot of information will be output,
    with the following command you can start `xev` and show only the relevant parts:
    ```console
    $ xev | awk -F'[ )]+' '/^KeyPress/ { a[NR+2] } NR in a { printf "%-3s %s\n", $5, $8 }'
    ```

### `slock` troubleshooting

* If you get this error `slock: getgrnam nogroup: group entry not found`, then you might want to
  apply this:
  ```console
  $ sed -i 's|static const char \*group = "nogroup";|static const char *group = "nobody";|' config.def.h
  $ sed -ri 's/((CPP|C|LD)FLAGS) =/\1 +=/g' config.mk
  ```
  Then don't forget to change `config.h` according to the new `config.def.h`.

* If you get this error `slock: unable to disable OOM killer. Make sure to suid or sgid slock.`,
  then you might want to build `slock` like so: `$ make clean && sudo make install` to install it
  system wide and not just user wide.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
