---
tags:
  - Graphical
  - Wayland
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Wayland

Wayland is a display server protocol. It is aimed to become the successor of the [X Window System
(or X11)](https://en.wikipedia.org/wiki/X_Window_System) implemented by [Xorg (or
X)](https://en.wikipedia.org/wiki/X.Org_Server). You can find a comparison between Wayland and Xorg
on
[Wikipedia](https://en.wikipedia.org/wiki/Wayland_(display_server_protocol)#Differences_between_Wayland_and_X).

???+ Note "Reference(s)"
    - <https://wayland.freedesktop.org/>
    - <https://wiki.gentoo.org/wiki/Wayland>
    - <https://wiki.archlinux.org/title/Wayland>
    - from Xorg to Wayland: <https://github.com/swaywm/sway/wiki/i3-Migration-Guide>
    - from `dwm` to `dwl`: <https://github.com/djpohly/dwl>
    - <https://www.reddit.com/r/unixporn/comments/v7wnp6/hyprland_a_beautiful_wayland_compositor/>

---
## Table of contents


<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)

<!-- vim-markdown-toc -->

---
## Install

TODO

---
## Config

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
