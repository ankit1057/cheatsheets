---
tags:
  - Graphical
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Fonts

???+ Note "Reference(s)"
    * <https://www.programmingfonts.org/>
    * <https://www.nerdfonts.com/font-downloads>
    * <https://wiki.gentoo.org/wiki/Fonts>
    * <https://wiki.gentoo.org/wiki/Fontconfig>
    * <https://wiki.archlinux.org/index.php/Fonts>
    * <https://wiki.archlinux.org/index.php/Font_configuration>
    * <https://forums.gentoo.org/viewtopic-t-995908-start-0.html>
    * <https://github.com/source-foundry/Hack#quick-installation>
    * <https://github.com/ryanoasis/nerd-fonts>
    * <https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/3270/Medium/complete>
    * <https://askubuntu.com/questions/3697/how-do-i-install-fonts>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
    * [How to manually install a font system wide](#how-to-manually-install-a-font-system-wide)
    * [How to manually install a font user wide](#how-to-manually-install-a-font-user-wide)
    * [Better emojis support](#better-emojis-support)
    * [Nerd Fonts](#nerd-fonts)
* [Config](#config)
* [Use](#use)
    * [Troubleshooting](#troubleshooting)
        * [Noto color emoji bug](#noto-color-emoji-bug)

<!-- vim-markdown-toc -->


---
## Install

```console
$ sudo pacman -S fontconfig
```

The first reflex to have is to search the font you chose with your package manager, if it's not
there then you can install it manually. E.g. some popular fonts on Arch Linux:
```console
$ sudo pacman -S ttf-hack ttf-fira-code ttf-inconsolata ttf-jetbrains-mono ttf-ubuntu-font-family ttf-joypixels
```

!!! Tip "Tip"
    Good Unicode coverage with `inconsolata`, `symbola` and `joypixels` (or `noto-fonts-emoji`).

!!! Tip "Tip"
    For coding, I personally like `Hack Nerd Font` or `FiraCode Nerd Font`. See the [Nerd
    Fonts](#nerd-fonts) section bellow.

### How to manually install a font system wide

Download and copy `.otf` or `.ttf` file(s) in `/usr/share/fonts`, then run:
```console
$ sudo fc-cache -f -v
```

### How to manually install a font user wide

Download and copy `.otf` or `.ttf` file(s) in `~/.local/share/fonts`, then run:
```console
$ fc-cache -f -v
```

### Better emojis support

If you want emojis support in your terminal and windows manager, install
[`libxft-bgra`](https://aur.archlinux.org/packages/libxft-bgra/) (more details
[here](https://gitlab.freedesktop.org/xorg/lib/libxft/-/merge_requests/1)). E.g. for `st` and `dwm`
to render emojis.

In this case, do not hesitate to replace `libxft`:
```console
:: libxft-bgra and libxft are in conflict. Remove libxft? [y/N] y
```

### Nerd Fonts

Nerd Fonts is a project that patches developer targeted fonts with a high number of glyphs (icons).
Specifically to add a high number of extra glyphs from popular 'iconic fonts' such as [Font
Awesome](https://github.com/FortAwesome/Font-Awesome),
[Devicons](https://vorillaz.github.io/devicons/), [Octicons](https://github.com/primer/octicons),
and [others](https://github.com/ryanoasis/nerd-fonts#glyph-sets).

You can either install the Nerd Fonts you want manually (from this link:
<https://www.nerdfonts.com/font-downloads>) like describe in the above sections.

Or you can install the Nerd Fonts you want with the Nerd Fonts installer:
```console
$ mkdir -p $HOME/apps/src-apps
$ cd $HOME/apps/src-apps
$ git clone https://github.com/ryanoasis/nerd-fonts.git # cloning nerd-fonts can take some time
$ cd nerd-fonts
$ chmod +x install.sh

$ ./install.sh --help
$ ./install.sh "Hack Nerd Font" # e.g. for "Hack" local install (in `$HOME/.local/share/fonts`)
$ sudo ./install.sh -S "Hack Nerd Font" # e.g. for "Hack" system install (in `/usr/local/share/fonts`)
```


---
## Config

The different options of a font (e.g. size) can be configured depending on your windows manager and
your terminal application. E.g. for `dwm` and `st` Suckless tools: fonts are configurable in their
respective `config.h` file.


---
## Use

* List available fonts:
```console
$ fc-list
```

* List installed fonts and available font options (with an X11 point and click application):
```console
$ xfontsel
```

### Troubleshooting

#### Noto color emoji bug

See <https://bugzilla.redhat.com/show_bug.cgi?id=1498269>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
