---
tags:
  - Graphical
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Fonts

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Icons>
    * <https://www.xfce-look.org/s/XFCE/browse/cat/132/ord/rating/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Choose](#choose)
* [Install](#install)
    * [How to manually install icons system wide](#how-to-manually-install-icons-system-wide)
    * [How to manually install a font user wide](#how-to-manually-install-a-font-user-wide)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Choose

Choose an icon pack (e.g. [here](https://wiki.archlinux.org/index.php/Icons)).

---
## Install

The first reflex to have is to search the icons you chose with your package manager, if it's not
there then you can install it manually. E.g. some popular icons on Arch Linux:
```console
$ sudo pacman -S arc-icon-theme adwaita-icon-theme hicolor-icon-theme
```

### How to manually install icons system wide

Download, extract, and copy the icons pack in `/usr/share/icons`, then run:
```console
$ sudo gtk-update-icon-cache -f -t /usr/share/icons/<icons-pack-name>
```

### How to manually install a font user wide

Download, extract, and copy the icons pack in `~/.local/share/icons`, then run:
```console
$ gtk-update-icon-cache -f -t ~/.local/share/icons/<icons-pack-name>
```


---
## Config

No configuration required.


---
## Use

Select the icon theme using the appropriate configuration tool for your desktop environment or
window manager.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
