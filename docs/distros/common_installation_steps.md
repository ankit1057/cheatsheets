---
tags:
  - Linux Distributions
  - Distros
  - Installation Guide
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Common installation steps

This cheat sheet gathers common Linux distributions installation steps, in order to refactor the
content of the following cheat sheets:

* [Gentoo installation](./gentoo-based/gentoo_installation.md)
* [Arch installation](./arch-based/arch_installation.md)
* [Artix installation](./arch-based/artix_installation.md)
* [NixOS installation](./nixos-based/nixos_installation.md)

???+ Note "Reference(s)"
    * <https://www.youtube.com/watch?v=xysISs0mcj8>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Prerequisites](#prerequisites)
    * [Bootable media](#bootable-media)
    * [After booting your live ISO image on a bootable media](#after-booting-your-live-iso-image-on-a-bootable-media)
* [Optional disk encryption: drive preparation](#optional-disk-encryption-drive-preparation)
    * [Drive preparation](#drive-preparation)
* [Partitioning, formatting and mounting](#partitioning-formatting-and-mounting)
    * [GPT (not MBR) and (U)EFI (not BIOS) partitioning, formatting and mounting](#gpt-not-mbr-and-uefi-not-bios-partitioning-formatting-and-mounting)
    * [MBR (not GPT) and BIOS (not (U)EFI) partitioning, formatting and mounting](#mbr-not-gpt-and-bios-not-uefi-partitioning-formatting-and-mounting)
* [Optional disk encryption: partition preparation](#optional-disk-encryption-partition-preparation)
    * [Root partition preparation](#root-partition-preparation)
* [Optional disk encryption: mkinitcpio configuration](#optional-disk-encryption-mkinitcpio-configuration)
* [Optional disk encryption: boot encryption](#optional-disk-encryption-boot-encryption)
* [Bootloading with GRUB](#bootloading-with-grub)
* [Optional disk encryption: GRUB2 bootloader configuration](#optional-disk-encryption-grub2-bootloader-configuration)
        * [If UEFI + systemd](#if-uefi-systemd)
    * [Else](#else)
* [Optional disk encryption: swap encryption](#optional-disk-encryption-swap-encryption)
* [Post installation recommended configuration](#post-installation-recommended-configuration)
    * [ldconfig](#ldconfig)
    * [/etc/environment ?](#etcenvironment-)
    * [/etc/profile ? (only for bash? <https://superuser.com/questions/664169/what-is-the-difference-between-etc-environment-and-etc-profile>)](#etcprofile-only-for-bash-httpssuperusercomquestions664169what-is-the-difference-between-etc-environment-and-etc-profile)
    * [`/etc/locale.conf` ?](#etclocaleconf-)
    * [`/etc/locale.gen` ?](#etclocalegen-)
    * [`/etc/localtime` ?](#etclocaltime-)
* [Post installation optional software](#post-installation-optional-software)

<!-- vim-markdown-toc -->

---
## Prerequisites

### Bootable media

Follow [this cheat sheet about bootable media](../hardware/bootable_media.md) in order to install
the ISO image on a USB drive and **boot on it with UEFI** (see also [this related Arch wiki
article](https://wiki.archlinux.org/title/USB_flash_installation_medium)).

!!! Warning "Warning"
    **Most bootable USB drive wont support Secure Boot!** You will need to [disable Secure
    Boot](https://web.archive.org/web/20220522042243/https://www.rodsbooks.com/efi-bootloaders/secureboot.html#disable)
    from the host computer before booting on the USB drive. Note that you can optionally [set up
    Secure Boot](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot) after
    completing the installation.

### After booting your live ISO image on a bootable media

* **Check the keyboard mapping (keymap)**. The default console keymap is the US one. So, if the
  keyboard layout feels wrong, then it can be changed like so:
    ```console
    # loadkeys fr # e.g. switch the keyboard mapping to French (or any other language) if needed
    ```

    Note that available keymaps can be listed usually with:
    ```console
    # ls /usr/share/kbd/keymaps/**/*.map.gz
    ```
    or with
    ```
    # ls /usr/share/keymaps/**/*.map.gz
    ```

* **Optionally start a graphical environment**. The live installation ISO image you are using might
  provide a graphical environment that you are willing to use, in that case run the following
  command:
    ```console
    # startx
    ```
    At this point the keyboard layout might feel wrong again. This might be due to the Xorg
    keyboard configuration, it can be changed like so:
    ```console
    # setxkbmap fr # e.g. to switch to French (or any other language) if needed
    ```

* **Verify the boot mode**:
    ```console
    # ls /sys/firmware/efi/efivars
    ```

    If the previous command achieves to list the `efivars` directory without error, then the system
    is booted in UEFI mode. If the directory does not exist, the system may be booted in BIOS (or
    CSM) mode. If the system did not boot in the mode you desired, refer to your motherboard's
    manual.

* **Update the system clock**:

    !!! Note ""

        === "SystemD"
            ```console
            # timedatectl set-ntp true
            # timedatectl set-timezone Region/City
            ```

        === "SystemD and non-SystemD"
            **TODO**
            ```console
            # ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
            ```

* **Connect to the internet**. To set up a network connection in the live environment, go through
  the following steps:

    * Ensure your network interface is listed and enabled:
        ```console
        # ip link
        ```

    * After booting on the live ISO image, you should easily have access to the internet with an
      Ethernet connection: just plug-in an Ethernet cable and GNU/Linux will do the rest. If you
      don't have access to an Ethernet cable or if you rather prefer a wireless connection, then
      just have a look at the wireless network settings of your live ISO image.

    * Don't forget to setup a proxy if needed,<br/>
      e.g. `$ export http_proxy="http://192.168.1.100:4242"`<br/>
      and `$ export https_proxy="http://192.168.1.100:4242"`.<br/>
      ⚠️ Remember that with a proxy: `ping` may not work! ⚠️

    * The connection may be verified with `ping` (or `curl` if `ping` does not work):
        ```console
        $ ping searx.space
        $ curl -sSL searx.space
        ```

    * If the previous steps are not working without trouble, then you will have to look deeper
      depending on the network manger used on the live ISO image. Find which network manager is
      used, and configure it yourself (see [network managers](../networks/network_managers.md) for
      more details).

    * Note: If you don't have access to an Ethernet connection, and need a wireless one (WLAN or
      WWAN), then first, make sure your wireless device is not blocked:

        ```console
        $ rfkill
        $ rfkill list
        ```

        And configure your wireless connection by following one of the below links:
        - ⭐️<https://wiki.archlinux.org/title/Iwd#iwctl>
        - <https://wiki.archlinux.org/title/Network_configuration/Wireless>
        - <https://wiki.archlinux.org/title/Wpa_supplicant>
        - <https://wiki.archlinux.org/title/Category:Wireless_networking>

---
## Optional disk encryption: drive preparation

This section is optional, you can skip it if your are no interested in encrypting your disk.

A lot of different encryption scenarios are available (e.g. see
<https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system>), but in this guide will
focus on the most simple and straightforward one: full system encryption with `dm-crypt` + `LUKS`
in a basic partition layout. The partition layout is just an non-encrypted boot partition (`/boot`
on `/dev/sdx1`), an non-encrypted SWAP partition (on `/dev/sdx2`, that can be encrypted later) and
an encrypted root partition (`/` on `/dev/sdx3`).

!!! Warning "Warning"
    This scenario will completely wipe the content of your disk, including all prior partition(s)
    and data (if any) it might have contained. Do appropriate backups if needed before starting!

### Drive preparation

Before encrypting a drive, it is recommended to perform a secure erase of the disk by overwriting
the entire drive with random data. To prevent cryptographic attacks or unwanted file recovery.

!!! Note "Note"
    When wiping large amount of data, the process can take several hours to several days to
    complete.

!!! Warning "Warning"
    If you intend to encrypt a SSD drive, then you will have first to clean it: see
    <https://wiki.archlinux.org/title/Solid_state_drive/Memory_cell_clearing>

First, create a temporary encrypted container on the device to be encrypted:
```console
# cryptsetup open --type plain -d /dev/urandom /dev/sdx to_be_wiped
```

You can verify that it exists:
```console
# lsblk

NAME          MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINT
sdx             8:0    0  1.8T  0 disk
└─to_be_wiped 252:0    0  1.8T  0 crypt
```

Wipe the container with zeros. A use of `if=/dev/urandom` is not required as the encryption cipher
is used for randomness.
```console
# dd if=/dev/zero of=/dev/mapper/to_be_wiped status=progress bs=1M
```

Finally, close the temporary container:
```console
# cryptsetup close to_be_wiped
```

---
## Partitioning, formatting and mounting

Here you have a choice to make: either go GPT + (U)EFI or go MBR + BIOS.

What I recommend is to go MBR + BIOS **only if** you have a *very* old computer (in this case make
sure it is not UEFI compatible) **or if** you use a virtual machine (VM) not supporting UEFI.
**Else** go GPT + (U)EFI.

!!! Warning "Warning"
    You might already have a Linux distro installed on another partition, in this case **make sure
    to NOT override it when partitioning**. As described at the end of this cheat sheet, the
    `os-prober` tool will find this partition and make it also available through GRUB.

!!! Tip "Tip"
    If you want to resize a partition in order to make some place for another partition (e.g. to
    install your distro in dual-boot with another distro), then you can refer to [this cheat sheet
    about partitioning e.g. with `gnome-parted`](../drives/partitions.md#gnome-parted) **after
    booting on the USB drive**.

### GPT (not MBR) and (U)EFI (not BIOS) partitioning, formatting and mounting

!!! Note "Info"
    The suggested tool used here to partition the disks is `gdisk` (but of course, you can use the
    tool you want). See:

    * [partitions](../drives/partitions.md)
    * [`gdisk`](https://wiki.archlinux.org/index.php/Gdisk)
    * [`gdisk` examples](https://debian-facile.org/doc:systeme:gdisk)

* Check which disk to partition with the `# lsblk` command. In this example, the disk to partition
  will be called `/dev/sdx` (note that in a virtual machine, it's likely to be called `/dev/sda` or
  `/dev/vda`).

* In this example, three partitions will be created on `sdx`:
    * `sdx1` : EFI system boot (512 MB) (OPTIONAL: see "OPTIONAL EFI system boot partition" tip bellow)
    * `sdx2` : Main file system
    * `sdx3` : Swap (OPTIONAL: see "OPTIONAL Swap" tip bellow)

    !!! Tip "OPTIONAL EFI system boot partition"
        **If an EFI system boot partition already exists** for another GNU/Linux distribution
        installed on the same disk, then you can use that partition without corrupting the other
        distribution. A single EFI system boot partition can be used for several systems.

    !!! Tip "OPTIONAL Swap"
        If a Swap partition already exists for another GNU/Linux distribution installed
        on the same disk, then you can use that partition without corrupting the other
        distribution. A single Swap partition can be used for several systems.

        Also, Swap is sometimes considered optional if you have enough RAM (> 8-16 GB), but
        necessary to hibernate (in this case you need as much Swap as RAM).

* But first, **backup your partition table**:
    ```console
    # sfdisk -d /dev/disk > disk.dump
    ```

    !!! Tip "Tip"
        If you want to restore your partition table later, run: `# sfdisk /dev/disk < disk.dump`

* Now proceed with the partitioning:
    ```console
    # gdisk /dev/sdx # gpt partitioning (alternative : parted)
        > ? # list all commands
        > p # list all partitions
        > d # delete a partition

        > o # create new GPT (⚠️ only if the partition table scan didn't find any valid GPT)
            > Y # validate new empty GPT (⚠️ delete all pre-existing partitions)

        > n # create a new partition (boot partition)
            > # enter to leave default part number (1 for sdx1)
            > # enter to leave default first sector
            > +512M # specify a 512MB partition size
            > ef00 # specify an EFI system boot partition type

        > n # create new partition
            > # enter to leave default part number (2 for sdx2)
            > # enter to leave default first sector
            > -16G # get all space left except for the last 16GB for the RAM
            > 8304 #specify a Linux root partition type

        > n # create new partition (optional swap)
            > # enter to leave default part number (3 for sdx3)
            > # enter to leave default first sector
            > # enter to get all space left (16GB) for the RAM
            > 8200 # specify a Linux Swap partition type

        > p # print the table to ensure that it is correct

        > w # write the table, validate all previous actions and quit
            > Y # validate new table
    ```

* Format and mount the partitions:

    !!! Warning "Warning"
        Only format the EFI system boot partition if you created it in the previous step (when
        partitioning). If there already was an EFI system boot partition on disk beforehand,
        reformatting it can destroy the boot loaders of other installed operating systems.

    ```console
    # mkfs.fat -F32 /dev/sdx1           # ⚠️ ONLY IF an EFI system boot partion has been previously created
    # mkfs.ext4 -L ROOT /dev/sdx2       # e.g. ext4 for the Main file system partition

    # mkswap -L SWAP /dev/sdx3          # ONLY IF a Swap partion has been previously created
    # swapon /dev/sdx3                  # mount swap (ONLY IF a Swap partion has been previously created)

    # mount --mkdir /dev/sdx2 /mnt/distro-name         # mount main partition

    # mount --mkdir /dev/sdx1 /mnt/distro-name/boot    # mount boot partition
    ```

### MBR (not GPT) and BIOS (not (U)EFI) partitioning, formatting and mounting

!!! Note "Info"
    The suggested tool used here to partition the disks is `fdisk` (but of course, you can use the
    tool you want). See:

    * [partitions](../drives/partitions.md)
    * [`fdisk`](https://wiki.archlinux.org/index.php/Fdisk)

* Check which disk to partition with the `# lsblk` command. In this example, the disk to partition
  will be called `/dev/sdx` (note that in a virtual machine, it's likely to be called `/dev/sda` or
  `/dev/vda`).

* In this example, two partitions will be created on `sdx`:
    * `sdx1` : Swap (OPTIONAL: see "OPTIONAL Swap" tip bellow)
    * `sdx2` : Main file system

    !!! Tip "OPTIONAL Swap"
        If a Swap partition already exists for another GNU/Linux distribution installed
        on the same disk, then you can use that partition without corrupting the other
        distribution. A single Swap partition can be used for several systems.

        Also, Swap is sometimes considered optional if you have enough RAM (> 8-16 GB), but
        necessary to hibernate (in this case you need as much Swap as RAM).

* But first, **backup your partition table**:
    ```console
    # sfdisk -d /dev/disk > disk.dump
    ```

    !!! Tip "Tip"
        If you want to restore your partition table later, run: `# sfdisk /dev/disk < disk.dump`

* Now proceed with the partitioning:
    ```console
    # fdisk /dev/sdx
        > m # help screen

        > o # create new MBR (⚠️ only if the partition table scan didn't find any valid MBR)
            > Y # validate new empty MBR (⚠️ delete all pre-existing partitions)

        > n # create a new partition: the Main file system partition
            > # press enter to leave default partition type: primary
            > 2 # press 2 to indicate partition number 2
            > # press enter to leave default first sector: 2048
            > -8G # OPTIONAL leave 8GB for the Swap or just press enter to leave default last sector
            > Y # remove signature if any

        > a # toogle the bootable flag
            > 2 # press 2 to indicate partition number 2

        > n # [OPTIONAL] create a new partition: the Swap partition
            > # press enter to leave default partition type: primary
            > 1 # press 1 to indicate partition number 1
            > # press enter to leave default first sector
            > # press enter to leave default last sector

            > t # change the partition type of the Swap from default "Linux" to "Linux swap / Solaris"
            > 1 # select the partition number of the Swap
            > 82 # enter the hex code of the "Linux swap / Solaris"

        > w # write the table to the disk and exit
    ```

* Format and mount the partitions:
    ```console
    # mkfs.ext4 -L ROOT /dev/sdx2 # e.g. ext4 for the Main file system partition

    # mkswap -L SWAP /dev/sdx1    # ONLY IF a Swap partion has been previously created
    # swapon /dev/sdx1            # mount swap (ONLY IF a Swap partion has been previously created)

    # mount --mkdir /dev/sdx. /mnt/distro-name   # mount main partition
    ```


---
## Optional disk encryption: partition preparation

This section is optional, you can skip it if your are no interested in encrypting your disk.

### Root partition preparation

The following commands create and mount the encrypted root partition:
```console
# umount /mnt/distro-name
# cryptsetup -y -v luksFormat /dev/sdx2
# cryptsetup open /dev/sdx2 root
# mkfs.ext4 /dev/mapper/root
# mount --mkdir /dev/mapper/root /mnt/distro-name
```

Check the mapping works as intended:
```console
# umount /mnt/distro-name
# cryptsetup close root
# cryptsetup open /dev/sdx2 root
# mount /dev/mapper/root /mnt/distro-name
```


---
## Optional disk encryption: mkinitcpio configuration

*after chroot*

* If using a systemd-based initramfs, the below command should returns "OK":

    ```console
    $ cat /etc/mkinitcpio.conf | grep -q "^HOOKS=(base systemd" && echo OK || echo KO
        > OK
    ```
    
    Then, add the `keyboard` and `sd-encrypt` hooks to `mkinitcpio.conf` (and if you use a non-US
    console keymap or a non-default console font, additionally add the `sd-vconsole` hook):

    ```console
    # vi /etc/mkinitcpio.conf
        > ...
      ~ > HOOKS=(base systemd autodetect modconf kms keyboard sd-vconsole block sd-encrypt filesystems fsck)
        > ...
    ```

* Else, if using a non systemd-based initramfs, the below command should return "OK":

    ```console
    $ cat /etc/mkinitcpio.conf | grep -q "^HOOKS=(base udev" && echo OK || echo KO
        > OK
    ```

    Then, add the `keyboard` and `encrypt` hooks to `mkinitcpio.conf` (and if you use a non-US
    console keymap or a non-default console font, additionally add the `keymap` and `consolefont`
    hooks, respectively):

    ```console
    # vi /etc/mkinitcpio.conf
        > ...
      ~ > HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block encrypt filesystems fsck)
        > ...
    ```

* Finaly, in any case, regenerate the initramfs:
    ```console
    # mkinitcpio -P
    ```


---
## Optional disk encryption: boot encryption

See
<https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Encrypted_boot_partition_(GRUB)>

!!! Warning "Warning"
    GRUB's support for LUKS2 is limited (see
    [here](https://wiki.archlinux.org/title/GRUB#Encrypted_/boot) for more details). Use LUKS1 for
    partitions that GRUB will need to unlock.


---
## Bootloading with GRUB

A bootloader (also spelled as boot loader or called boot manager and bootstrap loader) is a
computer program that is responsible for booting a computer.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/Window_manager>
    * <https://wiki.archlinux.org/title/Category:Boot_loaders>
    * <https://wiki.archlinux.org/title/GRUB>
    * <https://wiki.archlinux.org/title/GRUB/EFI_examples>
    * <https://wiki.archlinux.org/title/GRUB/Tips_and_tricks>
    * <https://wiki.archlinux.org/title/GRUB_Legacy>
    * <https://wiki.archlinux.org/title/EFISTUB>
    * <https://wiki.archlinux.org/title/Systemd-boot>

!!! Warning "Warning"
    At this point, if you want your GRUB menu to also add an entry for any Linux distro already
    installed on any other partition: **make sure to mount it** (it "just" need to be mounted for
    `os-prober` to find it)!

!!! Note ""

    === "GPT + UEFI"
        **Make sure that the `/boot` partition is created and mounted**:
        ```console
        # df -aTh # check mounted devices
        # mount --mkdir /dev/sdx1 /boot # if not already mounted
        ```

        Install GRUB tools:

        !!! Note ""

            === "apk"
                ```console
                # apk add grub os-prober efibootmgr
                ```

            === "apt"
                ```console
                # apt install grub os-prober efibootmgr
                ```

            === "dnf"
                ```console
                # dnf install grub2 os-prober efibootmgr
                ```

            === "emerge"
                ```console
                # emerge -a sys-boot/grub sys-boot/os-prober sys-boot/efibootmgr
                ```

            === "nix"

                === "on NixOS"
                    ```console
                    # nix-env -iA nixos.grub nixos.os-prober nixos.efibootmgr
                    ```

                === "on non-NixOS"
                    ```console
                    # nix-env -iA nixpkgs.grub nixpkgs.os-prober nixpkgs.efibootmgr
                    ```

            === "pacman"
                ```console
                # pacman -S grub os-prober efibootmgr
                ```

            === "yum"
                ```console
                # yum install grub2 os-prober efibootmgr
                ```

            === "xbps"
                ```console
                # xbps-install -S grub os-prober efibootmgr
                ```

            === "zypper"
                ```console
                # zypper install grub os-prober efibootmgr
                ```

        Install GRUB:
        ```console
        # grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
        ```

        !!! Note "Note"
            The last command should have created a `EFI` folder in `/boot` directory

        **If** this error message shows up: `Could not delete variable: Read-only file sys`, then
        remount efivars with read/write rights and run `grub-install` again:
        ```console
        # mount -o remount,rw /sys/firmware/efi/efivars
        # grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
        ```

    === "MBR + BIOS"
        Install GRUB tools:

        !!! Note ""

            === "apk"
                ```console
                # apk add grub os-prober efibootmgr
                ```

            === "apt"
                ```console
                # apt install grub os-prober efibootmgr
                ```

            === "dnf"
                ```console
                # dnf install grub2 os-prober efibootmgr
                ```

            === "emerge"
                ```console
                # emerge -a sys-boot/grub sys-boot/os-prober sys-boot/efibootmgr
                ```

            === "nix"

                === "on NixOS"
                    ```console
                    # nix-env -iA nixos.grub nixos.os-prober nixos.efibootmgr
                    ```

                === "on non-NixOS"
                    ```console
                    # nix-env -iA nixpkgs.grub nixpkgs.os-prober nixpkgs.efibootmgr
                    ```

            === "pacman"
                ```console
                # pacman -S grub os-prober efibootmgr
                ```

            === "yum"
                ```console
                # yum install grub2 os-prober efibootmgr
                ```

            === "xbps"
                ```console
                # xbps-install -S grub os-prober efibootmgr
                ```

            === "zypper"
                ```console
                # zypper install grub os-prober efibootmgr
                ```

        Install GRUB:
        ```console
        # grub-install --recheck /dev/sdx
        ```

Then, generate the GRUB configuration:
```console
# grub-mkconfig -o /boot/grub/grub.cfg
```

!!! Warning "Warning"
    After running the previous command, if you get the following message:
    ```console
    Warning: os-prober will not be executed to detect other bootable partitions
    ```

    And if you want to be able to boot multiple distros from your GRUB menu, then edit the
    `/etc/default/grub` file and uncomment the following line:
    ```console
    GRUB_DISABLE_OS_PROBER=false
    ```

    Now re-generate the GRUB configuration:
    ```console
    # grub-mkconfig -o /boot/grub/grub.cfg
    ```

    See <https://askubuntu.com/a/1378891>.

!!! Note "Info"
    The last command should have created a `grub` folder in the `/boot` directory. And it should
    have print something like:
    ```console
    Generating grub configuration file ...
    Found linux image: /boot/vmlinuz-linux
    Found initrd image: /boot/initramfs-linux.img
    Found fallback initrd image(s) in /boot: initramfs-linux-fallback.img
    Warning: os-prober will be executed to detect other bootable partitions.
    Its output will be used to detect bootable binaries on them and create new boot entries.
    Adding boot menu entry for UEFI Firmware Settings ...
    done
    ```
    `grub-mkconfig` will use `os-prober` in order to find any other installed OS and make sure you
    will still be able to boot on them from the GRUB menu.

!!! Tip "EFI payload location"
    You can check your GRUB's EFI payload location with:
    ```console
    # find /boot/EFI -type f -iname "*.efi"
    ```
    Should return something like: `./EFI/grub/grubx64.efi`

!!! Bug "First reboot with UEFI if on a VM"

    ???+ Note "Reference(s)"
        * <https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface#bcfg>
        * <https://unix.stackexchange.com/questions/530674/qemu-doesnt-respect-the-boot-order-when-booting-with-uefi-ovmf>
        * <https://docstore.mik.ua/manuals/hp-ux/en/5991-1247B/ch04s13.html>
    
    After your first reboot on a UEFI virtual machine, you might see that your VM is not finding any
    correct boot option. It might try to boot over the network (PXE boot): wait until it fallbacks to
    the UEFI Shell (it will take some minutes). You should en up in a similar environment:
    ```console
    UEFI Interactive Shell v2.2
    EDK II
    UEFI v2.70 (EDK II, 0x00010000)
    Mapping table
          FS0: Alias(s):HD1a65535a1:;BLK3:
              PciRoot(0x0)/Pci(0x4,0x0)/Sata(0x0,0xFFFF,0x0)/HD(1,MBR,0xBE1AFDFA,0x3F,0xFBFC1)
         BLK0: Alias(s):
              PciRoot(0x0)/Pci(0x1,0x0)/Floppy(0x0)
         BLK1: Alias(s):
              PciRoot(0x0)/Pci(0x1,0x0)/Floppy(0x1)
         BLK2: Alias(s):
              PciRoot(0x0)/Pci(0x4,0x0)/Sata(0x0,0xFFFF,0x0)
    
    Press ESC in 1 seconds to skip startup.nsh or any other key to continue.
    Shell>
    ```
    
    At this point, a new boot menu entry is needed for the previous GRUB configuration:
    ```console
    Shell> map # list
    Shell> bcfg boot dump -v
    Shell> bcfg boot add 0 FS0:\EFI\grub\grubx64.efi "distro-name_vm_boot"
    Shell> exit
    ```
    
    With:
    
    * `map`: list devices name (in this example `FS0` is the device map name that will be used).
    * `bcfg boot dump -v`: list current boot entries.
    * `bcfg boot add`: add a new boot entry.
    * `0`: where to insert (0 is the first boot option).
    * `FS0`: device map name
    * `\EFI\grub\grubx64.efi`: EFI payload location in device's `/boot` directory (see the last tip of the [previous section](#bootloading-with-grub)).
    * `"distro-name_vm_boot"`: an alias to your entry.
    
    Now you can reboot without problems (hopefully).


---
## Optional disk encryption: GRUB2 bootloader configuration

```console
[chroot] # vi /etc/default/grub
    > GRUB_ENABLE_CRYPTODISK=y
```

#### If UEFI + systemd

On UEFI-booted systems, if specific conditions are met,
[systemd-gpt-auto-generator](https://wiki.archlinux.org/title/Systemd#GPT_partition_automounting)
will automount GPT partitions following the Discoverable Partitions Specification. Automounted
partitions can thus be omitted from `/etc/fstab`, and if the root partition is automounted, then
additional [kernel command line
parameters](https://www.kernel.org/doc/html/v4.14/admin-guide/kernel-parameters.html) (like
`root=`) can be omitted.

The prerequisites are:

- The boot loader must set the [LoaderDevicePartUUID](https://systemd.io/BOOT_LOADER_INTERFACE/)
  EFI variable, so that the used EFI system partition can be identified. This has been done by GRUB
  with `grub-mkconfig` in order to generate `grub.cfg` (note that custom `grub.cfg` requires
  loading the `bli` module). This can be verified by running `bootctl` and checking the status of
  Boot loader sets ESP information.
- The root partition must be on the same physical disk as the used EFI system partition. Other
  partitions that will be automounted must be on the same physical disk as the root partition. This
  basically means that all automounted partitions must share the same physical disk with the ESP.


```console
# lsblk -p -o NAME,PARTTYPE
```

- Make sure your Root partition has the following GUID: `4f68bce3-e8cd-4db1-96e7-fbcaf984b709`
  (`SD_GPT_ROOT_X86_64`)
- Make sure your SWAP partition (if any) has the following GUID:
  `0657fd6d-a4ab-43c4-84e5-0933c84b4f4f` (`SD_GPT_SWAP`)
- Make sure your EFI system partition has the following GUID:
  `c12a7328-f81f-11d2-ba4b-00a0c93ec93b` (`SD_GPT_ESP`)

See <https://superuser.com/questions/1427654/change-partitions-type-guid>

At this point, those prerequisites should be met and no additional configuration should be
required.

### Else

- First, identify the UUID of your root partition: `ls -la /dev/disk/by-uuid` (e.g.
  `12345678-9abc-def0-1234-56789abcdef0`)

**If** using a systemd-based initramfs (i.e. the `keyboard` and `sd-encrypt` hooks have been added
to `mkinitcpio.conf`), then add `rd.luks.name=12345678-9abc-def0-1234-56789abcdef0=root
root=/dev/mapper/root` to `GRUB_CMDLINE_LINUX_DEFAULT` in `/etc/default/grub`:

    ```console
    # vi /etc/default/grub
        > ...
      ~ > GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet rd.luks.name=12345678-9abc-def0-1234-56789abcdef0=root root=/dev/mapper/root"
        > ..;
    # grub-mkconfig -o /boot/grub/grub.cfg
    ```

**Else**, if using a non systemd-based initramfs (i.e. the `keyboard` and `encrypt` hooks have been
added to `mkinitcpio.conf`), then add `cryptdevice=UUID=12345678-9abc-def0-1234-56789abcdef0:root
root=/dev/mapper/root` to `GRUB_CMDLINE_LINUX_DEFAULT` in `/etc/default/grub`:

    ```console
    # vi /etc/default/grub
        > ...
      ~ > GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet cryptdevice=UUID=12345678-9abc-def0-1234-56789abcdef0:root root=/dev/mapper/root"
        > ..;
    # grub-mkconfig -o /boot/grub/grub.cfg
    ```


---
## Optional disk encryption: swap encryption

<https://wiki.archlinux.org/title/Dm-crypt/Swap_encryption#Without_suspend-to-disk_support>


---
## Post installation recommended configuration

### ldconfig

ldconfig creates the necessary links and cache to the most recent shared libraries found in the
directories specified on the command line, in the file `/etc/ld.so.conf`, and in the trusted
directories `/lib` and `/usr/lib` (see <https://linux.die.net/man/8/ldconfig>).

The configuration file of ldconfig is `/etc/ld.so.conf`, and here is its default content:

```console
# Dynamic linker/loader configuration.
# See ld.so(8) and ldconfig(8) for details.

include /etc/ld.so.conf.d/*.conf
```

By default it will include all the paths contained in the `.conf` files located in the
`/etc/ld.so.conf.d/` directory.

Now, let's make sure that `/usr/local/lib` is part of the configuration file of the dynamic
linker/loader. The recommanded way to do this, is to create a new `.conf` file, containing the
`/usr/local/lib` path, in the `/etc/ld.so.conf.d/` directory:

```console
$ sudo vi /etc/ld.so.conf.d/99local.conf
  + > /usr/local/lib
```

This way, it allows you to keep your local/custom libraries paths separate from paths set by the
system. The "99" prefix is there to make sure the file is loaded last compared to other files
there, so that it won't preempt system paths that could contain the same libraries.

After modifying/creating dynamic linker/loader configuration file(s), you need to run `ldconfig` as root:

```console
$ sudo ldconfig -v
```

This command updates the `/etc/ld.so.cache` file, which is the actual file used by the dynamic
linker.

See <https://stackoverflow.com/a/17892806>.


<!--

### /etc/environment ?

* `LD_LIBRARY_PATH` ?

### /etc/profile ? (only for bash? <https://superuser.com/questions/664169/what-is-the-difference-between-etc-environment-and-etc-profile>)

### `/etc/locale.conf` ?

### `/etc/locale.gen` ?

### `/etc/localtime` ?
-->

---
## Post installation optional software

Here are some tools that I suggest for a nice little starter environment:

* [`sudo`](../admin/sudo.md)
* [network time synchronization](../networks/time/network_time_synchronization.md)
* [a network manager](../networks/network_managers.md) (if not already installed and configured)
* [a system logger](../logs/system_loggers.md) (if not already installed and configured)
* [a firewall](../networks/firewalls.md)
* [fonts](../graphical/fonts.md)
* [icons](../graphical/icons.md)
* [XDG](../shells/xdg.md)
* [Zsh](../shells/zsh.md)
* [`zim`](../shells/zim.md)
* [Git](../versioning/git.md)
* [configuration files management with bare Git ](../versioning/bare_git.md)
* [Dash](../shells/dash.md)
* [aliases](../shells/aliases.md)
* [SSH](../networks/ssh.md)
* [keychain](../security/keychain.md)
* [`fasd`](../shells/fasd.md)
* [`direnv`](../admin/direnv.md)
* [`eza`](../productivity/eza.md)
* [`bat`](../productivity/bat.md)
* [`fd`](../files/fd.md)
* [zoxide](../productivity/zoxide.md)
* [`ripgrep`](../files/ripgrep.md)
* [`ripgrep-all`](../files/ripgrep-all.md)
* [fzf](../productivity/fzf.md)
* [Neovim](../productivity/neovim.md) and/or [vscodium](https://vscodium.com/)

If you intend to use your freshly installed distro in a desktop-oriented fashion (not a server one)
then you might also be interested in a graphical server, a windows manager/compositor, and a sound
system:

* Graphical server:

    * [Xorg](../graphical/xorg.md)
        * [Windows manager for Xorg](https://wiki.archlinux.org/title/Window_manager), e.g.
          [awesome](../graphical/awesome.md) or [dwm](../graphical/suckless_tools.md#dwm).
        * Some [Suckless tools](../graphical/suckless_tools.md#dwm).
            * A terminal, e.g. [st]()
            * A launcher, e.g. [dmenu]()
            * A notification daemon, e.g. [dunst]()
            * A session locker, e.g. [slock]()

    * [Wayland](https://wiki.archlinux.org/index.php/Wayland)
        * [Compositor for Wayland](https://wiki.archlinux.org/title/Wayland#Compositors), e.g.
          [Sway](https://github.com/swaywm/sway) or
          [SwayFx](https://github.com/WillPower3309/swayfx).
        * [Display managers for
          Wayland](https://wiki.archlinux.org/title/Wayland#Display_managers), e.g.
          [lemur](https://github.com/coastalwhite/lemurs) or
          [greetd](https://github.com/kennylevinsen/greetd).
        * A terminal, e.g. [alacritty](https://github.com/alacritty/alacritty)
        * A launcher, e.g. [?]()
        * A notification daemon, e.g. []()
        * A session locker, e.g. []()
        * See also <https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway>
        * [Xwayland](https://wiki.archlinux.org/title/Wayland#Xwayland)
        * [Input remap utilities](https://wiki.archlinux.org/title/Input_remap_utilities)
        * [Troubleshooting](https://wiki.archlinux.org/title/Wayland#Troubleshooting)

* [Sound system](../sound/sound.md) (e.g. PipeWire and `pulsemixer` as sound arch server and mixer,
  see also other [PulseAudio front
  ends](https://wiki.archlinux.org/index.php/PulseAudio#Front-ends) and/or [ALSA front
  ends](https://github.com/opensrc/alsa/blob/master/lib/md/AlsaMixers.md) alternatives).

Here are some more suggestions:

* [flameshot](../misc/flameshot.md)
* [`kahl`, `khard` and `todoman`](../productivity/khal_khard_todoman.md)
* [evolution](../productivity/evolution.md) and/or [Neomutt](../productivity/neomutt.md)
* [`tmux`](../productivity/tmux.md)
* [`redshift`](../graphical/redshift.md)
* [Firefox](../misc/firefox.md)
* [BlueZ](../networks/bluez.md)


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
