---
tags:
  - System Administration
  - Sysadmin
  - Linux Distributions
  - Distros
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Ansible

Ansible is a configuration management system written in Python. It can be used for automating
machine deployments.

???+ Note "Reference(s)"
    * <https://docs.ansible.com/ansible/latest/>
    * <https://wiki.gentoo.org/wiki/Ansible>
    * <https://wiki.archlinux.org/index.php/Ansible>
    * <https://www.ansible.com/resources/videos/quick-start-video>
    * <https://www.ansible.com/>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

**TODO**


---
## Config

**TODO**


---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
