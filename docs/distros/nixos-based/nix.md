---
tags:
  - Linux Distributions
  - Distros
  - NixOS
  - Package Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Nix

Nix is a purely functional package manager that aims to make package management reliable and
reproducible.

!!! Tip
    Although this cheat sheet is categorized as "NixOS-based", the Nix package manager can be
    installed on any distribution.

???+ Note "Reference(s)"
    * <https://nixos.org/manual/nix/stable/>
    * <https://nixos.org/guides/nix-pills/index.html>
    * <https://nixos.org/manual/nixpkgs/stable/>
    * <https://nixos.wiki/wiki/Flakes>
    * <https://nixos.org/download.html#download-nix>
    * <https://nixos.wiki/wiki/Nix_Installation_Guide>
    * <https://wiki.archlinux.org/title/Nix>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

???+ Note "Reference(s)"
    * <https://nixos.org/download.html#download-nix>
    * <https://nixos.wiki/wiki/Nix_Installation_Guide>

!!! Warning "Prerequisite(s)"
    * [Git](../../versioning/git.md)


* "Multi-user" installation (recommended):
```console
$ sh <(curl -L https://nixos.org/nix/install) --daemon
```

---
## Config

* Switch to a previous Nix version (e.g. `2.4`):
    ```console
    $ cd /tmp
    $ nix build 'nix/2.4'
    $ nix-env -i ./result
    ```
    Then in a new terminal (or after `$ hash -r` in a Bash shell, or `$ rehash` in a Zsh shell):
    ```console
    $ nix --version
        > nix (Nix) 2.4
    ```

* **TODO**: Switch to NixOS Nix version:

---
## Use

* Get help:
    ```console
    $ nix --help
    ```

* Search a Nix package:
    ```console
    $ nix-env -qaP '.*package_name.*'
    ```
    or
    ```console
    $ nix search package_name
    ```

* Show a package description:
    ```console
    $ nix-env -qa --description '.*package_name.*'
    ```

**WIP**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
