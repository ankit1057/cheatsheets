---
tags:
  - Linux Distributions
  - Distros
  - Red Hat Based
  - Package Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# YUM

YUM

**TODO**

???+ Note "Reference(s)"
    * <https://linuxize.com/post/how-to-install-updates-on-centos/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Use](#use)
    * [System update](#system-update)
    * [YUM Utils](#yum-utils)
    * [EPEL](#epel)
    * [Prevent packages from being updated](#prevent-packages-from-being-updated)

<!-- vim-markdown-toc -->

---
## Use

### System update

```console
$ sudo yum check-update
$ sudo yum update
```

### YUM Utils

`yum-utils` is a collection of tools and programs for managing yum repositories, installing debug
packages, source packages, extended information from repositories and administration. You can
install it like so:
```console
$ sudo yum install -y yum-utils
```

Now you can add a repository e.g. like so:
```console
$ sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
```

### EPEL

EPEL represents "Extra Packages for Enterprise Linux". It is a repository that is found online. It
develops and manages extra packages of standard quality. This repository is developed for
Enterprise Linux, such as Oracle Linux (OL), CentOS, Scientific Linux (SL), and Red Hat Enterprise
Linux (RHEL). Using Fedora, these packages were developed and tested.

EPEL packages are mainly based on Fedora, which means that these packages will never replace or
conflict with packages in Enterprise Linux systems. EPEL shares so much of Fedora’s architecture,
including the Bugzilla instance, build system, mirror manager, updates manager, and more.

```console
$ sudo yum install epel-release
$ sudo yum repolist | grep epel && echo OK
```

### Prevent packages from being updated

Sometimes you may want to restrict a package from being updated to a newer version. The Yum plugin
`versionlock` allows you to lock packages to a specific version. The plugin is not installed by
default so first, you will need to install it:
    ```console
    $ sudo install yum-plugin-versionlock
    ```

During the installation two configuration files will be created on your system, stored in the
`/etc/yum/pluginconf.d` directory. The configuration file `versionlock.conf` and the file
`versionlock.list` containing the locked packages. By default, no packages are listed in this file.

To lock a version of a package you can either manually add the package name in the file or use the
`$ yum versionlock` command followed by the package name. For example, to prevent all PHP packages
(all packages starting with `php-`) from being updated you would run:
    ```console
    sudo yum versionlock php-*
    ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
