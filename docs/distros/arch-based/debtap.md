---
tags:
  - Linux Distributions
  - Distros
  - Arch Based
  - Package Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `debtap`

???+ Note "Reference(s)"
    * <https://www.maketecheasier.com/install-deb-package-in-arch-linux/>
    * <https://aur.archlinux.org/packages/debtap/>
    * <https://bbs.archlinux.org/viewtopic.php?id=187558>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**

You must run at least once `$ debtap -u` with root privileges, before running `debtap` for anything
else in order to update the `debtap` database:
```console
$ sudo debtap -u
```

List available commands:
```console
debtap -h
```

Run `debtap` on a `.deb` package:
```console
$ debtap packagetoconvert.deb
```

Then install it:
```console
$ sudo pacman -U packagetoconvert.pkg.tar.zst
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
