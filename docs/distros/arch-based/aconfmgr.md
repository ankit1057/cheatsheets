---
tags:
  - Arch Based
  - Dotfiles
  - Configuration Management
  - Package Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---

!!! Warning " "
    This document does not replace the [official Arch installation guide](https://wiki.archlinux.org/title/Installation_guide)<br/>
---

# aconfmgr

aconfmgr is a package to track, manage, and restore the configuration of an Arch Linux system. Its
goals are:

* Quickly configure a new system, or restore an existing system according to a saved configuration.
* Track temporary/undesired changes to the system's configuration
* Identify obsolete packages and maintain a lean system

aconfmgr tracks the list of installed packages (both native and external), as well as changes to
configuration files (`/etc/`). Since the system configuration is described as shell scripts, it is
trivially extensible.

???+ Note "Reference(s)"
    * <https://github.com/CyberShadow/aconfmgr>
    * <https://wiki.archlinux.org/title/Dotfiles> (alternatives)


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->


---
## Install

**TODO**


---
## Config

**TODO**


---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
