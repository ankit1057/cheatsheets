---
tags:
  - Linux Distributions
  - Distros
  - Arch Based
  - Arch
  - Installation Guide
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


!!! Warning " "
    This document does not replace the [official Arch installation guide](https://wiki.archlinux.org/title/Installation_guide)<br/>
---


# Arch installation

Arch Linux is an independently developed, x86-64 general-purpose GNU/Linux distribution that
strives to provide the latest stable versions of most software by following a rolling-release
model. The default installation is a minimal base system, configured by the user to only add what
is purposely required.

!!! Note "Note"
    In this cheat sheet, one or multiple Linux distributions (distros) can already be installed on
    other partitions: they will be preserved and still available through `grub`.

!!! Note "Note"
    In this cheat sheet, the installation process is compatible with a virtual machine (VM) or a
    container. In this case, you might be interested in those cheat sheets:

    * [Qemu KVM](../../virtualization/qemu_kvm.md)
    * [Virtualbox](../../virtualization/virtualbox.md)
    * [Docker](../../virtualization/docker.md)
    * [Podman](../../virtualization/podman.md)

!!! Note "Note"
    In this cheat sheet, UEFI won't be used as a bootloader, like described here:
    <https://wiki.archlinux.org/index.php/EFISTUB>, the bootloader used here is GRUB2 (or `grub` for
    short) but it could be anything else you want.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/Installation_guide>
    * <https://wiki.archlinux.org/title/General_recommendations>
    * <https://wiki.archlinux.org/title/Frequently_asked_questions>
    * <https://wiki.archlinux.org/title/Help:Reading>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Prerequisites](#prerequisites)
* [Partitioning, formatting and mounting](#partitioning-formatting-and-mounting)
* [System install](#system-install)
    * [mirrors, base, kernel, utilities and fstab](#mirrors-base-kernel-utilities-and-fstab)
    * [`chroot` and system configuration](#chroot-and-system-configuration)
* [Bootloader configuration with GRUB](#bootloader-configuration-with-grub)
* [Reboot](#reboot)
* [Post installation](#post-installation)
    * [Optional kernel tweaking](#optional-kernel-tweaking)

<!-- vim-markdown-toc -->

---
## Prerequisites

* Download the latest Arch Linux ISO image here: <https://archlinux.org/download/> (and download the
  associated `.sig` file)

* Check the ISO image with [GnuPG](../../security/gnupg.md) thanks to the PGP signature (`.sig`
  file):
    ```console
    $ gpg --keyserver-options auto-key-retrieve --verify archlinux-version-x86_64.iso.sig
    ```

* Follow the bellow [common prerequisites steps](../common_installation_steps.md#prerequisites)
  (*just the prerequisites for now*).

!!! Tip "Official documentations for the rest of the installation"
    * <https://wiki.archlinux.org/title/Installation_guide>

---
## Partitioning, formatting and mounting

* Optionaly read the [optional disk encryption: drive
  preparation](../common_installation_steps.md#optional-disk-encryption-drive-preparation)
* Read the [common partitioning, formatting and mounting steps to
  follow](../common_installation_steps.md#partitioning-formatting-and-mounting).
* Optionaly read the [optional disk encryption: partition
  preparation](../common_installation_steps.md#optional-disk-encryption-partition-preparation)

---
## System install

### mirrors, base, kernel, utilities and fstab

* Packages to be installed must be downloaded from [mirror
  servers](https://wiki.archlinux.org/title/Mirrors), which are defined in
  `/etc/pacman.d/mirrorlist`. On the live system, after connecting to the internet,
  [reflector](https://wiki.archlinux.org/title/Reflector) updates the mirror list by choosing 20
  most recently synchronized HTTPS mirrors and sorting them by download rate.

    The higher a mirror is placed in the list, the more priority it is given when downloading a
    package. You may want to inspect the file to see if it is satisfactory. If it is not, edit the
    file accordingly, and move the geographically closest mirrors to the top of the list, although
    other criteria should be taken into account.

    This file will later be copied to the new system by `pacstrap`, so it is worth getting right:
    ```console
    $ vi /etc/pacman.d/mirrorlist
    ```

* ⚠️ At this point, make sure that all partitions are mounted, after they have been created during the
  [common partitioning, formatting and mounting steps to
  follow](../common_installation_steps.md#partitioning-formatting-and-mounting)!

* Use the [`pacstrap`](https://man.archlinux.org/man/extra/arch-install-scripts/pacstrap.8.en)
  script to install the `base` (and optionally the `base-devel`) package group(s):
    ```console
    # pacstrap /mnt/arch base base-devel
    ```

* Use the [`pacstrap`](https://man.archlinux.org/man/extra/arch-install-scripts/pacstrap.8.en)
  script to install the Linux kernel:

    * ... with the following command if you don't mind
      [binary-blob](https://en.wikipedia.org/wiki/Proprietary_device_driver):
      ```console
      # pacstrap /mnt/arch linux linux-firmware
      ```

    * ... **or** with the following command if you don't want any
      [binary-blob](https://en.wikipedia.org/wiki/Proprietary_device_driver):
      ```console
      # pacstrap /mnt/arch linux
      ```

    * ... **or** with the following command if you prefer a LTS kernel without
      [binary-blob](https://en.wikipedia.org/wiki/Proprietary_device_driver):
      ```console
      # pacstrap /mnt/arch linux-lts
      ```

    * ... **or** with the following command if you prefer a LTS kernel and
      [binary-blob](https://en.wikipedia.org/wiki/Proprietary_device_driver):
      ```console
      # pacstrap /mnt/arch linux-lts linux-firmware
      ```

* Use the [`pacstrap`](https://man.archlinux.org/man/extra/arch-install-scripts/pacstrap.8.en)
  script to *optionally* install other utility packages, e.g.:
    ```console
    # pacstrap /mnt/arch vi man-db htop neovim sudo
    ```

* Generate a `/etc/fstab` file with `genfstab` (use `-U` or `-L` to define by UUID or labels,
  respectively):
    ```console
    # genfstab -U /mnt/arch >> /mnt/arch/etc/fstab # you can use `genfstab -L ...` instead, for labels instead of UUID
    ```

    Check the resulting `/mnt/arch/etc/fstab` file, and edit it in case of errors.
    ```console
    # blkid # ifentify labels and UUIDs
    # vi /mnt/arch/etc/fstab # edit and verify (this is just an example)
        > # Static information about the filesystems.
        > # See fstab(5) for details.
        >
        > # e.g. below example with the "noatime" option optimisation (no access date
        > # update) and "discard" option for ssd optimisation (trim functionnalities)
        >
        > # <fs>                                        <mountpoint>    <type>      <opts>                                 <dump> <pass>
        >
        > # /dev/vda3 LABEL=MAIN
        > UUID=ea530bdb-1116-4618-9247-e60663fde8bd     /               ext4        defaults,noatime,discard                0 1
        >
        > # /dev/vda1
        > UUID=BE08-4CD2                                /boot           vfat        defaults,noatime                        0 2
        >
        > # /dev/vda2 LABEL=SWAP
        > UUID=ab11cde3-fc5c-4d2f-be7c-5665e13ab272     none            swap        defaults                                0 0
    ```
    See <https://wiki.archlinux.org/title/Fstab> for more details.

### `chroot` and system configuration

* [`chroot`](https://wiki.archlinux.org/title/Chroot):
    ```console
    # arch-chroot /mnt/arch
    $ export PS1="[chroot] $PS1"
    ```

!!! Tip "Tip"
    Now that the new Arch system is entered, don't forget to setup a proxy if needed,<br/>
    e.g. `$ export http_proxy="http://192.168.1.100:4242"`<br/>
    and `$ export https_proxy="http://192.168.1.100:4242"`.<br/>
    ⚠️ Remember that with a proxy: `ping` may not work! ⚠️

!!! Note "Note"
    From now on, all actions will be performed in the new Arch environment.

* Time zone config:
    ```console
    [chroot]# ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
    ```
    See [this related Arch Wiki page](https://wiki.archlinux.org/title/System_time#Time_zone) for
    more details.

* Hardware clock config (not needed on a VM):
    ```console
    [chroot]# vi /etc/conf.d/hwclock # make sure that the clock is indeed UTC
        > clock="UTC"
    [chroot]# hwclock --show
    [chroot]# hwclock --systohc # generate /etc/adjtime
    ```
    See [this related Arch Wiki page](https://wiki.archlinux.org/title/System_time#Hardware_clock)
    for more details

* Localization config:
    ```console
    [chroot]# vi /etc/locale.gen # e.g. for US and FR:
        > en_US.UTF-8 UTF-8
        > en_US ISO-8859-1
        > fr_FR.UTF-8 UTF-8
        > fr_FR ISO-8859-1
        > fr_FR@euro ISO-8859-15
    [chroot]# locale-gen
    ```

    Create the `/etc/locale.conf` file, and set the LANG variable accordingly:
    ```console
    [chroot]# vi /etc/locale.conf # e.g. for US language:
        > LANG=en_US.UTF-8
    ```

    If you set the console keyboard layout, make the changes persistent by creating the
    `/etc/vconsole.conf` file:
    ```console
    [chroot]# vi /etc/vconsole.conf
        > KEYMAP=fr
    ```

    See [this related Arch Wiki page](https://wiki.archlinux.org/title/Locale) for more details.

* Users config:
    ```console
    [chroot]# passwd # set root password
    [chroot]# useradd -m user # add user
    [chroot]# passwd user # set user password
    ```

    * Optional `sudo` config: see [`sudo` cheat sheet](../../admin/sudo.md).

* Host name config:
    ```console
    [chroot]# vi /etc/hostname
        > whatever-name-you-want
    ```

* Network config (see [network managers](../../networks/network_managers.md)), e.g. with
  [NetworkManager](../../networks/networkmanager_nmcli.md):
    ```console
    [chroot]# pacman -S networkmanager
    [chroot]# systemctl enable NetworkManager
    ```

* Optional SSH config: see [ssh cheat sheet](../../networks/ssh.md)

---
## Bootloader configuration with GRUB

* Optionaly read the [optional disk encryption: mkinitcpio
  configuration](../common_installation_steps.md#optional-disk-encryption-mkinitcpio-configuration)
* Optionaly read the [optional disk encryption: boot
  encryption](../common_installation_steps.md#optional-disk-encryption-boot-encryption)

* Read the [common bootloader configuration steps to
  follow](../common_installation_steps.md#bootloading-with-grub).

* Optionaly read the [optional disk encryption: GRUB2 bootloader
  configuration](../common_installation_steps.md#optional-disk-encryption-grub2-bootloader-configuration)
* Optionaly read the [optional disk encryption: swap
  encryption](../common_installation_steps.md#optional-disk-encryption-swap-encryption)

---
## Reboot

* Exit `chroot`, `umount` and reboot:
    ```console
    [chroot]# exit # quit chroot
    # umount -R /mnt/arch
    # reboot # if in a VM, you might `poweroff` instead and run your VM with different launch options
    ```

* If using a VM with UEFI, and having troubles booting, then you might want to check [how to handle
  your first reboot with UEFI on a
  VM](../common_installation_steps.md#first-reboot-with-uefi-on-a-vm).

---
## Post installation

* Read the [common post installation steps to
  follow](../common_installation_steps.md#post-installation).

### Optional kernel tweaking

You might want to have the choice to boot either on the latest kernel or the LTS kernel. In this
case, install the following kernel related packages:
```console
# pacman -S linux linux-lts linux-lts-headers linux-lts-docs
```

Then make sure `/boot` is mounted:
```console
$ df -aTh      # check mounted devices
$ ls -la /boot # and make sure /boot is mounted
```

Finally, update your grub configuration file:
```console
# grub-mkconfig -o /boot/grub/grub.cfg # update /boot/grub/grub.conf
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
