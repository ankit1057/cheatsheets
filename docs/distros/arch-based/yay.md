---
tags:
  - Linux Distributions
  - Distros
  - Arch Based
  - Package Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Yay

Yay is an AUR helper (written in Go). AUR helpers automate usage of the AUR (like search for
AUR packages, resolving dependencies, retrieve and build AUR packages, etc).

???+ Note "Reference(s)"
    * <https://github.com/Jguer/yay>
    * <https://wiki.archlinux.org/title/AUR_helpers>
    * <https://computingforgeeks.com/pacman-and-yaourt-package-manager-mastery-cheat-sheet/>
    * <https://computingforgeeks.com/yay-best-aur-helper-for-arch-linux-manjaro/>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Troubleshooting](#troubleshooting)
        * [`Packages to exclude`](#packages-to-exclude)
        * [`Packages to cleanBuild?`](#packages-to-cleanbuild)

<!-- vim-markdown-toc -->

---
## Install

Update your system and install the needed packages:

```console
$ sudo pacman -Syu
$ sudo pacman -S --needed git base-devel
$ git clone https://aur.archlinux.org/yay-bin.git
$ cd yay-bin
$ makepkg -si
$ cd ..
$ rm -rf yay-bin
$ yay --version
```

---
## Config

First use:

* Run `yay -Y --gendb` to generate a development package database for `*-git` packages that were
  installed without yay. This command should only be run once.

* Run `yay -Syu --devel` in order to check for development package updates.

* Run `yay -Y --devel --save` in order to make development package updates permanently enabled
  (`yay` and `yay -Syu` will then always check for dev packages).


---
## Use

* Update all the system and packages (not only AUR ones):

    * First, check the latest news: <https://www.archlinux.org/news/>

    * Then, update:

    ```console
    $ yay # or `yay -Syu`
    ```

* Update AUR packages only:

    ```console
    $ yay -Sua
    ```

* Check updates for all the system and packages (not only AUR ones):

    ```console
    $ yay -Qu
    ```

* Check updates for AUR packages:

    ```console
    yay -Qua
    ```

* Search for a package (not only AUR one):

    ```console
    $ yay package-to-search # or `yay -Ss package-to-search`
    ```

* Install a package (not only AUR}:

    ```console
    $ yay -S package-to-install
    ```

* Print package information:

    ```console
    $ yay -Si package-to-print-info
    ```

* List packages not installed from the main repos (i.e. AUR, pkgbuild, ...):

    ```console
    $ yay -Qm
    ```

* Remove a package:

    ```console
    $ yay -R package-to-remove
    ```

* Clean package cache:

    ```console
    $ yay -Sc
    ```

* Clean unneeded dependencies:

    ```console
    $ yay -Yc
    ```

* Print system statistics:

    ```console
    $ yay -Ps
    ```

* Unvote for package (requires setting `AUR_USERNAME` and `AUR_PASSWORD` environment variables):

    ```console
    $ yay -Wu package-to-unvote
    ```

* Vote for package (requires setting `AUR_USERNAME` and `AUR_PASSWORD` environment variables):

    ```console
    $ yay -Wv package-to-vote
    ```

### Troubleshooting

#### `Packages to exclude`

Try to never exclude exclude packages when asked. Excluding packagges may cause partial upgrades
and break systems). Instead just hit <Enter>.

#### `Packages to cleanBuild?`

When asked `Packages to cleanBuild?`, it is recommanded to clean build only packages that cause
problem at installation (or update). If no package cause you any trouble,
you can just answer [N]one.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
