---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Package Management
  - Sysadmin
  - System Administration
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Overlays

An overlay is an ebuild repository, i.e. a repository containing packages you might want to install
and use. With Portage, the main Gentoo overlay is installed by default (you can find the config
file of this overlay here: `/etc/portage/repos.conf/gentoo.conf`).

By default, the overlays configuration files is located here: `/etc/portage/repos.conf`.

But alternative overlays also exist and can be used:

* They are *registered* repositories, which are "known and more trusted" by the community. You can
  find them here:
    * <https://overlays.gentoo.org/>
    * <https://gpo.zugaina.org/>

* They are *unregistered* repositories, which are not officially listed by the Gentoo Overlays
  project members. Note that if you want to add an overlay to the registered ones, you can follow
  [this guide](https://wiki.gentoo.org/wiki/Project:Overlays/Overlays_guide).

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Project:Overlays>
    * <https://wiki.gentoo.org/wiki/Project:Overlays/Overlays_guide>
    * <https://overlays.gentoo.org/>
    * <https://gpo.zugaina.org/>
    * <https://wiki.gentoo.org/wiki/Layman>
    * <https://wiki.gentoo.org/wiki/Eselect/Repository>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [`eselect` repositories](#eselect-repositories)
* [`eix`](#eix)
* [Layman](#layman)

<!-- vim-markdown-toc -->


---
## `eselect` repositories

`eselect-repository` is an [`eselect`](./eselect.md) module for manipulating
`/etc/portage/repos.conf` entries (i.e. ebuild repositories/overlays).

This utility supersedes `layman` for listing, configuring, and handling synchronization of
alternate repositories (except for version control systems which the package manager does not
natively sync, e.g. `darcs` and `g-sorcery` in Portage).

* Install:
  ```console
  # emerge -a app-eselect/eselect-repository
  ```

* List available repositories (note that added/enabled repositories are marked with `*`, and
  that the ones installed without `eselect-repository` are marked with `#`).
  ```console
  $ eselect repository list
  ```

* Add/enable a *registered* repository:
```console
$ eselect repository enable repo-name
```

* Sync added repository
  ```console
  # emaint sync -r repo-name
  ```

* Example installing monero:
  ```console
  # eselect repository enable monero
  # emaint sync -r monero
  # echo '*/*::monero ~amd64' >> /etc/portage/package.accept_keywords
  # emerge -a net-p2p/monero
  ```

* Add/enable an *unregistered* repository:
  ```console
  # eselect repository add unregistered-repo-name git https://github.com/test/unregistered-repo-name.git
  ```

* Disable a *registered* repository without removing its contents:
  ```console
  # eselect repository disable registered-repo-name
  ```

* Disable an *unregistered* repository without removing its contents:
  ```console
  # eselect repository disable -f unregistered-repo-name
  ```

* Disable a *registered* repository and remove its contents:
  ```console
  # eselect repository remove registered-repo-name
  ```

* Disable an *unregistered* repository and remove its contents:
  ```console
  # eselect repository remove -f unregistered-repo-name
  ```

* Create a new local repository (the `create` sub-command will make an ebuild repository skeleton):
  ```console
  # eselect repository create /path/to/local-repo
  ```

---
## `eix`

If you want to search across all *registered* overlays (and not only the installed ones) you can
use `eix` with the `-R` option, see [`eix`](./eix.md).

> Note that you can also this web interface to do so: <https://gpo.zugaina.org/>.


---
## Layman

**TODO** (see <https://wiki.gentoo.org/wiki/Layman>)


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
