---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Sysadmin
  - System Administration
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `savedconfig`

TODO

???+ Note "Reference(s)"
    * <https://www.youtube.com/watch?v=1sAJDK0Sj4w>
    * <https://forums.gentoo.org/viewtopic-t-1035746-view-next.html>
    * <https://wiki.gentoo.org/wiki//etc/portage/savedconfig>
    * <https://wiki.gentoo.org/wiki/Savedconfig>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

TODO

---
## Config

TODO

---
## Use

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
