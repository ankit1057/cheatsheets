---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Installation Guide
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


!!! Warning " "
    This document does not replace the [official Gentoo AMD64 handbook](https://wiki.gentoo.org/wiki/Handbook:AMD64).<br/>
---


# Gentoo installation

!!! Warning "Warning"
    **This cheat sheet will present a GPT (not MBR) and (U)EFI (not BIOS) installation!**

!!! Note "Note"
    In this cheat sheet, one or multiple Linux distributions (distros) can already be installed on
    other partitions: they will be preserved and still available through `grub`.

!!! Note "Note"
    In this cheat sheet, UEFI won't be used as a bootloader, like described here:
    <https://wiki.gentoo.org/wiki/EFI_stub_kernel>, the bootloader used here is GRUB2 (or `grub` for
    short) but it could be anything else you want.

---
## Table of Contents

<!-- vim-markdown-toc GitLab -->

* [1.0 Prerequisites](#10-prerequisites)
* [2.0 Preparing the disks : partitioning the disks](#20-preparing-the-disks-partitioning-the-disks)
* [2.1 Preparing the disks : creating and mounting the file systems](#21-preparing-the-disks-creating-and-mounting-the-file-systems)
* [3.0 Installing installation files : date and time verification](#30-installing-installation-files-date-and-time-verification)
* [3.1 Installing installation files : stage 3 and portage tarballs](#31-installing-installation-files-stage-3-and-portage-tarballs)
* [3.2 Installing installation files : configuration before `chroot`](#32-installing-installation-files-configuration-before-chroot)
* [4.0 Installing base system : mirrors, repositories, DNS and `chroot`, sync, profile...](#40-installing-base-system-mirrors-repositories-dns-and-chroot-sync-profile)
* [4 bis Recovering if needed](#4-bis-recovering-if-needed)
* [4.2 Installing base system : sync, profiles and world update](#42-installing-base-system-sync-profiles-and-world-update)
* [4.3 no `nano`](#43-no-nano)
* [4.4 Installing base system : locales and timezone](#44-installing-base-system-locales-and-timezone)
* [5.0 Configuring the kernel](#50-configuring-the-kernel)
* [6.0 Configuring the system : fstab](#60-configuring-the-system-fstab)
* [6.1 Configuring the system : networking](#61-configuring-the-system-networking)
    * [Default network config with `netifrc`](#default-network-config-with-netifrc)
    * [Alternative network config with `dhcpcd`](#alternative-network-config-with-dhcpcd)
    * [WiFi config](#wifi-config)
* [6.2 Configuring the system : users](#62-configuring-the-system-users)
* [7.0 Installing system tools : `numlock`](#70-installing-system-tools-numlock)
* [7.1 Installing system tools : logger](#71-installing-system-tools-logger)
* [7.2 Installing system tools : Cron daemon task manager](#72-installing-system-tools-cron-daemon-task-manager)
* [7.3 Installing system tools : NTP (clock auto sync via network)](#73-installing-system-tools-ntp-clock-auto-sync-via-network)
* [7.4 Installing system tools : `sshd` (optional: for servers)](#74-installing-system-tools-sshd-optional-for-servers)
* [7.5 Installing system tools : `elogind`](#75-installing-system-tools-elogind)
* [7.6 Installing system tools : `sudo`](#76-installing-system-tools-sudo)
* [8.0 Configuring the bootloader for UEFI boot : grub](#80-configuring-the-bootloader-for-uefi-boot-grub)
* [8.1 Configuring the bootloader : rebooting the system](#81-configuring-the-bootloader-rebooting-the-system)
* [9.0 Post installation config](#90-post-installation-config)
    * [`xorg` as graphic server](#xorg-as-graphic-server)
    * [`dwm` as windows manager](#dwm-as-windows-manager)

<!-- vim-markdown-toc -->

---
## 1.0 Prerequisites

* Download the latest Gentoo installation ISO image here: <https://www.gentoo.org/downloads/>. On
  this page, click on the "Minimal Installation CD" link at the bottom right of the "Advanced
  choices and other architectures" section. After following that link, you will be able to download
  the installation ISO you want, **and** its associated `.asc` file.

* Check the ISO image with [GnuPG](../../security/gnupg.md) thanks to the `.asc` file:
    ```console
    $ wget -O - https://qa-reports.gentoo.org/output/service-keys.gpg | gpg --import
    $ gpg --verify install-amd64-minimal-version.iso.asc
        > ...
        > Primary key fingerprint: 13EB BDBE DE7A 1277 5DFD  B1BA BB57 2E0E 2D18 2910
    ```
    Then check that the printed key fingerprint matches one of those:
    <https://www.gentoo.org/downloads/signatures/>

* Follow [this cheat sheet about bootable media](../../hardware/bootable_media.md) in order to
  install the ISO image on a USB drive and **boot on it with UEFI!** (see also [this related wiki
  article](https://wiki.archlinux.org/title/USB_flash_installation_medium))

!!! Warning "Warning"
    The bootable USB drive wont support Secure Boot. You will need to [disable Secure
    Boot](https://web.archive.org/web/20220522042243/https://www.rodsbooks.com/efi-bootloaders/secureboot.html#disable)
    from the host computer. Note that you can optionally [set up Secure
    Boot](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot) after
    completing the installation.

!!! Tip "Tip"
    After booting on the USB drive, you can easily get an Ethernet or WiFi connection to internet
    (just plug an Ethernet cable, or setup a WiFi connection through the graphical interface), but
    don't forget to setup a proxy if needed,<br/>
    e.g. `$ export http_proxy="http://10.100.6.193:8080" `<br/>
    and `$ export https_proxy="http://10.100.6.193:8080 `.<br/>
    Remember that in this case `ping` may not work!

!!! Tip "Tip"
    If the keyboard layout feels wrong, it can be changed like so:
    ```console
    # loadkeys fr # e.g. switch the keyboard layout to French (or any other language) if needed
    ```

!!! Tip "Tip"
    If you want to resize a partition in order to make some place for another one (e.g. to install
    Artix in dual-boot with another distro), then you can refer to [this cheat sheet about
    partitioning with `gnome-parted`](../../drives/partitions.md#gnome-parted) **after booting on
    the USB drive**.

!!! Note "Useful documentations for the installation"
    * [Linux tricks Gentoo
      installation](https://www.linuxtricks.fr/wiki/installer-gentoo-facilement)
    * [Linux tricks Gentoo desktop
      installation](https://www.linuxtricks.fr/wiki/installer-gentoo-facilement-environnement-de-bureau)
    * [quick installation checklist](https://wiki.gentoo.org/wiki/Quick_Installation_Checklist)
    * [AMD64 handbook](https://wiki.gentoo.org/wiki/Handbook:AMD64)
    * [AMD64 handbook on one full page](https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation)
    * [complete AMD64 handbook](https://wiki.gentoo.org/wiki/Complete_AMD64_Handbook)


---
## 2.0 Preparing the disks : partitioning the disks

!!! Warning "Warning"
    **This section is about GPT (not MBR) and (U)EFI (not BIOS) partitioning!**

!!! Warning "Warning"
    You might already have a Linux distro installed on another partition, in this case **make sure
    to NOT override it when partitioning**. As described at the end of this cheat sheet, the
    `os-prober` tool will find this partition and make it also available through `grub`.

Here, the tool used to partition the disks is `gdisk`:

* [partitions](../../drives/partitions.md)
* [`gdisk`](https://wiki.archlinux.org/index.php/Gdisk)
* [`gdisk` example](https://debian-facile.org/doc:systeme:gdisk)

Check which disk to partition with the `# lsblk` command, here the disk to partition will be called
`/dev/sdx`.

Two or three partitions will be created:

* `sdx1` : UEFI ESP boot (512 Mo)

* `sdx2` : Linux Swap (OPTIONAL when ram > 8-16 Go, but necessary to hibernate, in this case you
  need as much swap as ram)

* `sdx3` : Linux File system main (home + root)

But first, **backup your partition table**:
```console
# sfdisk -d /dev/disk > disk.dump
```

!!! Tip "Tip"
    If you want to restore your partition table later, run: `# sfdisk /dev/disk < disk.dump`

Then proceed:
```console
# gdisk /dev/sdx # gpt partitioning (alternative : parted)
    > ? # list all commands
    > p # list all partitions
    > d # delete a partition

    > o # create a new empty GPT
        > Y # validate new empty GPT (⚠️ delete all pre-existing partitions)

    > n # create a new partition (boot partition)
        > # enter to leave default part number (1 for sda1)
        > # enter to leave default first sector
        > +512M # specify a 512Mo partition size
        > ef00 # specify an EFI System boot partition type

    > n # create new partition (optional swap)
        > # enter to leave default part number (2 for sda2)
        > # enter to leave default first sector
        > +16384M # specify a 16Go (ram size) partition size
        > 8200 # specify a Linux Swap partition type

    > n # create new partition
        > # enter to leave default part number (3 for sda3)
        > # enter to leave default first sector
        > # enter to get all the space left
        > 8300 #specify a Linux Filesystem partition type

    > p # print the table to ensure that it is correct

    > w # write the table, validate all previous actions and quit
        > Y # validate new table
```


---
## 2.1 Preparing the disks : creating and mounting the file systems

Format and mount the partitions:
```console
# mkfs.fat -F 32 /dev/sdx1          # fat32 for EFI System boot
# mkswap /dev/sdx2                  # swap for Linux Swap
# mkfs.ext4 /dev/sdx3               # ext4 for Linux Filesystem

# swapon /dev/sdx2                  # mount swap
# mkdir -p /mnt/gentoo
# mount /dev/sdx3 /mnt/gentoo       # mount main partition

# mkdir -p /mnt/gentoo/boot
# mount /dev/sdx1 /mnt/gentoo/boot  # mount boot partition
```


---
## 3.0 Installing installation files : date and time verification

Ensure date and time are well set (also check it's UTC format), misconfigured clock may lead to
strange results.
```console
# date # print date

# date MMJJhhmmAAAA # set date and time manually (if needed)
```


---
## 3.1 Installing installation files : stage 3 and portage tarballs

First: it can be very interesting to know what modules where appropriately loaded on the
installation ISO image, it might help for later installation:

!!! Tip "Tip"
    Remember this step when configuring your kernel, and when configuring some modules, e.g. WiFi.

```console
# cd /mnt/gentoo
# lsmod > tmp_install_lsmod
# lspci -nnk > tmp_install_lspci
```

Now, download the stage 3 tarball ([identify the latest stage 3 AMD64 build
here](http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64/)):
```console
# wget http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64/stage3-amd64-*.tar.xz
# wget http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64/stage3-amd64-*.tar.xz.DIGESTS
```

Verify and validate the download (check that the following checksums diff match):
```console
# diff <(openssl dgst -r -sha512 stage3-amd64-*.tar.xz) <(sed -n 2p stage3-amd64-*.tar.xz.DIGESTS)
```

If the checksums don't match, delete the existing `stage3-amd64-*.tar.xz` and
`stage3-amd64-*.tar.xz.DIGESTS`: download and check them until it's OK.

Extract the stage 3 tarball:
```console
# rm stage3-amd64-*.tar.xz.DIGESTS
# tar xJvpf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner
```


---
## 3.2 Installing installation files : configuration before `chroot`

This section is about correctly configuring the `/etc/portage/make.conf` file settings, which are
customizing the Portage environment on a global level.

Those settings should be wisely configured by reading this wiki page and following its related
articles:

* [`make.conf`](https://wiki.gentoo.org/wiki//etc/portage/make.conf)

Particular attention should be paid to the following flags:

* `COMMON_FLAGS`
* `MAKEOPTS`
* `INPUT_DEVICES`
* `VIDEO_CARDS`

```console
# vi /mnt/gentoo/etc/portage/make.conf

    > # These settings were set by the catalyst build script that automatically
    > # built this stage.
    > # Please consult /usr/share/portage/config/make.conf.example for a more
    > # detailed example.
    >
    > CHOST="x86_64-pc-linux-gnu"
    > COMMON_FLAGS="-march=native -O2 -pipe" # recommended default choice auto-detecting the
    >                                        # CPU's architecture (also for vms), this CPU conf can
    >                                        # be optimized later, after the installation
    > #COMMON_FLAGS="-march=skylake -O2 -pipe"
    > #COMMON_FLAGS="-O2 -march=znver1"
    > # ⚠️ distcc
    > # ⚠️ <https://wiki.gentoo.org/wiki/Safe_CFLAGS>
    >
    > CFLAGS="${COMMON_FLAGS}"
    > CXXFLAGS="${COMMON_FLAGS}"
    > FCFLAGS="${COMMON_FLAGS}"
    > FFLAGS="${COMMON_FLAGS}"
    >
    > #ACCEPT_KEYWORDS="~amd64"
    > #ACCEPT_LICENSE="* -@EULA"  # default license group (any license exept EULA)
    >
    > # NOTE: This stage was built with the bindist Use flag enabled
    >
    > PORTAGE_TMPDIR="/var/tmp"  # compilation and temporary storage data of portage
    > PORTDIR="/usr/portage"  # ebuild repository: ebuilds and profile information
    > DISTDIR="/usr/portage/distfiles"  # source code tarballs for emerges
    > PKGDIR="/usr/portage/packages"  # binary packages with emerge '--buildpkg' or '-p'
    >
    > #USE=""
    > MAKEOPTS="-j9"    # extra options for 'make', here nb of parallel makes: CPUs+1
    > #MAKEOPTS="-j2"   # for vms
    >                   # even if there is more cores allocated to the vm !?
    >                   # (at least to compile llvm on virtualbox?)
    > #MAKEOPTS="-j17"  # extra options for 'make', here nb of parallel makes: CPUs+1
    > AUTOCLEAN="yes"   # portage auto-clean packages after every successful merge
    >
    > # This sets the language of build output to English.
    > # Please keep this setting intact when reporting bugs.
    > LC_MESSAGES=C
    >
    > INPUT_DEVICES="libinput synaptics"  # for mouse, keyboard, and synaptics support
    > #INPUT_DEVICES="evdev synaptics keyboard mouse" # not needed for virtual machines?
    > VIDEO_CARDS="nouveau"
    > #VIDEO_CARDS="virtualbox" # for virtualbox
    > #VIDEO_CARDS="vmware" # for vmware
    > #VIDEO_CARDS="nvidia"
    > #VIDEO_CARDS="amdgpu radeonsi"
    > #VIDEO_CARDS="intel nvidia nouveau radeon vesa virtualbox vmware"
    >
    > GRUB_PLATFORMS="efi-64" # optionnal: needed only if grub is wanted as bootloader with (U)EFI
    > #GRUB_PLATFORMS="pc" # optionnal: needed only if grub is wanted as bootloader with BIOS
    > #GRUB_PLATFORMS="qemu" ?
    > # <https://wiki.gentoo.org/wiki/GRUB2>
```

!!! Warning "Warning"
    While configuring those settings, one might end up on some Gentoo wiki pages, asking for kernel
    modifications, e.g. like those:

    * <https://wiki.gentoo.org/wiki/Ryzen#Kernel>
    * <https://wiki.gentoo.org/wiki/Nouveau#Kernel>
    * <https://wiki.gentoo.org/wiki/NVIDIA/nvidia-drivers#Kernel>
    * <https://wiki.gentoo.org/wiki/AMDGPU#Kernel>

    One shall not forget these modifications because they will have to be applied later
    when [configuring the kernel](#50-configuring-the-kernel).

---
## 4.0 Installing base system : mirrors, repositories, DNS and `chroot`, sync, profile...

Prepare to enter the new Gentoo system...
```console
# mirrorselect -i -o >> /mnt/gentoo/etc/portage/make.conf # (optionnal) mirrors selection

# mkdir -p /mnt/gentoo/etc/portage/repos.conf # copy the gentoo repo config file (from portage)
# cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf

# cp -L /etc/resolv.conf /mnt/gentoo/etc/     # copy dns infos

# mount -t proc /proc /mnt/gentoo/proc        #
# mount --rbind /sys /mnt/gentoo/sys          #
# mount --rbind /dev /mnt/gentoo/dev          # mounting the necessary filesystems

# chroot /mnt/gentoo /bin/bash                # chroot into the system
$ source /etc/profile                         # reloading settings
$ export PS1="[chroot] $PS1"                  # change prompt to remember chroot
```

!!! Tip "Tip"
    Now that the new Gentoo system is entered, don't forget to setup a proxy if any,<br/>
    e.g. `export http_proxy="http://10.100.6.193:8080" `<br/>
    and `export https_proxy="http://10.100.6.193:8080" `.<br/>
    Remember that in this case `ping` may not work!

!!! Warning "Warning"
    From now on, all actions will be performed in the new Gentoo Linux environment.

---
## 4 bis Recovering if needed

If the Gentoo installation is interrupted anywhere after this point (e.g. you want to shut down
your computer and resume the installation the next day), it should be possible to 'resume' where you
left it after entering those commands:

```console
# swapon /dev/sdx2                     # mount swap
# mount /dev/sdx3 /mnt/gentoo          # mount main partition
# mount /dev/sdx1 /mnt/gentoo/boot     # mount boot

# cp -L /etc/resolv.conf /mnt/gentoo/etc/   # copy dns infos (dont overwrite and discard if asked)

# mount -t proc /proc /mnt/gentoo/proc      #
# mount --rbind /sys /mnt/gentoo/sys        #
# mount --rbind /dev /mnt/gentoo/dev        # mount necessary filesystems

# chroot /mnt/gentoo /bin/bash              # chroot into the system
# source /etc/profile                       # reload settings
# export PS1="[chroot] $PS1"                # change prompt to remember chroot
```

!!! Tip "Tip"
    Now that the new Gentoo system is entered, don't forget to setup a proxy if any,<br/>
    e.g. `$ export http_proxy="http://10.100.6.193:8080" `<br/>
    and `$ export https_proxy="http://10.100.6.193:8080" `.<br/>
    Remember that in this case `ping` may not work!

Don't forget to do the following before leaving:
```console
# exit # exit chroot
# umount -l /mnt/gentoo/dev/{/shm,/pts,}
# umount -R /mnt/gentoo
# shutdown -h now
```

---
## 4.2 Installing base system : sync, profiles and world update

In this new environment, the first thing to do is to update the Gentoo `ebuild` repository to the
latest version:
```console
# emerge --sync
```

Or, for those who are behind a proxy and/or restrictive firewalls:
```console
# emerge-webrsync
```

Now is the time to select a profile:
```console
# eselect news list # print the news list to read
# eselect news read # read the news

# eselect profile list
    > [i] default/linux/amd64/n.0 # server ('n' the last version num)
    > [j] default/linux/amd64/n.0/desktop # desktop ('n' the last version num)
# eselect profile set j # e.g. 'desktop' profile selection

# emerge -avuDN @world
```

---
## 4.3 no `nano`

The only purpose of `nano` is actually to let you install Neovim:
```console
# nano -w /etc/portage/package.accept_keywords
    > # (manual) last neovim
    > app-editors/neovim ~amd64
    >
    > ...

# emerge -a neovim # accept the keyword changes ("y")
# etc-update # enter "-3" to auto merge all files, and accept overwrite ("y")
# emerge -a neovim
```

---
## 4.4 Installing base system : locales and timezone

```console
# nvim /etc/locale.gen # configure system language, date formating, etc.

    > en_US ISO-8860-1
    > en_US.UTF-8 UTF-8
    > fr_FR ISO-8859-1
    > fr_FR@euro ISO-8859-15
    > fr_FR.UTF-8 UTF-8

# locale-gen # generate all the locales previously specified
# eselect locale list # display generated available locales targets
# eselect locale set n # select a locale (perso: en_US.UTF-8 UTF-8)

# nvim /etc/conf.d/keymaps # configure the keyboard layout for TTY

    > keymap="fr"

# env-update && source /etc/profile && export PS1="[chroot] $PS1" # update env

# echo "Europe/Paris" > /etc/timezone

# emerge --config sys-libs/timezone-data

# nvim /etc/conf.d/hwclock # make sure that the clock is indeed UTC
    > clock="UTC"

# hwclock --systohc # (optionnal) only if installing on a vm
```

---
## 5.0 Configuring the kernel

* See [*the Gentoo kernel documentation*](https://wiki.gentoo.org/wiki/Kernel)

According to what has been found in section [3.1 Installing installation files : stage 3 and
portage tarballs](#31-installing-installation-files-stage-3-and-portage-tarballs), and according to
what has been found in section [3.2 Installing installation files : configuration before
`chroot`](#32-installing-installation-files-configuration-before-chroot), one might be looking
towards proprietary solutions.

In some cases, proprietary drivers (also known as binary blobs) are needed. If so, installing
`linux-firmware` might be needed for better compatibility:
```console
# emerge -a linux-firmware # <https://wiki.gentoo.org/wiki/Linux_firmware>
```

If you are configuring the kernel of a laptop, you might find it (or a similar one) in this wiki
list: <https://wiki.gentoo.org/wiki/Category:Laptops>

Also, some laptops are in the wiki but not yet in this list (e.g.
<https://wiki.gentoo.org/wiki/Dell_XPS_13_9350>), so you can try to search
`gentoo+wiki+<laptop-name>` in a search engine.

Install the official Gentoo kernel (unless you are interested in alternative kernels: see
<https://wiki.gentoo.org/wiki/Kernel/Overview> and see
<http://kroah.com/log/blog/2018/08/24/what-stable-kernel-should-i-use/>) and configure it:
```console
# emerge -a gentoo-sources # install kernel sources
# emerge -a pciutils usbutils # install lspci and lsusb

# cd /usr/src/linux

# make mrproper # delete current configuration and all generated files
# make nconfig # or `# make menuconfig`

    # Make sure that no option has been forgottent here:
    # <https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation#Activating_required_options>

    # Enabling devtmpfs support
    ###########################
    # Double check here:
    # <https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation#Activating_required_options>
    #
    > Device Drivers --->
    >     Generic Driver Options --->
    >         _*_ Maintain a devtmpfs filesystem to mount at /dev # Symbol: DEVTMPFS [=y]
    >         [*]   Automount devtmpfs at /dev, after the kernel mounted the rootfs # Symbol: DEVTMPFS_MOUNT [=y]
    ###########################

    # Enabling SCSI disk support
    ############################
    # Double check here:
    # <https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation#Activating_required_options>
    #
    > Device Drivers --->
    >    SCSI device support  --->
    >          <*> SCSI disk support # Symbol: BLK_DEV_SD [=y]
    ############################

    # EFI support
    #############
    # Double check here: <https://wiki.gentoo.org/wiki/EFI_System_Partition#Kernel>

    # Enable support for GPT
    #
    > -*- Enable the block layer ---> # Symbol: BLOCK [=y]
    >    Partition Types --->
    >       [*] Advanced partition selection # Symbol: PARTITION_ADVANCED [=y]
    >       [*] EFI GUID Partition support # Symbol: EFI_PARTITION [=y]

    # Enable ISO8859-1 codepage (in order to mount the EFI partition)
    #
    > File Systems --->
    >    -*- Native Language support ---> # Symbol: NLS [=y]
    >       [*] NLS ISO 8859-1  (Latin 1; Western European Languages) # Symbol: NLS_ISO8859_1 [=y]
    #############

    # TODO: MBR + BIOS support
    ##########################
    #
    ##########################

    # HDD support
    #############
    # Double check here: <https://wiki.gentoo.org/wiki/HDD#Kernel>
    #
    > Device Drivers --->
    >    <*> Serial ATA and Parallel ATA drivers (libata) ---> # Symbol: ATA [=y]
    >       [*] ATA ACPI Support # Symbol: ATA_ACPI [=y]
    >
    # If the drive is connected to a SATA Port Multiplier:
    >       [*] SATA Port Multiplier support # Symbol: SATA_PMP [=y]
    >
    # Select the driver for the SATA controller, e.g.:
    >       <*> AHCI SATA support # Symbol: SATA_AHCI [=y]
    >
    # If the drive is connected to an IDE controller:
    >       [*] ATA SFF support # Symbol: ATA_SFF [=y]
    >       [*] ATA BMDMA support # Symbol: ATA_BMDMA [=y]
    >
    # Select the driver for the IDE controller, e.g.:
    >       <*> Intel ESB, ICH, PIIX3, PIIX4 PATA/SATA support # Symbol: ATA_PIIX [=y]
    >
    > Device Drivers --->
    >    SCSI device support  --->
    >       -*- SCSI device support # Symbol: SCSI [=y]
    >       <*> SCSI disk support # Symbol: BLK_DEV_SD [=y]
    #############

    # SSD Support
    #############
    # Double check here: <https://wiki.gentoo.org/wiki/SSD>
    #
    # (nothing to check by default)
    #############

    # NVMe Support
    ##############
    # Double check here: <https://wiki.gentoo.org/wiki/NVMe#Kernel>
    #
    > Device Drivers  --->
    >   NVME Support --->
    >     <*> NVM Express block device # Symbol: BLK_DEV_NVME [=y]
    ##############

    # File systems
    ##############
    # To support more/other filesystems:
    # * <https://wiki.gentoo.org/wiki/Category:Filesystems>
    # * <https://wiki.gentoo.org/wiki/Filesystem>

    # Ext4
    # Double check here: <https://wiki.gentoo.org/wiki/Ext4#Kernel>
    #
    > File systems --->
    >   <*> The Extended 4 (ext4) filesystem # Symbol: EXT4_FS [=y]
    #
    # Optional Ext4 features:
    #
    >    [*]   Ext4 POSIX Access Control Lists # Symbol: EXT4_FS_POSIX_ACL [=y]
    >    [*]   Ext4 Security Labels # Symbol: EXT4_FS_SECURITY [=y]

    # XFS
    # Double check here: <https://wiki.gentoo.org/wiki/XFS#Kernel>
    #
    > File systems  --->
    >    <*> XFS filesystem support # Symbol: XFS_FS [=y]
    #
    # Optional XFS features:
    #
    >    [*]   XFS Quota support # Symbol: XFS_QUOTA [=y]
    >    [*]   XFS POSIX ACL support # Symbol: XFS_POSIX_ACL [=y]
    >    [*]   XFS Realtime subvolume support # Symbol: XFS_RT [=y]

    # Btrfs
    # Double check here: <https://wiki.gentoo.org/wiki/Btrfs#Kernel>
    #
    > File systems  --->
    >     <*> Btrfs filesystem # Symbol: BTRFS_FS [=y]

    # FAT
    # Double check here: <https://wiki.gentoo.org/wiki/FAT#Kernel>
    #
    > File systems  --->
    >   DOS/FAT/NT Filesystems  --->
    >     <*> VFAT (Windows-95) fs support # Symbol: VFAT_FS [=y]
    >         (437) Default codepage for FAT # Symbol: FAT_DEFAULT_CODEPAGE [=437]
    >         (iso8859-1) Default iocharset for FAT # Symbol: FAT_DEFAULT_IOCHARSET [=iso8859-1]
    >
    > File systems  --->
    >   -*- Native language support  ---> # Symbol: NLS [=y]
    >     (iso8859-1) Default NLS Option # Symbol: NLS_DEFAULT [=iso8859-1]
    >     <*>   Codepage 437 (United States, Canada) # Symbol: NLS_CODEPAGE_437 [=y]
    >     <*>   NLS ISO 8859-1  (Latin 1; Western European Languages) # Symbol: NLS_ISO8859_1 [=y]
    >     -*-   NLS UTF-8 # Symbol: NLS_UTF8 [=y]

    # ExFAT / Filesystem in Userspace (FUSE)
    # Double check here: <https://wiki.gentoo.org/wiki/ExFAT#Kernel>
    #
    > File systems  --->
    >     <*> FUSE (Filesystem in Userspace) support # Symbol: FUSE_FS [=y]

    # Procfs
    # Double check here: <https://wiki.gentoo.org/wiki/Procfs#Kernel>
    #
    > File systems --->
    >   Pseudo Filesystems --->
    >     [*] /proc file system support # Symbol: PROC_FS [=y]

    # Sysfs
    # Double check here: <https://wiki.gentoo.org/wiki/Sysfs>
    #
    # (nothing to check by default)

    # Tmpfs
    # Double check here: <https://wiki.gentoo.org/wiki/Tmpfs#Installation>
    #
    > File systems --->
    >   Pseudo Filesystems --->
    >     [*] Tmpfs virtual memory file system support (former shm fs) # Symbol: TMPFS [=y]
    ##############

    # USB Support
    #############
    # Double check here: <https://wiki.gentoo.org/wiki/USB/Guide#Config_options_for_the_kernel>
    #
    > Device Drivers  --->
    >   SCSI device support  --->
    >           *** SCSI support type (disk, tape, CD-ROM) ***
    >       <*> SCSI disk support # Symbol: BLK_DEV_SD [=y]
    >
    > Device Drivers  --->
    >   [*] USB support  ---> # Symbol: USB_SUPPORT [=y]
    >     <*> Support for Host-side USB # Symbol: USB [=y]
    >     ...
    >         *** USB Host Controller Drivers ***
    >     <*> xHCI HCD (USB 3.0) support # Symbol: USB_XHCI_HCD [=y]
    >     ...
    >     <*> EHCI HCD (USB 2.0) support # Symbol: USB_EHCI_HCD [=y]
    >     ...
    >     <*> OHCI HCD (USB 1.1) support # Symbol: USB_OHCI_HCD [=y]
    >     ...
    >     <*> UHCI HCD (most Intel and VIA) support # Symbol: USB_UHCI_HCD [=y]
    >     ...
    >     <*> USB Printer support # Symbol: USB_PRINTER [=y]
    >     ...
    >     <*> USB Mass Storage support # Symbol: USB_STORAGE [=y]

    # If you have a USB Network Card, e.g. the RTL8150
    #
    > Device Drivers  --->
    >   [*] Network device support  ---> # Symbol: NETDEVICES [=y]
    >       <*>USB Network Adapters  ---> # Symbol: USB_NET_DRIVERS [=y]
    >           <*> USB RTL8150 based ethernet device support # Symbol: USB_RTL8150 [=y]

    # If you have a serial to USB converter, e.g. the Prolific 2303
    #
    > Device Drivers  --->
    >   [*] USB support  ---> # Symbol: USB_SUPPORT [=y]
    >     <*> USB Serial Converter support  ---> # Symbol: USB_SERIAL [=y]
    >         <*> USB Prolific 2303 Single Port Serial Driver # Symbol: USB_SERIAL_PL2303 [=y]

    # If you have a USB keyboard, mouse, joystick, or any other input device, you need to enable HID support
    >
    > Device Drivers --->
    >     HID support --->
    >         <*>   Generic HID driver # Symbol: HID_GENERIC [=y]
    #         # select specific devices in below sections

    # If you have a USB Type-C and Thunderbolt
    #
    > Bus options (PCI etc.)  --->
    >    [*] PCI support # Symbol: PCI [=y]
    >    [*] PCI Express Port Bus support # Symbol: PCIEPORTBUS [=y]
    >    [*]   PCI Express Hotplug driver # Symbol: HOTPLUG_PCI_PCIE [=y]
    >    [*] Support for PCI Hotplug  ---> # Symbol: HOTPLUG_PCI [=y]
    >        [*]   ACPI PCI Hotplug driver # Symbol: HOTPLUG_PCI_ACPI [=y]

    # Thunderbolt on Apple or on PCs with Intel Falcon Ridge or newer:
    #
    > Device Drivers --->
    >     <*> Thunderbolt support  ---- # Symbol: THUNDERBOLT [=n]

    # If you want USB Audio
    #
    > Device Drivers -->
    >     <*> Sound card support --> # Symbol: SOUND [=y]
    >         <*> Advanced Linux Sound Architecture --> # Symbol: SND [=y]
    >             [*] USB sound devices --> # Symbol: SND_USB [=y]
    >                 <*> USB Audio/MIDI driver # Symbol: SND_USB_AUDIO [=n]
    #############

    # Support for VirtualBox hardware (if installing gentoo on virtualbox)
    #################################
    # Double check here: <https://wiki.gentoo.org/wiki/VirtualBox#Kernel_configuration>
    # <https://wiki.gentoo.org/wiki/VirtualBox#Gentoo_guests>
    #
    > Bus options (PCI etc.)  --->
    >     [*] Mark VGA/VBE/EFI FB as generic system framebuffer # Symbol: X86_SYSFB [=y]
    > Device Drivers  --->
    >     <*> Serial ATA and Parallel ATA drivers (libata)  ---> # Symbol: ATA [=y]
    >         [*] AHCI SATA support # Symbol: SATA_AHCI [=y]
    >         [*] ATA SFF support (for legacy IDE and PATA) # Symbol: ATA_SFF [=y]
    >         [*]   ATA BMDMA support # Symbol: ATA_BMDMA [=y]
    >         [*]     Intel ESB, ICH, PIIX3, PIIX4 PATA/SATA support # Symbol: ATA_PIIX [=y]
    >     <*> Network device support  ---> # Symbol: NETDEVICES [=y]
    >         <*> Ethernet driver support  ---> # Symbol: ETHERNET [=y]
    >             [*] Intel devices # Symbol: NET_VENDOR_INTEL [=y]
    >             [*]   Intel(R) PRO/1000 Gigabit Ethernet support # Symbol: E1000 [=y]
    >     Input device support  --->
    >         <*> Keyboards  ---> # Symbol: INPUT_KEYBOARD [=y]
    >             [*] AT keyboard # Symbol: KEYBOARD_ATKBD [=y]
    >         <*> Mice  ---> # Symbol: INPUT_MOUSE [=y]
    >             [*] PS/2 mouse # Symbol: MOUSE_PS2 [=y]
    >     <*> Virtio drivers  ---> # Symbol: VIRTIO_MENU [=y]
    >         <*> PCI driver for virtio devices # Symbol: VIRTIO_PCI [=y]
    >     Graphics support  --->
    >         <*> Direct Rendering Manager (XFree86 4.1.0 and higher DRI support)  ---> # Symbol: DRM [=y]
    >             [*] Enable legacy fbdev support for your modesetting driver # Symbol: DRM_FBDEV_EMULATION [=y]
    >         <*> Virtio GPU driver # Symbol: DRM_VIRTIO_GPU [=y]
    >         Frame buffer Devices  --->
    >             <*> Support for frame buffer devices  ---> # Symbol: FB [=y]
    >                 [*] Enable Firmware EDID # Symbol: FIRMWARE_EDID [=y]
    >             [*] Simple framebuffer support # Symbol: FB_SIMPLE [=y]
    >         Console display driver support  --->
    >             [*] Framebuffer Console support # Symbol: FRAMEBUFFER_CONSOLE [=y]
    >             [*]   Map the console to the primary display device # Symbol: FRAMEBUFFER_CONSOLE_DETECT_PRIMARY [=y]
    >     <*> Sound card support  ---> # Symbol: SOUND [=y]
    >         <*> Advanced Linux Sound Architecture  ---> # Symbol: SND [=y]
    >             <*> PCI sound devices  ---> # Symbol: SND_PCI [=y]
    >                 [*] Intel/SiS/nVidia/AMD/ALi AC97 Controller # Symbol: SND_INTEL8X0 [=y]
    >     <*> USB support  ---> # Symbol: USB_SUPPORT [=y]
    >         [*] xHCI HCD (USB 3.0) support # Symbol: USB_XHCI_HCD [=y]
    >         [*] EHCI HCD (USB 2.0) support # Symbol: USB_EHCI_HCD [=y]
    #
    # [Optional] Virtual Box Guest integration support (no more need to manually install it)
    # See <https://www.omgubuntu.co.uk/2018/01/virtualbox-guest-additions-linux-kernel>
    #
    > Device Drivers  --->
    >     [*] Virtualization drivers  ---> Symbol: VIRT_DRIVERS [=y]
    >         <*>   Virtual Box Guest integration support # Symbol: VBOXGUEST [=y]
    #################################

    # TODO: Qemu support
    ####################
    #
    ####################

    # ⚠️
    #####
    #
    # After setting all those kernel options,
    # you might want to add some other modifications:
    #
    # ...according to what has been found in section
    # [3.1 Installing installation files : stage 3 and portage tarballs](## 3.1 Installing installation files : stage 3 and portage tarballs)
    #
    # ...and according to what has been found in section
    # [3.2 Installing installation files : configuration before chroot](## 3.2 Installing installation files : configuration before chroot)
    #
    # You also may refer to the kernel configuration part of my xorg cheat sheet note if needed:
    # <https://gitlab.com/stephane.tzvetkov/cheatsheets/-/blob/master/xorg.md>
    #####

    # linux-firmware support (⚠️ optional proprietary blob).
    ########################
    # This might be needed along with some proprietary drivers.
    # Double check here: <https://wiki.gentoo.org/wiki/Linux_firmware#Kernel>

    > Device Drivers  --->
    >   Generic Driver Options  --->
    >     Firmware loader --->
    >        -*- Firmware loading facility # Symbol: FW_LOADER [=y]
    >
    >        #####
    >        # ⚠️ this option might need to be fileld ⚠️
    >        # for example, see <https://wiki.gentoo.org/wiki/AMDGPU#Incorporating_firmware>
    >        () Build named firmware blobs into the kernel binary # Symbol: EXTRA_FIRMWARE [=...]
    >        #####
    >
    >        (/lib/firmware) Firmware blobs root directory # Symbol: EXTRA_FIRMWARE_DIR [=/lib/firmware]
    ########################

# make -j9              # make "nb cores +1": compile the kernel
# make modules_install  # install all modules
# make install          # install kernel
```

---
## 6.0 Configuring the system : fstab

* See [*fstab when
  installing*](https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/System#About_fstab)

* See [*fstab*](https://wiki.gentoo.org/wiki/Fstab)

```console
# blkid # ifentify labels and UUIDs
# vi /etc/fstab
    > # e.g. below example with the "noatime" option optimisation (no access date
    > # update) and "discard" option for ssd optimisation (trim functionnalities)
    >
    > # <fs>                                        <mountpoint>        <type>      <opts>                                  <dump/pass>
    >
    > UUID=ea530bdb-1116-4618-9247-e60663fde8bd     /                   ext4        defaults,noatime,discard                0 1
    > UUID=BE08-4CD2                                /boot               vfat        defaults,noatime                        0 0
    > UUID=ab11cde3-fc5c-4d2f-be7c-5665e13ab272     none                swap        sw                                      0 0
    > #UUID=E68614DD8614B053                        /media/data         ntfs-3g     uid=1001,gid=1001,dmask=022,fmask=133   0 2
    > #UUID=0c6a4e1c-ba2e-4acb-bc47-01165a7a8d91    /media/oneterao     ext4        defaults,noatime                        0 2
```

---
## 6.1 Configuring the system : networking

```console
# vi /etc/conf.d/hostname # name the computer

    > hostname="gentoo-workstation"

# ip a # print, among other things, network interfaces names used below, e.g. enp0s3 and wlp2s0
```

### Default network config with `netifrc`

```console
# nvim /etc/conf.d/net

    > config_enp0s3="dhcp"
    > config_wlp2s0="dhcp"
    > dns_domain_lo="homenetwork"

# cd /etc/init.d
# ln -sv net.lo net.enp0s3 # create symlink from network service to enp0s3
# ln -sv net.lo net.wlp2s0 # create symlink from network service to wlp2s0
# rc-update add net.enp0s3 # activate enp0s3 at startup
# rc-update add net.wlp2s0 # activate wlp2s0 at startup

# nvim /etc/rc.conf # if needed: configure services, startup, and shutdown of the system
```

### Alternative network config with `dhcpcd`

* <https://wiki.gentoo.org/wiki/Network_management_using_DHCPCD>
* <https://wiki.gentoo.org/wiki/Dhcpcd>
* <https://wiki.archlinux.org/index.php/Dhcpcd>

Install `dhcpcd` and add it to `openrc`:
```console
# emerge -a dhcpcd
# rc-update add dhcpcd sysinit
# rc-service dhcpcd start
```

Any service starting with `net.` must not have a run level assigned (they are `netifrc` related). In
the above example, `net.wlp8s0` needs to be removed:
```console
# rc-config list | grep 'wpa_supplicant\|dhcpcd\|net.'
    > net.wlp8s0    default

# rc-update del net.wlp8s0 default
# rc-service net.wlp8s0 stop
```

Once the network setup works well, it is possible to completely remove `netifrc`:
```console
# echo "sys-apps/openrc -netifrc" >> /etc/portage/package.use/openrc
# etc-update
# emerge --ask --newuse --oneshot sys-apps/openrc
# emerge --ask --verbose --depclean net-misc/netifrc
# rm /etc/conf.d/net
# rm /etc/init.d/net.*
```

### WiFi config

To setup a WiFi connection, you may refer to my [`wpa_supplicant` cheat
sheet](../../networks/wpa_supplicant.md) and/or to those documentations:

* <https://wiki.gentoo.org/wiki/Wifi>
* <https://wiki.gentoo.org/wiki/iwlwifi>
* <https://wiki.gentoo.org/wiki/Wpa_supplicant>
* <https://wiki.archlinux.org/index.php/Wpa_supplicant>
* <https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Networking>

---
## 6.2 Configuring the system : users

```console
# passwd # define root password

# useradd -m -G users,audio,usb,portage,plugdev,video,wheel -s /bin/bash username
# passwd username
```

---
## 7.0 Installing system tools : `numlock`

```console
# rc-update add numlock default
# /etc/init.d/numlock start
```

---
## 7.1 Installing system tools : logger

```console
# emerge -a syslog-ng logrotate
# rc-update add syslog-ng default
```

See [system loggers cheat sheet](../../logs/system_loggers.md).

---
## 7.2 Installing system tools : Cron daemon task manager

Check that `cronie` is installed and added to `OpenRC`:
```console
# rc-update show -v | grep cronie
```

If not, you can emerge and add it:
```console
# emerge -a cronie
# rc-update add cronie default
```

Or emerge and add the Cron manager you want, e.g.:
```console
# emerge -a vixie-cron
# rc-update add vixie-cron default
```

See [Cron cheat sheet](../../admin/cron.md).

---
## 7.3 Installing system tools : NTP (clock auto sync via network)

```console
# emerge -a net-misc/ntp
# /etc/init.d/ntpd start
# rc-update add ntpd default
```

See [NTP cheat sheet](../../networks/time/ntp.md).

---
## 7.4 Installing system tools : `sshd` (optional: for servers)

```console
# rc-update add sshd default
```

See [ssh cheat sheet](../../networks/ssh.md).

---
## 7.5 Installing system tools : `elogind`

* <https://wiki.gentoo.org/wiki/Elogind>

---
## 7.6 Installing system tools : `sudo`

See [`sudo` cheat sheet](../../admin/sudo.md).

---
## 8.0 Configuring the bootloader for UEFI boot : grub

> TODO: Configuring the bootloader for BIOS boot (see Artix installation cheat sheet)

Ensure `/etc/portage/make.conf` contains: `GRUB_PLATFORMS="efi-64"` (note: `GRUB_PLATFORMS="pc"`
for MBR + BIOS).

Install `grub` tools:
```console
# emerge -a sys-boot/grub:2

# emerge -a ntfs3g    # if windows partition is installed (don't forget associated kernel drivers)
# emerge -a os-prober # if multiple Linux distribution are installed
```

**Make sure that the `/boot` partition is created and mounted**:
```console
# df -aTh # check mounted devices

# mkdir -p /boot # if not already created
# mount /dev/sdx1 /boot # if not already mounted
```

!!! Warning "Warning"
    At this point, if you want your `grub` menu to also add an entry for any Linux distro already
    installed on any other partition: **make sure to mount it** (it *just* need to be mounted for
    `os-prober` to find it)!

Install and configure `grub`:
```console
# grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
```

!!! Note "Note"
    The last command should have created a `EFI` folder in `/boot` directory.

**If** this error message shows up: `Could not delete variable: Read-only file sys`, then remount
`efivars` with read/write rights:
```console
# mount -o remount,rw /sys/firmware/efi/efivars
```

Then generate the `grub` configuration:
```console
# grub-mkconfig -o /boot/grub/grub.cfg
```

!!! Note "Note"
    The last command should have create a "grub" folder in the `/boot` directory. And it should
    have print something like: `Found linux image: /boot/vmlinuz-x.x.x-gentoo`

If not, double check from the `$ make install` kernel step (in `/usr/src/linux`). This previous
step is where the following files should have been copied into `/boot`:

* `config-x.x.x-gentoo`
* `System.map-x.x.x-gentoo`
* `vmlinuz-x.x.x-gentoo`

You can now check your `/boot` layout, it should look like this:
```console
# emerge -a app-text/tree
# tree -L 3 /boot

/boot
├── config-4.19.27-gentoo-r1
├── EFI
│   ├── Boot
│   │   └── bootx64.efi
│   ├── gentoo
│   │   └── grubx64.efi
│   └── kali
│       └── grubx64.efi
├── grub
│   ├── fonts
│   │   └── unicode.pf2
│   ├── grub.cfg
│   ├── grubenv
│   ├── locale
│   │   ├── ast.mo
│   │   ├── ca.mo
│   │   ├── da.mo
│   │   └── ...
│   ├── themes
│   │   └── starfield
│   └── x86_64-efi
│       ├── acpi.mod
│       ├── adler32.mod
│       ├── affs.mod
│       └── ...
├── System.map-4.19.27-gentoo-r1
└── vmlinuz-4.19.27-gentoo-r1
```

Notice that if you had accidentally deleted the `/boot` folder, you could recreate it entirely
using the previous commands, that are:
```console
# mkdir /boot
# cd /usr/src/linux
# make -j9 && make modules_install # optionnal if you already have run this
# make install
# grub-install --target=x86_64-efi --efi-directory=/boot
# grub-mkconfig -o /boot/grub/grub.cfg
```

---
## 8.1 Configuring the bootloader : rebooting the system

Finally:
```console
# exit # exit chroot
# rm -f /mnt/gentoo/*.tar.xz # delete archives
# cd /
# umount -l /mnt/gentoo/dev/{/shm,/pts,}
# umount -R /mnt/gentoo
# reboot
```


---
## 9.0 Post installation config

### `xorg` as graphic server

* See [Xorg](../../graphical/xorg.md).

### `dwm` as windows manager

* See [`dwm`](../../graphical/suckless_tools.md).


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
