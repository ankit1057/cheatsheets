---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Package Management
  - Sysadmin
  - System Administration
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Ebuilds

*TODO*

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Ebuild>
    * <https://wiki.gentoo.org/wiki/Ebuild_repository>
    * <https://wiki.gentoo.org/wiki/Basic_guide_to_write_Gentoo_Ebuilds>
    * <https://devmanual.gentoo.org/eclass-reference/ebuild/index.html>
    * <https://wiki.gentoo.org/wiki/Package_Manager_Specification>
    * <https://wiki.gentoo.org/wiki/Submitting_ebuilds>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

* Location: Gentoo ebuilds are located in `/var/db/repos/gentoo` (or in `/usr/portage` for older
  installs). The location is determined by the `repos.conf` file. Custom ebuilds are recommended to
  be placed in a custom ebuild repository, say `/var/db/repos/username`.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
