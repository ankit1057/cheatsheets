---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Package Management
  - Sysadmin
  - System Administration
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Gentoolkit

Gentoolkit is a suite of tools to ease the administration of a Gentoo system, and Portage in
particular. Gentoolkit contains tools to help users manage packages and keep track of what is going
on in their systems. Most users - particularly those who update systems often - will benefit from
having Gentoolkit installed.

The Gentoolkit commands have man pages, type "man <command>" for each command for full documentation.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Gentoolkit>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

```console
# emerge --ask app-portage/gentoolkit
```

---
## Use

List of related Gentoolkit cheat sheets:

* [equery](./equery.md)
* **TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
