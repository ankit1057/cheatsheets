---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Sysadmin
  - System Administration
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `dispatch-conf`

The `dispatch-conf` utility is included with Portage and is used to manage configuration file
updates. It allows system administrators to review then accept or reject upstream configuration
changes, which frequently happens when packages are updated.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Dispatch-conf>
    * <https://dev.gentoo.org/~zmedico/portage/doc/man/dispatch-conf.1.html>
    * <https://forums.gentoo.org/viewtopic.php?t=184107>
    * <https://wiki.gentoo.org/wiki/Handbook:AMD64/Portage/Tools>

!!! Note "Note"
    Note that, as alternatives on Portage, `etc-update` and `cfg-update` also are tools to merge
    configuration files. But they are not as simple and fully featured as `dispatch-conf`.


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

This package should be included by default with Portage.


---
## Config

* Enable the `diff --color` switch, which displays the different types of changes in different
  colors, for `dispatch-conf`:
```console
# vi /etc/dispatch-conf.conf
    > ...
    > diff="diff --color=always -Nu '%s' '%s'"
    > ...
```


---
## Use

* Run `dispatch-conf`:
```console
# dispatch-conf
```

* Update (replace) the current config file with the new config file and continue:
```console
u
```

* Zap (delete) the new config file and continue:
```console
z
```

* Skip to the next config file, leaving both the original config file and any `CONFIG_PRO‐TECTed`
files:
```console
n
```

* Edit the new config file, using the editor defined in EDITOR:
```console
e
```

* Interactively merge the current and new config files (press enter to list the sub commands of the
  merge tool):
```console
m
```

* Look at the differences between the pre merged and merged config files:
```console
l
```

* Toggle between the merged and pre merged config files (in  terms  of  which  should  be installed
using the u command):
```console
t
```

* Display a help screen:
```console
h
```

* Quit `dispatch-conf`:
```console
q
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
