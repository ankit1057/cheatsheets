---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Package Management
  - Sysadmin
  - System Administration
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `qlop`

A utility (from the [q applets](./q_applets.md)) for extracting information about emerged ebuilds
from Portage log files (`/var/log/emerge.log`), written in C. It can be useful when package
compilation times need to be estimated or to compare build times with other systems. It also allows
to check what is compiling at the moment and how long it will probably take - which is handy when
working in the console and don't have any other means to check it.

!!! Note "Alternative"
    * `genlop` from the [Gentoolkit](./gentoolkit.md), written in Perl.
        * See <https://github.com/gentoo-perl/genlop>.
        * See <https://wiki.gentoo.org/wiki/Genlop>.
        * See <https://wiki.gentoo.org/wiki/Genlop>.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Q_applets#Extracting_information_from_emerge_logs_.28qlop.29>
    * <https://github.com/gentoo/portage-utils>
    * <https://github.com/gentoo/portage-utils/blob/master/qlop.c>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

**TODO**

---
## Config

**TODO**

---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
