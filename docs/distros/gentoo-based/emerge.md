---
tags:
  - Linux Distributions
  - Distros
  - Gentoo Based
  - Package Management
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# emerge

???+ Note "Reference(s)"
    * <https://forums.gentoo.org/viewtopic-t-855048-start-0.html>
    * <https://wiki.gentoo.org/wiki/Gentoo_Cheat_Sheet>
    * <https://wiki.gentoo.org/wiki/Portage_Security>
    * <https://wiki.archlinux.org/index.php/Pacman/Rosetta>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
