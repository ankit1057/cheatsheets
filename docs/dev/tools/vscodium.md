---
tags:
  - Programming Languages
  - Tools
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# VSCodium

VSCodium is a community-driven, freely-licensed binary distribution of Microsoft’s editor VS Code.

???+ Note "Reference(s)"
    * <https://vscodium.com/>
    * <https://www.gnu.org/software/make/manual/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "apt"
        ```console
        $ wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
            | gpg --dearmor \
            | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
        $ echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
            | sudo tee /etc/apt/sources.list.d/vscodium.list
        $ sudo apt update && sudo apt install codium
        ```

    === "dnf"
        ```console
        $ sudo rpmkeys --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg
        $ printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=download.vscodium.com\nbaseurl=https://download.vscodium.com/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg\nmetadata_expire=1h" | sudo tee -a /etc/yum.repos.d/vscodium.repo
        $ sudo dnf install codium
        ```

    === "emerge"
        ```console
        # emerge -a app-editors/vscodium
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.vscodium
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.vscodium
            ```

    === "yay"
        ```console
        $ yay -S vscodium-bin vscodium-bin-marketplace vscodium-bin-features
        ```

    === "zypper"
        ```console
        $ sudo rpmkeys --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg
        $ printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=gitlab.com_paulcarroty_vscodium_repo\nbaseurl=https://download.vscodium.com/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg\nmetadata_expire=1h" | sudo tee -a /etc/zypp/repos.d/vscodium.repo
        $ sudo zypper in codium
        ```


---
## Config

TODO

My extensions:

- https://github.com/astral-sh/ruff-vscode.git
- https://github.com/tekumara/typos-vscode
- https://github.com/streetsidesoftware/vscode-spell-checker
- https://marketplace.visualstudio.com/items?itemName=vivaxy.vscode-conventional-commits

---
## Use

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
