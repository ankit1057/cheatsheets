---
tags:
  - Servers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `webapp-config` with Gentoo

`webapp-config` is Gentoo's installer for web based applications. It is used for automatic setup of
web applications in virtual hosting environments.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Webapp-config>
    * <https://www.linuxtricks.fr/wiki/webapp-config-installateur-gentoo-pour-applications-web>


---
## Table of content

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

Install `webapp-config`:
```console
# emerge -a webapp-config
```


---
## Config

Configure `webapp-config`:
```console
# vi /etc/vhosts/webapp-config
    > ...
    > vhost_server="nginx" # e.g. for nginx server
    > ...
```


---
## Use

* Install a web app (e.g. Nextcloud):
```console
# webapp-config -I nextcloud <version>
```

* List all the installed virtual copies:
```console
# webapp-config --list-installs
```

* Outputs the app name and app version of the installed apps (e.g. Nextcloud):
```console
# webapp-config --show-installed --host <hostname> -d nextcloud
```

* Update a previously installed web app (e.g. Nextcloud):
```console
# webapp-config -U nextcloud <version> --host <hostname>
```
> Then?: "To avoid timeouts with larger installations, you can instead run the following command
> from your installation directory: `$ ./occ upgrade`"

<br/>

* Remove an installed web app (e.g. Nextcloud):
```console
# webapp-config -C nextcloud <version>
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
