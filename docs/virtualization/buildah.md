---
tags:
  - Virtualization
  - Containers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Buildah

**TODO**

!!! Note "Info"
    * See [Containers vs. Virtual Machines (VMs): What’s the Difference?](https://web.archive.org/web/20220510161213/https://www.ibm.com/cloud/blog/containers-vs-vms)

???+ Note "Reference(s)"
    * <https://buildah.io/>
    * <https://github.com/containers/buildah>
    * <https://github.com/containers/buildah/tree/main/docs>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "apk"
        ```console
        # apt add buildah
        ```

    === "apt"
        ```console
        # apt install buildah
        ```

    === "dnf"
        ```console
        # dnf install buildah
        ```

    === "emerge"
        ```console
        # emerge -a app-containers/buildah
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.buildah
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.buildah
            ```

    === "pacman"
        ```console
        # pacman -S buildah
        ```

    === "yum"
        ```console
        # yum install buildah
        ```

    === "xbps"
        ```console
        # xbps-install -S buildah
        ```

    === "zypper"
        ```console
        # zypper install buildah
        ```


---
## Config

**TODO**


---
## Use

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
