---
tags:
  - Virtualization
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# VirtualBox

VirtualBox is a cross platform virtualization software that allows users to run guest operating
systems inside a host operating system without having to reboot.

!!! Note "Info"
    * See [Containers vs. Virtual Machines (VMs): What’s the Difference?](https://web.archive.org/web/20220510161213/https://www.ibm.com/cloud/blog/containers-vs-vms)

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/VirtualBox>
    * <https://wiki.archlinux.org/index.php/VirtualBox>
    * <https://wiki.archlinux.org/index.php/VirtualBox/Tips_and_tricks>
    * <https://www.virtualbox.org/wiki/Documentation>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config for host machine](#config-for-host-machine)
    * [Change the default VMs path](#change-the-default-vms-path)
    * [VM creation](#vm-creation)
    * [USB forwarding](#usb-forwarding)
* [Config for a windows guest machine](#config-for-a-windows-guest-machine)
* [Config for a Gentoo guest machine](#config-for-a-gentoo-guest-machine)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note "Gentoo kernel"
    Before emerging it, a correct [kernel
    config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) is needed:

    ```console
    $ cd /usr/src/linux
    # make nconfig # or `# make menuconfig`

        # Module support and virtualization
        ###################################
        # Double check here: <https://wiki.gentoo.org/wiki/VirtualBox#VirtualBox_2>
        #
        > [*] Enable loadable module support  ---> # Symbol: MODULES [=y]
        >   [ ]   Trim unused exported kernel symbols # Symbol: TRIM_UNUSED_KSYMS [=n]
        > [*] Virtualization  ---> # Symbol: VIRTUALIZATION [=y]
        ###################################
    ```

    !!! Warning " "
        After configuring the kernel don't forget to do a [kernel make and
        rebuild](../distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!

**TODO**: virtualbox with dkms vs virtualbox without dkms

!!! Note ""

    === "emerge"
        ```console
        # emerge -a virtualbox # or virtualbox-bin
        # gpasswd -a $USER vboxusers
        ```

    === "pacman"
        ```console
        # pacman -S virtualbox
        ```
        *then*
        <br/>
        * for arch linux:
        ```console
        # pacman -S virtualbox-host-modules-arch
        ```
        <br/>
        * for artix:
        ```console
        # pacman -S virtualbox-host-modules-artix
        ```
        See <https://wiki.archlinux.org/index.php/VirtualBox#Install_the_core_packages> for more
        details.

        *or (with dkms, see <https://forum.artixlinux.org/index.php/topic,1004.0.html>)*
        <br/>
        ```console
        # pacman -S linux-headers
        # pacman -S dkms
        # pacman -Ss virtualbox-dkms
        # pacman -Ss virtualbox-host-dkms
        ```

        ```console
        # gpasswd -a $USER vboxusers
        # modprobe vboxdrv
        $ lsmod | grep vboxdrv
        ```

    === "apt"
        ```console
        # apt install virtualbox
        ```

    === "yum"
        ```console
        # yum install virtualbox
        ```

    === "dnf"
        ```console
        # dnf install virtualbox
        ```


---
## Config for host machine

### Change the default VMs path

Open VirtualBox -> Go in "File" (top left corner) -> "Preferences" -> Select the "General" tab
-> change the "Default Machine Folder" path
to whatever path you want.

### VM creation

* When creating a VM, if you wonder about virtual hardware, `vdi`, `vmdk`, `vhd`, etc: see
  <https://superuser.com/questions/360517/what-disk-image-should-i-use-with-virtualbox-vdi-vmdk-vhd-or-hdd>

* When creating a windows 10 VM, you can download an ISO here:
  <https://www.microsoft.com/en-us/software-download/windows10ISO>

* When creating a windows 10 VM, you can get free product keys here:
  <https://www.windowsfeed.com/2020/02/windows-10-product-key-serial-keys.html>

### USB forwarding

If the host machine is a Linux machine, you might want to forward USB ports. If so, first you have
to make sure to be in the `vboxusers` group:
```console
$ sudo gpasswd -a yourusername vboxusers
```

Now log out and log back in so the changes can take effect. Then, to check which groups your user
account belongs to, run:
```console
$ groups yourusername
```
<br/>

Now that you belong in the right group, open VirtualBox -> Select your VM -> click on "Settings" ->
Select the "USB" tab -> make sure "Enable USB Controller" is on -> Click on "Add new USB" and
select the USB device you want to forward.

<br/>

You can also add/remove USB devices to forward while the VM is running, in this case select
"Devices" in the menu bar -> "USB" -> select the USB device you want to forward.


---
## Config for a windows guest machine

After installing a windows VM, don't forget to install the guest additions (on the windows guest):

* `$ pacman -S virtualbox-guest-iso` # -> `/usr/lib/virtualbox/additions/VBoxGuestAdditions.iso`

**OR**

* Open a web browser -> <https://download.virtualbox.org/virtualbox> -> select the latest
  VirtualBox version (e.g. 6.1.4 at the time of writing) -> `VBoxGuestAdditions_6.1.4.iso` ->
  download it and install it.

Then

Open VirtualBox -> select the VM -> settings -> storage tab -> right click on `Controller: SATA`
(in the "Storage Devices" list) -> Optical Drive -> Add ->
`/usr/lib/virtualbox/additions/VBoxGuestAdditions.iso` -> Open -> ... -> And don't forget to tick
"Live CD/DVD"


---
## Config for a Gentoo guest machine

When running Gentoo as a guest system, apply the below [kernel
config](../distros/gentoo-based/gentoo_kernel.md#kernel-config) on the guest system (either
built-in or as modules) to get proper support for the hardware emulated by VirtualBox:
```console
$ cd /usr/src/linux
# make nconfig # or `# make menuconfig`

    # Support for VirtualBox hardware
    # Double check here: <https://wiki.gentoo.org/wiki/VirtualBox#Kernel_configuration>
    #
    > Bus options (PCI etc.)  --->
    >     [*] Mark VGA/VBE/EFI FB as generic system framebuffer # Symbol: X86_SYSFB [=y]
    > Device Drivers  --->
    >     <*> Serial ATA and Parallel ATA drivers (libata)  ---> # Symbol: ATA [=y]
    >         [*] AHCI SATA support # Symbol: SATA_AHCI [=y]
    >         [*] ATA SFF support (for legacy IDE and PATA) # Symbol: ATA_SFF [=y]
    >         [*]   ATA BMDMA support # Symbol: ATA_BMDMA [=y]
    >         [*]     Intel ESB, ICH, PIIX3, PIIX4 PATA/SATA support # Symbol: ATA_PIIX [=y]
    >     <*> Network device support  ---> # Symbol: NETDEVICES [=y]
    >         <*> Ethernet driver support  ---> # Symbol: ETHERNET [=y]
    >             [*] Intel devices # Symbol: NET_VENDOR_INTEL [=y]
    >             [*]   Intel(R) PRO/1000 Gigabit Ethernet support # Symbol: E1000 [=y]
    >     Input device support  --->
    >         <*> Keyboards  ---> # Symbol: INPUT_KEYBOARD [=y]
    >             [*] AT keyboard # Symbol: KEYBOARD_ATKBD [=y]
    >         <*> Mice  ---> # Symbol: INPUT_MOUSE [=y]
    >             [*] PS/2 mouse # Symbol: MOUSE_PS2 [=y]
    >     <*> Virtio drivers  ---> # Symbol: VIRTIO_MENU [=y]
    >         <*> PCI driver for virtio devices # Symbol: VIRTIO_PCI [=y]
    >     Graphics support  --->
    >         <*> Direct Rendering Manager (XFree86 4.1.0 and higher DRI support)  ---> # Symbol: DRM [=y]
    >             [*] Enable legacy fbdev support for your modesetting driver # Symbol: DRM_FBDEV_EMULATION [=y]
    >         <*> Virtio GPU driver # Symbol: DRM_VIRTIO_GPU [=y]
    >         Frame buffer Devices  --->
    >             <*> Support for frame buffer devices  ---> # Symbol: FB [=y]
    >                 [*] Enable Firmware EDID # Symbol: FIRMWARE_EDID [=y]
    >             [*] Simple framebuffer support # Symbol: FB_SIMPLE [=y]
    >         Console display driver support  --->
    >             [*] Framebuffer Console support # Symbol: FRAMEBUFFER_CONSOLE [=y]
    >             [*]   Map the console to the primary display device # Symbol: FRAMEBUFFER_CONSOLE_DETECT_PRIMARY [=y]
    >     <*> Sound card support  ---> # Symbol: SOUND [=y]
    >         <*> Advanced Linux Sound Architecture  ---> # Symbol: SND [=y]
    >             <*> PCI sound devices  ---> # Symbol: SND_PCI [=y]
    >                 [*] Intel/SiS/nVidia/AMD/ALi AC97 Controller # Symbol: SND_INTEL8X0 [=y]
    >     <*> USB support  ---> # Symbol: USB_SUPPORT [=y]
    >         [*] xHCI HCD (USB 3.0) support # Symbol: USB_XHCI_HCD [=y]
    >         [*] EHCI HCD (USB 2.0) support # Symbol: USB_EHCI_HCD [=y]

    # [Optional] Virtual Box Guest integration support (no more need to manually install it)
    # See <https://www.omgubuntu.co.uk/2018/01/virtualbox-guest-additions-linux-kernel>
    #
    > Device Drivers  --->
    >     [*] Virtualization drivers  ---> Symbol: VIRT_DRIVERS [=y]
    >         <*>   Virtual Box Guest integration support # Symbol: VBOXGUEST [=y]
```

!!! Warning " "
    After configuring the kernel don't forget to do a [kernel make and
    rebuild](../distros/gentoo-based/gentoo_kernel.md#kernel-make-and-rebuild)!

To install the Guest Additions (⚠️ if the `VBOXGUEST` option hasn't been enabled in the kernel
configuration ⚠️), invoke the following command on the Gentoo guest system:
```console
# emerge --ask app-emulation/virtualbox-guest-additions
```

Make it persistently start across reboots, as well as the D-Bus service:
```console
# rc-update add virtualbox-guest-additions default
# rc-update add dbus default
```

To enable the shared clipboard, display resizing, seamless mode, and drag and drop... Make sure the
user running the X session (on the Gentoo guest system) belongs to the `vboxusers` group:
```console
# gpasswd -a <user-name> vboxusers
```
Changes will not take effect until the user signs out and then signs in again (re logins).

<br/>

Make sure that your `make.conf` file is well configured for VirtualBox:
```console
# vi /etc/portage/make.conf
    > ...
    > VIDEO_CARDS="virtualbox"
    > ...
```

!!! Warning " "
    After modifying the `USE` variable in your `make.conf`, don't forget to update the system using
    the following command so the changes take effect: `# emerge --ask --changed-use --deep @world`


---
## Use

Manage VirtualBox VMs from CLI with `vboxmanage`: <https://www.virtualbox.org/manual/ch08.html>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
