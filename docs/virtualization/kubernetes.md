---
tags:
  - Virtualization
  - Containers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Kubernetes


!!! Note "Info"
    * See [Containers vs. Virtual Machines (VMs): What’s the Difference?](https://web.archive.org/web/20220510161213/https://www.ibm.com/cloud/blog/containers-vs-vms)

???+ Note "Reference(s)"
    * <https://blog.zwindler.fr/2020/05/25/kubernetes-ressources-utiles-pour-bien-debuter/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
