---
tags:
  - Virtualization
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Wine

???+ Note "Reference(s)"
    * <https://usebottles.com/>
    * <https://www.youtube.com/watch?v=ttsGxu1o6pQ>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**

To reduce the burden on main Gentoo repository, older versions of Wine will be available only in
the wine overlay. These `ebuilds` will still be fully supported by the Gentoo Wine Project. This
will result in upstream stable releases and the several most recent upstream `devel` releases being
the only versions in `::gentoo`; all versions meeting the criteria for support within Gentoo will
be available in `::wine`.

To install the overlay you can either add the `repos.conf` file to your portage configuration
directory, add the repository via layman, or add the repository via `eselect-repository`.

* To add the `repos.conf` file:
```console
# wget https://dev.gentoo.org/~np-hardass/proj/wine/wine.conf -O \
    /etc/portage/repos.conf/wine.conf
```

* Edit the `/etc/portage/repos.conf/wine.conf` file so that "location" points to your desired
  folder to install the overlay.
```console
# emaint sync --repo wine
```

* To install the overlay via layman:
```console
# layman -a wine
```

* To install the overlay via `eselect-repository`:
```console
# eselect repository enable wine
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
