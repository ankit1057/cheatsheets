---
tags:
  - Shells
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Aliases

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Favorites](#favorites)

<!-- vim-markdown-toc -->

---
## Favorites

See my favorite aliases:

* <https://gitlab.com/stephane.tzvetkov/config/-/blob/artix-laptop/home/stephane/.config/zdotdir/.zshrc>
* <https://gitlab.com/stephane.tzvetkov/config/-/blob/gentoo-desktop/home/brioche/.config/zdotdir/.zshrc>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
