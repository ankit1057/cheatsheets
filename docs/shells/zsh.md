---
tags:
  - Shells
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Zsh

!!! Warning "Prerequisite(s)"
    * [XDG](./xdg.md)

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Zsh>
    * <https://wiki.gentoo.org/wiki/Zsh/Guide>
    * <https://wiki.archlinux.org/index.php/Zsh>
    * <https://github.com/koalaman/shellcheck>
    * <https://wiki.gentoo.org/wiki/Shell>
    * <https://wiki.archlinux.org/index.php/Command-line_shell>
    * <https://wiki.archlinux.org/index.php/Command-line_shell#Changing_your_default_shell>
    * <https://unix.stackexchange.com/questions/71253/what-should-shouldnt-go-in-zshenv-zshrc-zlogin-zprofile-zlogout>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [Avoid dotfile madness](#avoid-dotfile-madness)
* [`zim`](#zim)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a app-shells/zsh
        # emerge -a app-shells/zsh-completions
        # emerge -a app-shells/gentoo-zsh-completions
        # emerge -a app-eselect/eselect-sh
        ```

    === "pacman"
        ```console
        # pacman -S zsh
        # pacman -S zsh-completions
        ```

Define Zsh as the default shell for current user but don't configure it, let `zim` (see later)
handle the configuration:
```console
$ zsh
$ autoload -Uz zsh-newuser-install
$ zsh-newuser-install -f # init guide, do not configure anything: enter 'q' to quit immediatly
$ chsh -s /bin/zsh
```

!!! Note ""

    === "portage"
        ```console
        # euse -E zsh-completion
        # vi /etc/portage/make.conf # check that the use flag "zsh-completion" has been append:
            > ...
            > USE="... zsh-completion"
            > ...

        # emerge --ask --changed-use --deep @world # sys update for the use flag to take effect
        ```

Now exit Zsh and reboot:
```console
$ exit # exit zsh
# reboot
```

---
## Config

* The Zsh config will mostly be handled by `zim` (see the last section of this guide).

* Specify the location of your command history:
  ```console
  $ vi ${ZDOTDIR:-${HOME}}/.zshenv
    > ...
    > # ZSH & ZIM
  + > export ZDOTDIR="$XDG_CONFIG_HOME/zdotdir"
  + > export HISTFILE="${ZDOTDIR:-${HOME}}/.zhistory"
    > ...
  $ source ${ZDOTDIR:-${HOME}}/.zshenv
  ```

### Avoid dotfile madness

[Make sure you stay in control of your home
directory](https://web.archive.org/web/20210807080152/https://0x46.net/thoughts/2019/02/01/dotfile-madness/).

!!! Warning "Prerequisite"
    * [XDG](./xdg.md)

See [how to handle Zsh related
dotfiles](../admin/avoid_dotfile_madness.md#zsh-zlogout-zlogin-zcompdump-zcompdumpzwc-zimrc-zprofile-zshrczwcold-zshrczwc-zshrc-zhistory).


---
## `zim`

To start with a sane `zsh` config and a good `zsh` framework: see [`zim`](./zim.md)


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
