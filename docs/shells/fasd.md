---
tags:
  - Shells
  - Productivity
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# `fasd`

`fasd` is a command line productivity booster. `fasd` offers quick access to files and directories
for POSIX shells: it keeps track of files and directories you have accessed, so that you can
quickly reference them in the command line.

???+ Note "Reference(s)"
    * <https://github.com/clvv/fasd.git>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Avoid dotfile madness](#avoid-dotfile-madness)
* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->


---
## Avoid dotfile madness

Prior to installation, [make sure you stay in control of your home
directory](https://web.archive.org/web/20210807080152/https://0x46.net/thoughts/2019/02/01/dotfile-madness/).

!!! Warning "Prerequisite(s)"
    * [XDG](../shells/xdg.md)

See [how to handle `fasd` related dotfiles](../admin/avoid_dotfile_madness.md#fasd-fasd).

---
## Install

!!! Note ""

    === "pacman"
        ```console
        # pacman -S fasd
        ```

    === "apt"
        ```console
        # apt install fasd
        ```

    === "yum"
        ```console
        # yum install fasd
        ```

    === "dnf"
        ```console
        # dnf install fasd
        ```

    === "from source"
        Install `fasd` from git source:
        ```console
        $ cd ~/apps/src-apps
        $ git clone https://github.com/clvv/fasd.git
        $ cd fasd
        $ git checkout 1.0.1 # checkout to latest tag (1.0.1 at the time of writting)
        # make install
        ```

---
## Config

Configure `fasd`:
```console
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
    > ...
    > # fasd
    > eval "$(fasd --init auto)"
    >
    > alias a='fasd -a'        # any
    > alias s='fasd -si'       # show / search / select
    > alias d='fasd -d'        # directory
    > alias f='fasd -f'        # file
    > alias sd='fasd -sid'     # interactive directory selection
    > alias sf='fasd -sif'     # interactive file selection
    > alias z='fasd_cd -d'     # cd, same functionality as j in autojump
    > alias zz='fasd_cd -d -i' # cd with interactive selection
    >
    > alias v='f -e nvim'      # quick opening files with nvim
    > alias m='f -e mpv'       # quick opening files with mpv
    > alias o='a -e xdg-open'  # quick opening files with xdg-open

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
```

---
## Use

Examples:
```console
$ f foo            # list frecent files matching foo
$ a foo bar        # list frecent files and directories matching foo and bar
$ f js$            # list frecent files that ends in js
$ v foo            # run nvim on the most frecent file matching foo
$ mpv `f bar`      # run mpv on the most frecent file matching bar
$ z foo            # cd into the most frecent directory matching foo
$ open `sf pdf`    # interactively select a file matching pdf and launch `open`
$ nvim `f rc lo`   # like 'nvim /etc/rc.local'
$ nvim `f rc conf` # like 'nvim /etc/rc.conf'
$ o eng paper      # like 'xdg-open /you/dont/remember/where/english_paper.pdf'
$ v def conf       # like 'nvim /some/awkward/path/to/type/default.conf'
$ j abc            # like 'cd /hell/of/an/awkward/path/to/get/to/abcdef'
$ m movie          # like 'mpv /whatever/whatever/whatever/awesome_movie.mp4'
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
