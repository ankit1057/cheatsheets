---
tags:
  - Messengers
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# WeeChat

WeeChat is a highly extendable and feature rich IRC client.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/index.php/WeeChat>
    * <https://wiki.gentoo.org/wiki/WeeChat>
    * <https://github.com/poljar/weechat-matrix>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Avoid dotfile madness](#avoid-dotfile-madness)
* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Avoid dotfile madness

Prior to installation, [make sure you stay in control of your home
directory](https://web.archive.org/web/20210807080152/https://0x46.net/thoughts/2019/02/01/dotfile-madness/).

!!! Warning "Prerequisite(s)"
    * [XDG](../shells/xdg.md)

See [how to handle WeeChat related dotfiles](../admin/avoid_dotfile_madness.md#weechat-weechat).

---
## Install

TODO


---
## Config

TODO


---
## Use

TODO

```console
sudo pacman -S weechat
sudo pacman -S weechat-matrix

Optional Deps   : aspell: spellchecker support
                  guile: support for guile scripts [installed]
                  lua: support for lua scripts
                  perl: support for perl scripts [installed]
                  python: support for python scripts [installed]
                  ruby: support for ruby scripts
                  tcl: support for tcl scripts

In matrix:
/script load /usr/share/weechat/python/weechat-matrix.py

/set matrix.server.matrix_org.username johndoe
/set matrix.server.matrix_org.password jd_is_awesome

automatically load the script:
$ ln -s ../matrix.py ~/.weechat/python/autoload



Automatically connect to the server

/set matrix.server.matrix_org.autoconnect on

If everything works, save the configuration

/save

For using a custom (not matrix.org) matrix server:

    Add your custom server to the plugin:

    /matrix server add myserver myserver.org

    Add the appropriate credentials

    /set matrix.server.myserver.username johndoe
    /set matrix.server.myserver.password jd_is_awesome

    If everything works, save the configuration

    /save
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
