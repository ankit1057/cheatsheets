---
tags:
  - Office
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# LibreOffice

LibreOffice is a free and open-source office productivity software suite, a project of The Document
Foundation (TDF). It was forked in 2010 from OpenOffice, an open-sourced version of the earlier
StarOffice. The LibreOffice suite consists of programs for word processing, creating and editing of
spreadsheets, slideshows, diagrams and drawings, working with databases, and composing mathematical
formula.

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Use](#use)
    * [Troubleshooting](#troubleshooting)
        * [LibreOffice takes a lot of time to save with 100% CPU load](#libreoffice-takes-a-lot-of-time-to-save-with-100-cpu-load)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "apk"
        ```console
        # apk add libreoffice
        ```

    === "apt"
        ```console
        # apt install libreoffice
        ```

    === "dnf"
        ```console
        # dnf install libreoffice
        ```

    === "emerge"
        ```console
        # emerge -a app-office/libreoffice
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.libreoffice
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.libreoffice
            ```

    === "pacman"
        ```console
        # pacman -S libreoffice
        ```

    === "yum"
        ```console
        # yum install libreoffice
        ```

    === "xbps"
        ```console
        # xbps-install -S libreoffice
        ```

    === "zypper"
        ```console
        # zypper install libreoffice
        ```

---
## Use

### Troubleshooting

#### LibreOffice takes a lot of time to save with 100% CPU load

???+ Note "Reference(s)"
    * <https://ask.libreoffice.org/t/libreoffice-6-on-ubuntu-linux-soffice-bin-100-cpu-load/33945/11>
    * <https://ask.libreoffice.org/t/why-is-soffice-bin-using-60-cpu-with-nothing-open/27405/2>
    * <https://serverfault.com/questions/953809/soffice-bin-high-cpu-usage/986039#986039>
    * <https://ask.libreoffice.org/t/writer-soffice-bin-eats-all-the-cpu-after-a-while-until-save/55908/12>

If, after waiting 4 to 5 minutes, your document(s) are still saving with 100% CPU load, then run `$
pkill soffice.bin`and re-open LibreOffice, letting it recover your open document(s).


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
