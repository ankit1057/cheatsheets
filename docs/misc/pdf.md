---
tags:
  - PDF
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Ghostscript

Ghostscript is an interpreter for the PostScript language and PDF files. It consists of a
PostScript interpreter layer and a graphics library.

There are a family of other products, including GhostPCL, GhostPDF, and GhostXPS that are built
upon the same graphics library (note that GhostPDL, pulls all these languages into a single
executable). Between them, this family of products offers native rendering of all major page
description languages.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/PDF,_PS_and_DjVu>
    * <https://www.ghostscript.com/>
    * <https://www.ghostscript.com/doc/current/WhatIsGS.htm>
    * <https://ghostscript.readthedocs.io/en/gs10.0.0/Use.html>
    * <https://www.gnu.org/software/ghostscript/>
    * <https://www.gnu.org/software/ghostscript/intro.html>
    * <https://en.wikipedia.org/wiki/Ghostscript>
    * <https://stackoverflow.com/questions/2507766/merge-convert-multiple-pdf-files-into-one-pdf>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [PDF manipulations](#pdf-manipulations)

<!-- vim-markdown-toc -->

---
## Install


!!! Note ""

    === "emerge"
        ```console
        # emerge -a app-text/ghostscript-gpl
        ```

    === "pacman"
        ```console
        # pacman -S ghostscript
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.ghostscript
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.ghostscript
            ```

    === "apt"
        ```console
        # apt install ghostscript
        ```

    === "yum"
        ```console
        # yum install ghostscript
        ```

    === "dnf"
        ```console
        # dnf install ghostscript
        ```

---
## PDF manipulations

* Compress PDF:
    ```console
    $ gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=compressed_PDF_file.pdf input_PDF_file.pdf
    ```

!!! Tip ""
    The `dPDFSETTINGS` option allows the following levels of compression:
    * `/prepress`: (default) higher quality output (300 dpi) but bigger size
    * `/ebook`: medium quality output (150 dpi) with moderate output file size
    * `/screen`: lower quality output (72 dpi) but smallest possible output file size

!!! Tip ""
    The `-r` option allows you to specify directly the resolution you want (e.g. `-r42` for 42
    dpi.

!!! Tip ""
    You can add the following to your zshrc:
    ```console
    pdfcompress ()
    {
        gs -q -dNOPAUSE -dBATCH -dSAFER -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dEmbedAllFonts=true -dSubsetFonts=true -dColorImageDownsampleType=/Bicubic -dColorImageResolution=32 -dGrayImageDownsampleType=/Bicubic -dGrayImageResolution=32 -dMonoImageDownsampleType=/Bicubic -dMonoImageResolution=32 -dNOTRANSPARENCY -sOutputFile=${1%.*}_compressed.pdf $1;
    }
    ```

* Merge PDFs:
    ```console
    $ gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=merged.pdf mine1.pdf mine2.pdf
    ```

* Merge with compression:
    ```console
    $ gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/default -dNOPAUSE -dQUIET -dBATCH -dDetectDuplicateImages -dCompressFonts=true -r150 -sOutputFile=merged.pdf mine1.pdf mine2.pdf
    ```

* Convert PNG to PDF (the `imagemagick` package might be needed?):
    ```console
    $ convert input.png output.pdf
    ```

* Convert PDF to PNG (the `imagemagick` package might be needed?):
    ```console
    $ convert output.pdf input.png
    ```

    !!! Tip ""
        If the the background is converted into a transparent background, then just add the
        `flatten` option: `$ convert -flatten output.pdf input.png`.

**WIP**


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
