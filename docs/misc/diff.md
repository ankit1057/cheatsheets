---
tags:
  - Standard Unix Programs
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Diff

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->

**TODO**

* Diff two directories:
```console
$ diff -ENwbur ./dir1/ ./dir2/ > diff.log
```

* Diff two directories, ignoring contents of `.svn` and `.git` directories, but also individual
  files named `*.zip/`, `*.gz/`, etc:
```console
$ diff -ENwbur
   --exclude="*~" \
   --exclude=".svn" \
   --exclude=".git" \
   --exclude="*.zip*" \
   --exclude="*.gz" \
   --exclude="*.tar" \
   ./dir1/ ./dir2/ > diff.log
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
