---
tags:
  - Files
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# difftastic

`difftastic` is a structural diff tool that compares files based on their syntax.

???+ Note "Reference(s)"
    * <https://github.com/Wilfred/difftastic>
    * <https://difftastic.wilfred.me.uk/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [Git external diff](#git-external-diff)
    * [`git-difftool`](#git-difftool)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "apk"
        Not available with `apk`.

    === "apt"
        Not available with `apt`.

    === "dnf"
        Not available with `dnf`.

    === "emerge"
        ```console
        # emerge -a dev-util/difftastic
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.difftastic
            ```

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.difftastic
            ```

    === "pacman"
        ```console
        # pacman -S difftastic
        ```

    === "yum"
        Not available with `yum`.

    === "xbps"
        Not available with `xbps`.

    === "zypper"
        Not available with `zypper`.


---
## Config

### Git external diff

* Git supports [external diff
  tools](https://git-scm.com/docs/diff-config#Documentation/diff-config.txt-diffexternal). You can
  use `GIT_EXTERNAL_DIFF` for a one-off git command, e.g.:
    ```console
    $ GIT_EXTERNAL_DIFF=difft git diff
    ```

* If you want to use difftastic by default, use `git config`:
    ```console
    $ git config diff.external difft # Set git configuration for the current repository
    $ git config --global diff.external difft # Set git configuration for all repositories
    ```

    After running `git config`, `git diff` will use `difft` automatically. Other git commands
    require `--ext-diff` to use external diff, e.g. `git log` and `git show`:
    ```console
    $ git diff
    $ git log -p --ext-diff
    $ git show e96a7241760319 --ext-diff
    ```

* See also [git diff](../versionning/git.md#git-diff).

### `git-difftool`

* [git difftool](https://git-scm.com/docs/git-difftool) is a git command for viewing the current changes with a different diff tool. It's
  useful if you want to use difftastic occasionally.

* Add the following to your `.gitconfig` to use difftastic as your difftool:
    ```console
    $ vi $HOME/.gitconfig # or $XDG_CONFIG_HOME/git/config or wherever
        > ...
      + >
      + > [diff]
      + >     tool = difftastic
      + >
      + > [difftool]
      + >     prompt = false
      + >
      + > [difftool "difftastic"]
      + >     cmd = difft "$LOCAL" "$REMOTE"
      + >
      + > [pager]
      + >     # Use a pager for large output, just like other git commands
      + >     difftool = true
      + >
      + > [alias]
      + >     # `git dft` is less to type than `git difftool`
      + >     dft = difftool
    ```

* You can then run `$ git difftool` to see current changes with difftastic.

* See also [git difftool](../versionning/git.md#git-difftool).

---
## Use

* Diffing files:
    ```console
    $ difft sample_files/before.js sample_files/after.js
    ```

* Diffing directories:
    ```console
    $ difft sample_files/dir_before/ sample_files/dir_after/
    ```
    Difftastic will recursively walk the two directories, diffing files with the same name.

    The --skip-unchanged option is useful when diffing directories that contain many unchanged
    files.

* You can read a file from `stdin` by specifying `-` as the file path:
    ```console
    $ cat sample_files/before.js | difft - sample_files/after.js
    ```

* Difftastic guesses the language used based on the file extension, file name, and the contents of
  the first lines. You can override the language detection by passing the `--language` option.
  Difftastic will treat input files as if they had that extension, and ignore other language
  detection heuristics.
    ```console
    $ difft --language cpp before.c after.c
    ```

* Difftastic includes a range of configuration CLI options, see `$ difft --help` for the full list.
  Difftastic can also be configured with environment variables. These are also visible in `--help`.
  For example, `DFT_BACKGROUND=light` is equivalent to `--background=light`. This is useful when
  using tools like git, where you are not invoking the `difft` binary directly.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
