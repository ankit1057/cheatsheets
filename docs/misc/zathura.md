---
tags:
  - Document Viewer
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Zathura

Zathura is a highly customizable and functional document viewer with `vi` styled keybindings. It
provides a minimalistic and space saving interface as well as an easy usage that mainly focuses on
keyboard interaction.

???+ Note "Reference(s)"
    * <https://wiki.gentoo.org/wiki/Zathura>
    * <https://wiki.archlinux.org/index.php/Zathura>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "emerge"
        ```console
        # emerge -a zathura
        ```

    === "pacman"
        ```console
        # pacman -S zathura
        ```

    === "apt"
        ```console
        # apt install zathura
        ```

    === "yum"
        ```console
        # yum install zathura
        ```

    === "dnf"
        ```console
        # dnf install zathura
        ```


---
## Config

Create and fill `zathurarc`, e.g. invert colors of opened files:
```console
$ vi ~/.config/zathura/zathurarc
    > set recolor
```


---
## Use

* Reverse colors:
```console
<Ctrl-r>
```

* Create bookmark:
```console
:bmark bookmark-name
```

* Delete bookmark:
```console
:bdelete bookmark-name
```

* List bookmarks:
```console
:blist bookmark-name
```
Bookmarks, as well as some default file configuration, are located here: `~/.local/share/zathura`.

<br/>

* Switch to 2 pages per row, and switch back to 1 page per row:
```console
bd
```

Fit page(s) to the screen:
```console
a
```
or
```console
s
```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
