---
tags:
  - Science
  - Scientific Research
  - Medical Research
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Folding@home

Folding@home (FAH or F@H) is a distributed computing project for performing molecular dynamics
simulations of protein dynamics. Its initial focus was on protein folding but has shifted to more
biomedical problems, such as Alzheimer's disease, cancer, COVID-19, and Ebola. The project uses the
idle processing resources of personal computers owned by volunteers who have installed the software
on their systems.

???+ Note "Reference(s)"
    * <https://foldingathome.org/>
    * <https://forums.gentoo.org/viewtopic-p-8453378.html?sid=5713e114176c7f26f83fb4da9909ad61>
    * <https://bbs.archlinux.org/viewtopic.php?id=254001>
    * <https://foldingforum.org/viewtopic.php?f=81&t=33353>

!!! Warning "Warning"
    This cheat sheet might be still Gentoo + Portage + OpenRC oriented, since I just tested it with
    this setup...

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)
    * [Troubleshooting](#troubleshooting)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "apk"
        TODO

    === "apt"
        TODO

    === "dnf"
        TODO

    === "portage + emerge"
        ```console
        # vi /etc/portage/package.license
            > # (manual) foldingathome
            > sci-biology/foldingathome FAH-EULA-2014 FAH-special-permission
            >
            > ...
        
        # vi /etc/portage/package.accept_keywords
            > # (manual) last foldingathome
            > sci-biology/foldingathome ~amd64
            >
            > ...
        
        # emerge -a sci-biology/foldingathome
        ```

    === "nix"

        === "on NixOS"
            ```console
            # nix-env -iA nixos.fahclient
            ```
            TODO: see also `nixos.fahviewer` and `nixos.fahcontrol`

        === "on non-NixOS"
            ```console
            # nix-env -iA nixpkgs.fahclient
            ```
            TODO: see also `nixpkgs.fahviewer` and `nixpkgs.fahcontrol`

    === "~~pacman~~ AUR (by hand)"
        Install with [AUR](../distros/arch-based/aur.md):
        ```console
        $ mkdir -p ~/apps/aur-apps
        $ cd ~/apps/aur-apps
        $ git clone https://aur.archlinux.org/packages/foldingathome/
        $ cd foldingathome
        $ makepkg -is # --syncdeps to auto-install deps, --install to install after building
        ```

    === "yum"
        TODO

    === "xbps"
        TODO

    === "zypper"
        TODO


---
## Config

Optionally, get a passkey:

* <https://foldingathome.org/support/faq/points/passkey/>
* <https://apps.foldingathome.org/getpasskey>

Configure Folding at home:
```console
$ sudo /opt/foldingathome/FAHClient --configure
# /opt/foldingathome/config.xml # e.g. it might result in:
    > <config>
    >   <!-- User Information -->
    >   <passkey v='optional_passphrase'/>
    >   <team v='0'/>
    >   <user v='Anonymous'/>
    >
    >   <!-- Folding Slots -->
    >   <slot id='0' type='CPU'/>
    > </config>
```

**TODO**

Add the `foldingathome` user to the video group!


---
## Use

* Monitor your `foldingathome` client:
```console
$ firefox https://client.foldingathome.org/
```

* Get all commands details
```console
$ sudo -u foldingathome /opt/foldingathome/FAHClient --help
```

* Get send command details
```console
$ sudo -u foldingathome /opt/foldingathome/FAHClient --chdir "/opt/foldingathome/" --send-command help
```

* Run
```console
$ sudo -u foldingathome /opt/foldingathome/FAHClient --chdir "/opt/foldingathome/"
```

* Gracefully stop, after completing current steps, on an already running client:
```console
$ sudo -u foldingathome /opt/foldingathome/FAHClient --chdir "/opt/foldingathome/" --send-finish
```

* Immediately stop an already running client:
```console
$ sudo -u foldingathome /opt/foldingathome/FAHClient --chdir "/opt/foldingathome/" --send-command "shutdown"
```

* Run folding at home only at night (from 0 to 6):
```console
$ sudo contab -e
    > ...
    > 0 0 * * * sudo -u foldingathome /opt/foldingathome/FAHClient --chdir "/opt/foldingathome/"
    > 0 6 * * * sudo -u foldingathome /opt/foldingathome/FAHClient --chdir "/opt/foldingathome/" --send-finish
```

!!! Note ""

    === "OpenRC"
        * Start
        ```console
        # rc-service foldingathome start
        ```
        * Stop
        ```console
        # rc-service foldingathome stop
        ```
        * Run Folding at home in the background at boot:
        ```console
        # rc-update add foldingathome default
        ```

    === "Runit"
        * Depending on your `runit` implementation, either run:
          ```console
          # ln -s /etc/runit/sv/foldingathome /service
          ```
          **or** run:
          ```console
          # ln -s /etc/runit/sv/foldingathome /var/service
          ```
          **or** run:
          ```console
          # ln -s /etc/runit/sv/foldingathome /run/runit/service
          ```
        * Start
          ```console
          # sv up foldingathome
          ```
        * Stop
            **TODO**

    === "SysVinit"
        * Start
          ```console
          # service foldingathome start
          ```
        * Stop
          ```console
          # service foldingathome stop
          ```
        * Run Folding at home in the background at boot:
          ```console
          # chkconfig foldingathome on
          ```

    === "SystemD"
        * Start
        ```console
        # systemctl start foldingathome
        ```
        * Stop
        ```console
        # systemctl stop foldingathome
        ```
        * Run Folding at home in the background at boot:
        ```console
        # systemctl enable foldingathome
        ```

### Troubleshooting

* If getting this kind of error: `PRAGMA synchronous=NORMAL`, `database is locked`, then
  `foldingathome` run multiple instances, try to stop them:
    ```console
    $ sudo killall FAHClient
    $ sudo -u foldingathome /opt/foldingathome/FAHClient --chdir "/opt/foldingathome/"
    ```

* If getting this kind of error:
  <http://www.gromacs.org/Documentation/Errors#There_is_no_domain_decomposition_for_n_nodes_that_is_compatible_with_the_given_box_and_a_minimum_cell_size_of_x_nm>
  Then you can try to lower the number of used CPU (e.g. from 16 to 12):
  ```console
  # vi /opt/foldingathome/config.xml
      > ...
    + >
    + > <!-- Folding Slots -->
    + > <slot id='0' type='CPU'>
    + > <cpus v="12"/>
    + > </slot>
  ```


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
