---
tags:
  - Firmware update
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---



# fwupd

**TODO**

* <https://wiki.archlinux.org/title/Fwupd>
* <https://azlinux.fr/fwupd-un-outil-pour-mettre-a-jour-le-firmware-de-votre-pc/>

## gnome-firmware

**TODO**

* <https://gitlab.gnome.org/World/gnome-firmware>
* <https://wiki.archlinux.org/title/Fwupd#Graphical_front-ends>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

<!-- vim-markdown-toc -->


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
