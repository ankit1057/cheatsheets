---
tags:
  - Self-hosted
  - Torrent
  - Peer to Peer
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# rTorrent

rTorrent is a fast and highly configurable torrent client based on `libtorrent` and `ncurses`.

???+ Note "Reference(s)"
    * <https://www.youtube.com/watch?v=2n93ioqHf-U>
    * <https://wiki.gentoo.org/wiki/RTorrent>
    * <https://wiki.archlinux.org/index.php/RTorrent>
    * <https://github.com/rakshasa/rtorrent/wiki/CONFIG-Template>
    * <https://blog.datacargo.fr/2016/07/27/o-trouver-la-meilleure-blocklist-pour-deluge-et-les-autres-clients-torrent/>
    * <https://gist.github.com/johntyree/3331662>
    * <https://www.iblocklist.com/lists>

---
## Tables of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
    * [if no IPv6](#if-no-ipv6)
    * [Email notifications](#email-notifications)
* [Use](#use)
    * [Tips](#tips)

<!-- vim-markdown-toc -->

---
## Install

!!! Note ""

    === "portage"
        ```console
        # emerge -a net-p2p/rtorrent
        ```

    === "pacman"
        ```console
        # pacman -S rtorrent
        ```

    === "apt"
        ```console
        # apt install rtorrent
        ```

    === "yum"
        ```console
        # yum install rtorrent
        ```

    === "dnf"
        ```console
        # dnf install rtorrent
        ```


---
## Config

* By default, rTorrent will search its config file here `~/.rtorrent.rc`. But if rTorrent is run
  with the `-o import=/path/to/rtorrent.rc` option, the `rtorrent.rc` file can be created anywhere.
  E.g.:
  ```console
  $ mkdir -p ~/.config/rtorrent
  $ cp /usr/share/doc/rtorrent/rtorrent.rc ~/.config/rtorrent
  ```
  The `/usr/share/doc/rtorrent/rtorrent.rc` file is pretty self explanatory and provides good
  reference links in its top header.

* Set a default session directory (**run only one rTorrent instance per session**):
  ```console
  $ mkdir -p ~/.config/rtorrent/session
  $ vi ~/.config/rtorrent/rtorrent.rc
      > ...
      > session.path.set = ~/.config/rtorrent/session
      > ...
  ```

* Set a default download directory:
  ```console
  $ vi ~/.config/rtorrent/rtorrent.rc
      > ...
      > drectory.default.set = /media/rdownloads
      > ...
  ```

* Set a watch directory (new torrents added here will auto download, those removed will stop
  download/sharing):
  ```console
  $ vi ~/.config/rtorrent/rtorrent.rc
      > ...
      > schedule2 = watch_directory,5,5,load.start=/media/rwatch/*.torrent
      > ...
  ```

* Enable encryption:
  ```console
      > ...
      > protocol.encryption.set = allow_incoming,try_outgoing,enable_retry
      > ...
  ```

* Enable [DHT](https://en.wikipedia.org/wiki/Distributed_hash_table) support:
  ```console
      > ...
      > dht.mode.set = auto
      > dht.port.set = 6881
      > protocol.pex.set= yes
      > ...
  ```

### if no IPv6

> See <https://github.com/rakshasa/rtorrent/issues/732>

Add the following to `rtorrent.rc`
```console
    > ...
    > bind 0.0.0.0
```

### Email notifications

TODO

See <https://wiki.archlinux.org/index.php/RTorrent#Notification_with_Google_Mail>


---
## Use

* Run rTorrent with an alternative `rtorrent.rc` location:
  ```console
  $ rtorrent -o import=~/.config/rtorrent/rtorrent.rc
  ```

* Run rTorrent with an other download directory:
  ```console
  $ rtorrent -d /media/rdownloads/movies
  ```

* Run rTorrent with a specific torrent file:
  ```console
  $ rtorrent /path/to/file.torrent
  ```

* `Ctrl-q`      Quit rTorrent.
* `Ctrl-s`      Start download. Runs hash first unless already done.
* `Ctrl-d`      Stop an active download or remove a stopped download
* `Ctrl-k`      Stop and close the files of an active download.
* `Ctrl-r`      Initiate hash check of torrent. Starts downloading if file is not available.
* `Ctrl-o`      Specify the download directory for a added, but not started torrent.
* `Left`        Returns to the previous screen
* `Right`       Goes to the next screen
* `Backspace`   Adds and starts the specified *.torrent
* `Return`      Adds and doesn't start the specified *.torrent
* `a|s|d`       Increase global upload throttle about 1|5|50 KB/s
* `A|S|D`       Increase global download throttle about 1|5|50 KB/s
* `z|x|c`       Decrease global upload throttle about 1|5|50 KB/s
* `Z|X|C`       Decrease global download throttle about 1|5|50 KB/s

### Tips

* Check the IP used by rTorrent: <http://checkmyip.torrentprivacy.com>


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
