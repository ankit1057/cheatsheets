---
tags:
  - Self-hosted
  - RSS
  - Feed Reader
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Miniflux

Miniflux is a minimalist self hosted feed (RSS) reader.

!!! Note "Prerequisite(s)"
    * [PostgreSQL](../data-bases/postgresql.md)

???+ Note "Reference(s)"
    * <https://github.com/miniflux/miniflux>
    * <https://miniflux.app/docs/index.html>
    * <https://alternativeto.net/software/miniflux/?license=opensource>
    * <https://suckless.org/rocks/> (see "Feed aggregators" section)

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
    * [Uninstall](#uninstall)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

PostgreSQL should be started at this point.

Create the Miniflux user and Miniflux database:
```console
# su
# su postgres
$ createuser -P miniflux # enter miniflux pwd, confirm pwd & enter postgres pwd
$ createdb -O miniflux miniflux2
$ psql miniflux -c 'create extension hstore'
```

Enabling `HSTORE` extension for PostgreSQL:
```console
# sudo -u postgres psql $MINIFLUX_DATABASE
    > CREATE EXTENSION hstore;
    > \q
```

Download the latest Miniflux executable and install it:
```console
# wget https://github.com/miniflux/miniflux/releases/download/2.0.16/miniflux-linux-amd64
# mv miniflux-linux-amd64 /usr/local/bin/miniflux
# chmod +x /usr/local/bin/miniflux
$ vi $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
    > ...
  + >
  + > # MINIFLUX
  + > export DATABASE_URL="user=postgres password=postgres dbname=miniflux2 sslmode=disable"
    > ...

$ source $HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
# miniflux -migrate # create admin user
```

### Uninstall

TODO


---
## Config

See <https://github.com/miniflux/miniflux>


---
## Use

Run Miniflux:
```console
$ miniflux
```

Open it in a browser at "127.0.0.1:8080"


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
