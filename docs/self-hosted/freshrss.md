---
tags:
  - Self-hosted
  - RSS
  - Feed Reader
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Fresh RSS

Fresh RSS is a self hosted feed (RSS) reader.

???+ Note "Reference(s)"
    * <https://github.com/FreshRSS/FreshRSS/tree/edge/Docker>
    * <https://github.com/Korbak/freshrss-invidious>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [With Docker](#with-docker)
    * [Install and run](#install-and-run)
    * [Update](#update)
    * [How to add an extension](#how-to-add-an-extension)

<!-- vim-markdown-toc -->

---
## With Docker

!!! Note "Prerequisite(s)"
    * [Docker](../virtualization/docker.md)

### Install and run

```console
$ docker run -d --restart unless-stopped --log-opt max-size=10m \
  -p 36006:80 \
  -e TZ=Europe/Paris \
  -e 'CRON_MIN=1,31' \
  -v /var/www/FreshRSS/data:/var/www/FreshRSS/data \
  -v /var/www/FreshRSS/extensions:/var/www/FreshRSS/extensions \
  --name freshrss \
  freshrss/freshrss
```

### Update

TODO

### How to add an extension

The first step is to put the extension into your FreshRSS extension directory:

```console
$ cd /var/www/FreshRSS/extensions/
$ wget https://github.com/kevinpapst/freshrss-youtube/archive/master.zip
$ unzip master.zip
$ mv freshrss-youtube-master/xExtension-YouTube .
$ rm -rf freshrss-youtube-master/
```

Then switch to your browser <https://localhost:36006/FreshRSS/p/i/?c=extension> and activate it.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
