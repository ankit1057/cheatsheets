---
tags:
  - Self-hosted
  - Documentation
  - Wiki
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# XWiki

XWiki is an open-source enterprise ready wiki server written in Java, with a focus on
extensibility.

???+ Note "Reference(s)"
    * <https://wiki.archlinux.org/title/XWiki>
    * <https://www.xwiki.org/xwiki/bin/view/Documentation/>
    * <https://store.xwiki.com/xwiki/bin/view/Extension/>
    * <https://www.xwiki.org/xwiki/bin/view/Documentation/DevGuide/Tutorials/CreatingExtensions/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
    * [Uninstall](#uninstall)
* [Config](#config)
* [Use](#use)
    * [Tips](#tips)
        * [Install XWiki plugins from source](#install-xwiki-plugins-from-source)

<!-- vim-markdown-toc -->

---
## Install

> On Debian based with PostgreSQL and Tomcat 9

???+ Note "Reference(s)"
    * <https://www.xwiki.org/xwiki/bin/view/Documentation/AdminGuide/Installation/InstallationViaAPT/>

* First, you have to configure your package manager in order to use the XWiki repository. This can
  be done simply, using this command:
  ```console
  $ wget -q "https://maven.xwiki.org/public.gpg" -O- | sudo apt-key add -
  $ sudo wget "https://maven.xwiki.org/stable/xwiki-stable.list" -P /etc/apt/sources.list.d/
  ```

* Then do a full system update/upgrade:
  ```console
  # apt update
  # apt list --upgradable
  # apt full-upgrade
  # apt --purge autoremove # removes deps and systemwide configuration files too
  ```

* Now you can see what this repository offers:
  ```console
  $ apt-cache search xwiki
  ```

* The following will install everything you need to run XWiki with PostgreSQL and Tomcat 9:
  ```console
  $ sudo apt-get install xwiki-tomcat9-pgsql
  ```

* Open `/etc/xwiki/xwiki.cfg` file and change lines:
  ```console
  $ sudo vi /etc/xwiki/xwiki.cfg
    > ...
    > #-# Cookie encryption keys. You SHOULD replace these values with any random string,
    > #-# as long as the length is the same.
    > xwiki.authentication.validationKey=your1stkeyhere
    > xwiki.authentication.encryptionKey=your2ndkeyhere
    > ...
  ```

* If you need, add password to the built-in `superadmin` account, by changing line:
  ```console
  $ sudo vi /etc/xwiki/xwiki.cfg
    > ...
    > #-# Enable to allow superadmin. It is disabled by default as this could be a
    > #-# security breach if it were set and you forgot about it. Should only be enabled
    > #-# for recovering the Wiki when the rights are completely messed.
    > xwiki.superadminpassword=yourpassword
    > ...
  ```

### Uninstall

* If you want to remove XWiki from your system, you only have to uninstall the package you have
  installed at the beginning of this documentation. For example:
  ```console
  $ sudo apt-get --purge remove xwiki-common
  $ sudo apt-get --purge autoremove
  ```

---
## Config

You need to configure your `Servlet` container so that XWiki has enough memory. You will need to
tune the value to your need. You should check the logs and see if there are any "out of memory"
errors. Here are some good default values:

For Java 8 (i.e. `XWiki >= 8.1`):
```console
$ sudo vi /etc/default/tomcat9
    > ...
    > # You may pass JVM startup parameters to Java here. If unset, the default
    > # options will be: -Djava.awt.headless=true -XX:+UseG1GC
    > # JAVA_OPTS="-Djava.awt.headless=true -XX:+UseG1GC"
    > JAVA_OPTS="-Djava.awt.headless=true -Xmx2048m"
    > ...
```

In most Debian based systems the default random implementation is `/dev/random` which can be very
slow. `/dev/urandom` is much faster and is fine for XWiki:
```console
$ sudo vi /etc/default/tomcat9
    > ...
    > # In most Debian-based systems the default random implementation is /dev/random which can be very
    > # slow. /dev/urandom is much faster and it's fine for XWiki's needs.
    > JAVA_OPTS="${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom"
```

* Then simply browse to <http://localhost:8080/xwiki> to run the final configuration with the
  [distribution
  wizard](https://www.xwiki.org/xwiki/bin/view/Documentation/UserGuide/Features/DistributionWizard).
    * When asked, install the XWiki Standard Flavor

---
## Use

### Tips

* If you want to migrate from confluence to XWiki:
  <https://xwiki.com/en/Blog/Confluence-to-XWiki-migration-export-import/>

* See all the shortcuts:
  <https://www.xwiki.org/xwiki/bin/view/Documentation/UserGuide/Features/KeyboardShortcuts>

#### Install XWiki plugins from source

???+ Note "Reference(s)"
    * <https://dev.xwiki.org/xwiki/bin/view/Community/Building/>
    * <https://dev.xwiki.org/xwiki/bin/view/Main/WebHome>

!!! Note "Prerequisite(s)"
    * [java 8](../dev/java/java.md)
    * [maven](../dev/java/maven.md)

For example, let's install [Diagram Application
(Pro)](https://github.com/xwikisas/application-diagram) from source:

* After installing [maven](../dev/java/maven.md), change your `$HOME/.config/maven/settings.xml`
  like so:
  ```console
  > <settings>
  >   <profiles>
  >     <profile>
  >       <id>xwiki</id>
  >       <repositories>
  >         <repository>
  >           <id>xwiki-snapshots</id>
  >           <name>XWiki Nexus Snapshot Repository Proxy</name>
  >           <url>https://nexus.xwiki.org/nexus/content/groups/public-snapshots</url>
  >           <releases>
  >             <enabled>false</enabled>
  >           </releases>
  >           <snapshots>
  >             <enabled>true</enabled>
  >           </snapshots>
  >         </repository>
  >         <repository>
  >           <id>xwiki-releases</id>
  >           <name>XWiki Nexus Releases Repository Proxy</name>
  >           <url>https://nexus.xwiki.org/nexus/content/groups/public</url>
  >           <releases>
  >             <enabled>true</enabled>
  >           </releases>
  >           <snapshots>
  >             <enabled>false</enabled>
  >           </snapshots>
  >         </repository>
  >       </repositories>
  >       <pluginRepositories>
  >         <pluginRepository>
  >           <id>xwiki-snapshots</id>
  >           <name>XWiki Nexus Plugin Snapshot Repository Proxy</name>
  >           <url>https://nexus.xwiki.org/nexus/content/groups/public-snapshots</url>
  >           <releases>
  >             <enabled>false</enabled>
  >           </releases>
  >           <snapshots>
  >             <enabled>true</enabled>
  >           </snapshots>
  >         </pluginRepository>
  >         <pluginRepository>
  >           <id>xwiki-releases</id>
  >           <name>XWiki Nexus Plugin Releases Repository Proxy</name>
  >           <url>https://nexus.xwiki.org/nexus/content/groups/public</url>
  >           <releases>
  >             <enabled>true</enabled>
  >           </releases>
  >           <snapshots>
  >             <enabled>false</enabled>
  >           </snapshots>
  >         </pluginRepository>
  >       </pluginRepositories>
  >     </profile>
  >   </profiles>
  >   <activeProfiles>
  >     <activeProfile>xwiki</activeProfile>
  >   </activeProfiles>
  > </settings>
  ```
  Note that you can, of course, tweak your `settings.xml` in order for it to include more settings
  like e.g. `<localRepository>${user.home}/.config/maven/.m2/repository</localRepository>`.

* Then, clone, build and install `Diagram Application (Pro)`:
  ```console
  $ mkdir -p $HOME/projects/xwiki-plugins
  $ cd $HOME/projects/xwiki-plugins
  $ git clone https://github.com/xwikisas/application-diagram.git
  $ git checkout application-diagram-1.16.2 # checkout to the lastest stable tag, e.g. 1.16.2 here
  $ mvn clean install
  ```

* After successfully building the plugin, you should find the following `.xar` file :
  `application-diagram-ui/target/application-diagram-1.16.2.xar`

* Execute the following steps:
    * Log into your XWiki instance with an admin user.
    * Click on the "Drawer" (very top right corner).
    * Click on "Administer Wiki".
    * Click on "Content".
    * Click on "Import".
    * Click on "Browse...".
    * Select the previously generated `.xar` file in the browser.
    * Select it again on the XWiki import page (under the "Available packages" section).
    * Click on "Import" (under the "Package Content" section).
    * Wait a moment and the plugin will be installed.

That's it the [Diagram Application (Pro)](https://github.com/xwikisas/application-diagram) plugin
should be installed and running.

**WIP**: not working with this particular plugin at the moment.


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
