---
tags:
  - Self-hosted
  - Search Engines
---

<!-- Load abbreviations -->
--8<-- ".abbreviations"

!!! Warning " "
    This document is a **WORK IN PROGRESS**.<br/>
    This is just a quick personal cheat sheet: treat its contents with caution!
---


# Searx

Searx is a free internet metasearch engine which aggregates results from more than 70 search
services. Users are neither tracked nor profiled. Additionally, Searx can be used over Tor for
online anonymity.

???+ Note "Reference(s)"
    * <https://searx.me/>
    * <https://searx.space/>
    * <https://github.com/searx/searx>
    * <https://searx.github.io/searx/>

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Config](#config)
* [Use](#use)

<!-- vim-markdown-toc -->

---
## Install

TODO

---
## Config

TODO

---
## Use

TODO


---

!!! Star " "
    If this cheat sheet has been useful to you, then please consider leaving a star
    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).
