#!/bin/sh

# check '-' instead of '_' in file name
check=$( \
      /bin/find ../docs -type f -name '*-*.md' \
    | /bin/grep -v "s-nail.md" \
    | /bin/grep -v "syslog-ng.md" \
    | /bin/grep -v "modprobed-db.md" \
    | /bin/grep -v "webapp-config.md" \
    | /bin/grep -v "systemd-timesyncd.md"
)
if [ -n "$check" ] ; then
    >&2 echo "❌ files name"
    >&2 echo ""
    >&2 echo "The following files name contain the '-' character (instead of '_'),"
    >&2 echo "please replace '-' by '_'."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ files name"
