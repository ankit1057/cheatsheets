#!/bin/sh

# check '*' instead of '-'
check=$( \
      /bin/grep -r '^\- ' ../docs \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
if [ -n "$check" ] ; then
    >&2 echo "❌ lists"
    >&2 echo ""
    >&2 echo "The following files contains at least one list item starting with '-' instead of '*',"
    >&2 echo "please prefer using '*'."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

# check that every lists is preceded by an empty line
check=$( \
      /bin/grep -r -B1 -e '^\- ' -e '^\* ' ../docs \
    | /bin/grep -v -e '\:\- ' -e '\:    \- ' -e '\:        \- ' -e '\:            \- ' \
    | /bin/grep -v -e '\:\- ' -e '\:                \- ' -e '\:                    \- ' \
    | /bin/grep -v -e '\:\* ' -e '\:    \* ' -e '\:        \* ' -e '\:            \* ' \
    | /bin/grep -v -e '\:\- ' -e '\:                \* ' -e '\:                    \* ' \
    | /bin/grep -v -e '\-\- ' -e '\-    \- ' -e '\-        \- ' -e '\-            \- ' \
    | /bin/grep -v -e '\-\- ' -e '\-                \- ' -e '\-                    \- ' \
    | /bin/grep -v -e '\-\* ' -e '\-    \* ' -e '\-        \* ' -e '\-            \* ' \
    | /bin/grep -v -e '\-\- ' -e '\-                \* ' -e '\-                    \* ' \
    | /bin/grep -v "\.md-$" \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
if [ -n "$check" ] ; then
    >&2 echo "❌ lists"
    >&2 echo ""
    >&2 echo "The following files contains at least one list that is not preceded by a blank line,"
    >&2 echo "please add that blank line in ordrer for the list to be correctly rendered."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ lists"
