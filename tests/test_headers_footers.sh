#!/bin/sh

# Check header
check1=$(/bin/grep -rL '^--8<-- ".abbreviations"$' --include="*.md" --exclude="tags.md" ../docs)
check2=$(/bin/grep -rL -F '!!! Warning " "' --include="*.md" --exclude="tags.md" ../docs)
check2=$(/bin/grep -rL -F '    This document is a **WORK IN PROGRESS**.<br/>' --include="*.md" --exclude="tags.md" ../docs)
check3=$(/bin/grep -rL -F '    This is just a quick personal cheat sheet: treat its contents with caution!' --include="*.md" --exclude="tags.md" ../docs)
check=$(
      printf "%s\n%s\n%s" "$check1" "$check2" "$check3" \
    | sort -u \
)
if [ -n "$check" ]; then
    >&2 echo "❌ headers and footers"
    >&2 echo ""
    >&2 echo "The following files do not conform to the expected header format: "
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    >&2 echo "The expected header format is:"
    >&2 echo ""
    >&2 echo "<!-- Load abbreviations -->"
    >&2 echo '--8<-- ".abbreviations"'
    >&2 echo ""
    >&2 echo "!!! Warning \" \""
    >&2 echo "    This document is a **WORK IN PROGRESS**.<br/>"
    >&2 echo "    This is just a quick personal cheat sheet: treat its contents with caution!"
    >&2 echo "---"
    >&2 echo ""
    return 1
fi

# Check footer
check1=$(/bin/grep -rL -F '!!! Star " "' --include="*.md" --exclude="tags.md" ../docs)
check1=$(/bin/grep -rL -F '    If this cheat sheet has been useful to you, then please consider leaving a star' --include="*.md" --exclude="tags.md" ../docs)
check2=$(/bin/grep -rL -F '    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/).' --include="*.md" --exclude="tags.md" ../docs)
check=$(
      printf "%s\n%s" "$check1" "$check2" \
    | sort -u \
)
if [ -n "$check" ]; then
    >&2 echo "❌ headers and footers"
    >&2 echo ""
    >&2 echo "The following files do not conform to the expected footer format: "
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    >&2 echo "The expected header format is:"
    >&2 echo ""
    >&2 echo "---"
    >&2 echo "!!! Star " ""
    >&2 echo "    If this cheat sheet has been useful to you, then please consider leaving a star"
    >&2 echo "    [here](https://gitlab.com/stephane.tzvetkov/cheatsheets/)."
    >&2 echo ""
    return 1
fi

echo "✅ headers and footers"
