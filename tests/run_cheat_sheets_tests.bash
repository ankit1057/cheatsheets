#!/bin/bash

################################################################################
# Safe shell settings:
######################
# see https://sipb.mit.edu/doc/safe-shell/
#set -e
set -euf -o pipefail

################################################################################
# Directory location of the current script:
###########################################
INIT_LOCATION="$(pwd)"
TESTS_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "$TESTS_DIR"

################################################################################
# Command line arguments:
#########################
# see https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
INSIDERS=false
POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    -i|--insiders)
      INSIDERS=true
      shift # past argument
      ;;
    *)      # unknown option
      POSITIONAL+=("$1") # save it in an array for later
      shift # past argument
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

################################################################################
# Count cheat sheets:
#####################
nb_cheat_sheets=$(find ../docs -type f -name '*.md' | wc -l)
echo ""
echo "(Current cheat sheets number: $nb_cheat_sheets)"
echo ""
echo ""

################################################################################
# Run tests:
############

#if [[ $(aspell --version) ]] # with GNU aspell
#then
#    /bin/sh test_spelling_mistakes.sh  # ✅ done with check_spelling_mistakes.py
#else
#    echo "❌  the aspell package is needed in order to check spelling mistakes,
#    but it is missing.
#    Please install it (see https://repology.org/project/aspell/versions)."
#    exit 1
#fi

/bin/sh test_files_name.sh  # ✅ done with check_files_name.py

/bin/sh test_directories_name.sh  # ✅ done with check_directories_name.py

/bin/sh test_bashrc.sh  # ✅ done with check_shell_configuration_references.py

/bin/sh test_avoid_dotfile_madness.sh  # ✅ done with check_dotfile_madness_references.py

/bin/sh test_headers_footers.sh  # ✅ done with check_headers_and_footers.py
/bin/sh test_main_sections.sh  # ✅ done with check_main_section.py
/bin/sh test_no_line_above_main_section.sh  # ✅ done with check_main_section.py
/bin/sh test_line_above_subsection.sh  # ✅ done with check_main_section.py
/bin/sh test_toc_sections.sh  # ✅ done with check_toc.py
/bin/sh test_paragraphs_consistency.sh

#/bin/sh test_admonitions.sh
#/bin/sh test_admonitions_indentations.sh
#/bin/sh test_empty_line_above_admonitions.sh

/bin/sh test_content_tabs_indentations.sh
/bin/sh test_empty_line_above_content_tabs.sh

/bin/sh test_references.sh
/bin/sh test_prerequisites.sh

/bin/sh test_lists.sh

/bin/sh test_warning_signs.sh

/bin/sh test_urls.sh
/bin/sh test_no_absolute_links.sh

poetry run pytest -vv -r a --docs-location="../docs" --dict-location="../custom_dict.txt"
poetry run pytest --black .
poetry run pytest --flake8 .
poetry run pytest --pylint .
poetry run pytest --pydocstyle
poetry run vulture .

if [[ $(linkchecker --version) ]] # whith linkchecker
then
    if [[ $INSIDERS = false ]]
    then
        /bin/bash test_links.sh
    else
        /bin/bash test_links_insiders.sh
    fi
else
    echo "❌  the linkchecker package is needed in order to check broken links,
    but it is missing.
    This issue is probably due to the fact that this script hasn't been run through poetry,
    e.g. like so '$ poetry run ./tests/run-cheat-sheets-tests.bash'
    (or with the '--insiders' option)."
    exit 1
fi

cd "$INIT_LOCATION"
