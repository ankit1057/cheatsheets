#!/bin/sh

# Check admonitions:
check=$( \
      /bin/grep -r -e '!!!' -e '???' ../docs \
    | /bin/grep -v \
                -e '!!! Note$' \
                -e '!!! Note ".*"$' \
                -e '??? Note$' \
                -e '??? Note ".*"$' \
                -e '???+ Note$' \
                -e '???+ Note ".*"$' \
    | /bin/grep -v \
                -e '!!! Abstract$' \
                -e '!!! Abstract ".*"$' \
                -e '??? Abstract$' \
                -e '??? Abstract ".*"$' \
                -e '???+ Abstract$' \
                -e '???+ Abstract ".*"$' \
    | /bin/grep -v \
                -e '!!! Summary$' \
                -e '!!! Summary ".*"$' \
                -e '??? Summary$' \
                -e '??? Summary ".*"$' \
                -e '???+ Summary$' \
                -e '???+ Summary ".*"$' \
    | /bin/grep -v \
                -e '!!! Tldr$' \
                -e '!!! Tldr ".*"$' \
                -e '??? Tldr$' \
                -e '??? Tldr ".*"$' \
                -e '???+ Tldr$' \
                -e '???+ Tldr ".*"$' \
    | /bin/grep -v \
                -e '!!! Info$' \
                -e '!!! Info ".*"$' \
                -e '??? Info$' \
                -e '??? Info ".*"$' \
                -e '???+ Info$' \
                -e '???+ Info ".*"$' \
    | /bin/grep -v \
                -e '!!! Todo$' \
                -e '!!! Todo ".*"$' \
                -e '??? Todo$' \
                -e '??? Todo ".*"$' \
                -e '???+ Todo$' \
                -e '???+ Todo ".*"$' \
    | /bin/grep -v \
                -e '!!! Tip$' \
                -e '!!! Tip ".*"$' \
                -e '??? Tip$' \
                -e '??? Tip ".*"$' \
                -e '???+ Tip$' \
                -e '???+ Tip ".*"$' \
    | /bin/grep -v \
                -e '!!! Hint$' \
                -e '!!! Hint ".*"$' \
                -e '??? Hint$' \
                -e '??? Hint ".*"$' \
                -e '???+ Hint$' \
                -e '???+ Hint ".*"$' \
    | /bin/grep -v \
                -e '!!! Important$' \
                -e '!!! Important ".*"$' \
                -e '??? Important$' \
                -e '??? Important ".*"$' \
                -e '???+ Important$' \
                -e '???+ Important ".*"$' \
    | /bin/grep -v \
                -e '!!! Success$' \
                -e '!!! Success ".*"$' \
                -e '??? Success$' \
                -e '??? Success ".*"$' \
                -e '???+ Success$' \
                -e '???+ Success ".*"$' \
    | /bin/grep -v \
                -e '!!! Check$' \
                -e '!!! Check ".*"$' \
                -e '??? Check$' \
                -e '??? Check ".*"$' \
                -e '???+ Check$' \
                -e '???+ Check ".*"$' \
    | /bin/grep -v \
                -e '!!! Done$' \
                -e '!!! Done ".*"$' \
                -e '??? Done$' \
                -e '??? Done ".*"$' \
                -e '???+ Done$' \
                -e '???+ Done ".*"$' \
    | /bin/grep -v \
                -e '!!! Question$' \
                -e '!!! Question ".*"$' \
                -e '??? Question$' \
                -e '??? Question ".*"$' \
                -e '???+ Question$' \
                -e '???+ Question ".*"$' \
    | /bin/grep -v \
                -e '!!! Help$' \
                -e '!!! Help ".*"$' \
                -e '??? Help$' \
                -e '??? Help ".*"$' \
                -e '???+ Help$' \
                -e '???+ Help ".*"$' \
    | /bin/grep -v \
                -e '!!! Faq$' \
                -e '!!! Faq ".*"$' \
                -e '??? Faq$' \
                -e '??? Faq ".*"$' \
                -e '???+ Faq$' \
                -e '???+ Faq ".*"$' \
    | /bin/grep -v \
                -e '!!! Warning$' \
                -e '!!! Warning ".*"$' \
                -e '??? Warning$' \
                -e '??? Warning ".*"$' \
                -e '???+ Warning$' \
                -e '???+ Warning ".*"$' \
    | /bin/grep -v \
                -e '!!! Caution$' \
                -e '!!! Caution ".*"$' \
                -e '??? Caution$' \
                -e '??? Caution ".*"$' \
                -e '???+ Caution$' \
                -e '???+ Caution ".*"$' \
    | /bin/grep -v \
                -e '!!! Attention$' \
                -e '!!! Attention ".*"$' \
                -e '??? Attention$' \
                -e '??? Attention ".*"$' \
                -e '???+ Attention$' \
                -e '???+ Attention ".*"$' \
    | /bin/grep -v \
                -e '!!! Failure$' \
                -e '!!! Failure ".*"$' \
                -e '??? Failure$' \
                -e '??? Failure ".*"$' \
                -e '???+ Failure$' \
                -e '???+ Failure ".*"$' \
    | /bin/grep -v \
                -e '!!! Fail$' \
                -e '!!! Fail ".*"$' \
                -e '??? Fail$' \
                -e '??? Fail ".*"$' \
                -e '???+ Fail$' \
                -e '???+ Fail ".*"$' \
    | /bin/grep -v \
                -e '!!! Missing$' \
                -e '!!! Missing ".*"$' \
                -e '??? Missing$' \
                -e '??? Missing ".*"$' \
                -e '???+ Missing$' \
                -e '???+ Missing ".*"$' \
    | /bin/grep -v \
                -e '!!! Danger$' \
                -e '!!! Danger ".*"$' \
                -e '??? Danger$' \
                -e '??? Danger ".*"$' \
                -e '???+ Danger$' \
                -e '???+ Danger ".*"$' \
    | /bin/grep -v \
                -e '!!! Error$' \
                -e '!!! Error ".*"$' \
                -e '??? Error$' \
                -e '??? Error ".*"$' \
                -e '???+ Error$' \
                -e '???+ Error ".*"$' \
    | /bin/grep -v \
                -e '!!! Bug$' \
                -e '!!! Bug ".*"$' \
                -e '??? Bug$' \
                -e '??? Bug ".*"$' \
                -e '???+ Bug$' \
                -e '???+ Bug ".*"$' \
    | /bin/grep -v \
                -e '!!! Example$' \
                -e '!!! Example ".*"$' \
                -e '??? Example$' \
                -e '??? Example ".*"$' \
                -e '???+ Example$' \
                -e '???+ Example ".*"$' \
    | /bin/grep -v \
                -e '!!! Quote$' \
                -e '!!! Quote ".*"$' \
                -e '??? Quote$' \
                -e '??? Quote ".*"$' \
                -e '???+ Quote$' \
                -e '???+ Quote ".*"$' \
    | /bin/grep -v \
                -e '!!! Cite$' \
                -e '!!! Cite ".*"$' \
                -e '??? Cite$' \
                -e '??? Cite ".*"$' \
                -e '???+ Cite$' \
                -e '???+ Cite ".*"$' \
    | /bin/grep -v \
                -e '!!! Star$' \
                -e '!!! Star ".*"$' \
                -e '??? Star$' \
                -e '??? Star ".*"$' \
                -e '???+ Star$' \
                -e '???+ Star ".*"$' \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
if [ -n "$check" ] ; then
    >&2 echo "❌ admonitions"
    >&2 echo ""
    >&2 echo "The following files do contain admonitions"
    >&2 echo "(see https://squidfunk.github.io/mkdocs-material/reference/admonitions) that are not"
    >&2 echo "formated correctly."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ admonitions"
