"""
"""

import logging
import os

from rich.logging import RichHandler

FORMAT = "%(message)s"
# FORMAT = "[%(levelname)s] [%(asctime)s] %(module)s.%(funcName)s(): %(message)s"
# FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"
logging.basicConfig(
    level="NOTSET", format=FORMAT, datefmt="[%X]", handlers=[RichHandler()]
)  # set level=20 or logging.INFO to turn of debug
log = logging.getLogger("rich")

# see https://rich.readthedocs.io/en/stable/logging.html
# log.debug("[bold cyan]debug...[/bold cyan]", extra={"markup": True})
# log.info("info...")
# log.warning("warning...")
# log.error("error...")
# log.fatal("fatal...")


def find_markdown_files(path: str, extension: str) -> list[str]:
    """
    """
    markdown_files: list[str] = []
    for root, dirs, files in os.walk(path):
        for f in files:
            if f.endswith(extension):
                markdown_files.append(root + "/" + f)

    if not markdown_files:
        log.error(f"There is no markdown files at this path: {path}")
    assert markdown_files

    return markdown_files


def find_directories(path: str) -> list[str]:
    """
    """
    directories: list[str] = []
    for path, dirs, files in os.walk(path):
        if dirs:
            directories.extend(dirs)

    if not directories:
        log.error(f"There is no directory at this path: {path}")
    assert directories

    return directories
