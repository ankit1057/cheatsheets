from tests.utils import log, find_markdown_files

EXPECTED_REF_1: str = '''
$HOME/.bashrc # or ${BDOTDIR:-${HOME/.config/bdotdir}}/.bashrc or wherever
'''
EXPECTED_REF_2: str = '''
$HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever
'''
EXPECTED_REF_3: str = '''
$HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever
'''

def check_shell_configuration_references(docs_location: str):
    md_files_path: list = find_markdown_files(docs_location, ".md")

    files_containing_bad_shell_config_reference: list = []
    for file_path in md_files_path:
        with open(file_path, "r") as file:
            file_content = file.read()
            if ".bashrc" in file_content \
               and EXPECTED_REF_1 not in file_content \
               and EXPECTED_REF_2 not in file_content \
               and EXPECTED_REF_3 not in file_content:
                files_containing_bad_shell_config_reference.append(file_path)

    if files_containing_bad_shell_config_reference:
        log.error("The following files are not referencing the `.bashrc` file as expected:" \
                  f"\n{files_containing_bad_shell_config_reference}")
        log.error("The expected reference is " \
                  f"\n`{EXPECTED_REF_1}`" \
                  "\nor" \
                  f"\n`{EXPECTED_REF_2}`" \
                  "\nor" \
                  f"\n`{EXPECTED_REF_3}`" \
                  "\n\nPlease fix the `.bashrc` references in those files.\n")

    assert not files_containing_bad_shell_config_reference

def check():
    log.debug("Checking shell configuration references...")
    check_shell_configuration_references("./docs")
    log.info("Shell configuration references checked!")

if __name__=="__main__":
    check()
