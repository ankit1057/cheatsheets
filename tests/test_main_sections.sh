#!/bin/sh

# Check every cheat sheet has at least one main section '# ':
check=$(/bin/grep -rL '^# ' --include="*.md" ../docs)
if [ -n "$check" ] ; then
    >&2 echo "❌ main sections"
    >&2 echo ""
    >&2 echo "The following files do not contain a main section ('# '),"
    >&2 echo "please add that section."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ main sections"
