#!/bin/sh

# Check no separating line (`---`) directly above main section ('# '):
check=$( \
      /bin/grep -r --no-group-separator -B1 "^# " ../docs \
    | /bin/grep -v "# " \
    | /bin/grep "\.md----$" \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
if [ -n "$check" ] ; then
    >&2 echo "❌ no line above main sections"
    >&2 echo ""
    >&2 echo "The following files do contain a separating line ('---')"
    >&2 echo "directly above a main section ('# '), please remove that separating line."
    >&2 echo "(this line should be place below the file header, not above the file main section)"
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ no line above main sections"
