import os
import re

from tests.utils import log, find_markdown_files

# See https://regex101.com/r/9JGYay/2 for a detailed explanation of the below regex
REGEX_MATCHING_TAGS = r"""^\-\-\-$
^tags\:$
(?:^\ \ \-\ .*$\n)+^\-\-\-$
^$
"""

EXPECTED_TAGS_REFERENCE = """
---⏎
tags:⏎
  - Your tag name⏎
  - One more tag name⏎
  - Add as many tags as you want⏎
---⏎
⏎
"""

allowed_files_without_tags: list[str] = [
    "index.md",
    "tags.md",
]

def check_tags(docs_location: str):
    md_files_path: list = find_markdown_files(docs_location, ".md")

    files_containing_bad_tags: list = []
    for file_path in md_files_path:
        file_basename: str = os.path.basename(file_path)
        if file_basename in allowed_files_without_tags :
            continue  # skip files that do not require a header and a footer
        with open(file_path, "r", encoding="utf-8") as file:

            file_content = file.read()
            p = re.findall(REGEX_MATCHING_TAGS, file_content, re.MULTILINE)

            lines: list[str] = file_content.splitlines()
            tags_at_the_top_of_the_file: bool = False
            prev_line: str = "/"
            i: int = 0
            for current_line in lines :
                if i >= 1 :
                    prev_line = lines[i-1]
                if (current_line == "tags:"
                   and prev_line.startswith("---")
                   and i == 1) :
                    tags_at_the_top_of_the_file = True
                    break
                i += 1

            # "good" tags if:
            # - the regex is matched
            # - the very first line of the file is `---⏎`
            # - the second line of the file is  tags:⏎`
            if not p or not tags_at_the_top_of_the_file :
                files_containing_bad_tags.append(file_path)

    if files_containing_bad_tags :
        log.error("The following files do not reference tags with the expected format: " \
                  f"\n{files_containing_bad_tags}")
        log.error("The expected tags reference format (note that line returns matter) " \
                  f"is: \n```{EXPECTED_TAGS_REFERENCE}```\n")
        log.error("Note that a file MUST reference a least one tag, and that tags MUST be " \
                  "referenced at the very top of a file.")

    assert not files_containing_bad_tags

def check():
    log.debug("Checking tags...")
    check_tags("./docs")
    log.info("Tags checked!")

if __name__=="__main__":
    check()
