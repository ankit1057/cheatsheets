#!/bin/sh

check=$(
      /bin/grep -ri '\.bashrc' --exclude="xdg.md" ../docs \
    | /bin/grep -v -F '`$HOME/.bashrc`' \
    | /bin/grep -v -F '$HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever' \
    | /bin/grep -v -F '$HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever' \
    | /bin/grep -o "^\S*\.md" \
    | sort -u \
)
if [ -n "$check" ] ; then
    >&2 echo "❌ shell configuration file references"
    >&2 echo ""
    >&2 echo "The following files are not referencing the `.bashrc` file as wanted, the expected"
    >&2 echo 'reference is `$HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshrc or wherever`'
    >&2 echo 'or           `$HOME/.bashrc # or ${ZDOTDIR:-${HOME}}/.zshenv or wherever`'
    >&2 echo "Please fix those references."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ shell configuration file references"
