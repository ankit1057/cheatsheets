import re

from tests.utils import log, find_markdown_files

import spelchek

is_ignoring: bool = False
is_super_ignoring: bool = False

toggle_super_ignore: str = "```"
toggle_ignore: str = "`"

# start_ignore: list[str] = ["<!--"] # "<"
# stop_ignore: list[str]  = ["-->"] # "/>"

special_strings_to_ignore: list[str] = [
    "-",
    "--",
    "==",
    "--8",
    "br",
    "br/",
    "vim-markdown-toc",
    "t",
    "⚠️",
    "🚧",
    "⭐️",
    "→",
]


def is_toggle_ignore(word: str) -> bool:
    count: int = word.count(toggle_ignore)
    if (count % 2) == 0:
        return False
    else:
        return True


def apply_syntax_format(line: str) -> str:

    # replace markdown links, by link text:
    # e.g. `[link text](link path/url)` -> `link text`
    line = re.sub(r"\]\((.+)\)", r"", line)
    # line = re.sub(r'\[(.+)\]\((.+)\)', r'\1', line)

    # replace URLs by nothing
    # e.g. `https://url.url` -> ``
    line = re.sub(r"http://\S+", r"", line)
    line = re.sub(r"https://\S+", r"", line)

    ## replace start_ignore delimiters by toggle_ignore
    ## e.g. `<!--` -> ```
    # for starti in start_ignore:
    #     line = line.replace(starti, " "+toggle_ignore+" ")

    ## replace stop_ignore delimiters by toggle_ignore
    ## e.g. `-->` -> ```
    # for stopi in stop_ignore:
    #     line = line.replace(stopi, " "+toggle_ignore+" ")

    # lower all characters
    line = line.lower()

    # replace all line-return characters by nothing
    line = line.replace("\n", "")

    # replace ():;,.*#|!?+%~ by nothing
    line = re.sub(r"[\(\)\:\;\,\.\*\#\|\!\?\+\%\~]", "", line)

    # replace []<>'’/_"“ by a single space
    line = re.sub(r"[\[\]\<\>\'\’\/\_\"]", " ", line)

    # repalce === by nothing and repalce --- by nothing
    line = re.sub(r"\=\=\=", "", line)
    line = re.sub(r"\-\-\-", "", line)

    # replace word-number by a single space
    line = re.sub(r"\s[0-9]+\s", " ", line)
    line = re.sub(r"\s[0-9]+$", " ", line)
    line = re.sub(r"^[0-9]+\s", " ", line)

    # add a single space before and after toggle_super_ignore
    line = line.replace(toggle_super_ignore, " " + toggle_super_ignore + " ")

    return line


def contains_a_spelling_mistake(line: str, line_nb: int, file_path: str) -> bool:
    global is_ignoring
    global is_super_ignoring

    line = apply_syntax_format(line)
    words = line.split(" ")

    has_spelling_mistake: bool = False
    for word in words:

        if toggle_super_ignore in word:
            is_super_ignoring = not is_super_ignoring
        elif toggle_ignore in word:
            is_ignoring = is_toggle_ignore(word)

        if (
            not word
            or is_super_ignoring
            or is_ignoring
            or word in special_strings_to_ignore
            or word.startswith(toggle_ignore)
            and word.endswith(toggle_ignore)
        ):
            pass
        else:
            known = spelchek.known(word)
            if not known:
                log.error(
                    f"The word '{word}' is not correct or not known\nfile {file_path}, "
                    + f"line {line_nb}"
                )
                # log.error(f"guesses: {spelchek.guesses(word)}")
                has_spelling_mistake = True
            else:
                has_spelling_mistake = False

    return has_spelling_mistake

def add_custom_dict(dict_location: str):
    with open(dict_location, "rt") as new_dict:
        spelchek.update_dictionary(new_dict)


def test_spelling_mistakes(docs_location, dict_location):
    global is_ignoring
    global is_super_ignoring
    has_spelling_mistake: bool = False
    add_custom_dict(dict_location)
    md_files_path: list = find_markdown_files(docs_location, ".md")
    for file_path in md_files_path:
        with open(file_path, "r") as file:
            is_super_ignoring = False
            is_ignoring = False
            line_nb: int = 0
            for line in file:
                line_nb += 1
                if contains_a_spelling_mistake(line, line_nb, file_path):
                    has_spelling_mistake = True
    assert not has_spelling_mistake


def check():
    log.debug("Checking spelling mistakes...")
    test_spelling_mistakes("./docs", "./custom_dict.txt")
    log.info("Spelling mistakes checked!")


if __name__=="__main__":
    check()
