
from tests.utils import log, find_markdown_files

MAX_LINE_LENGTH_IN_PARAGRAPH = 100

EXPECTEED_PARAGRAPH_INDENTATION = '''
⏎
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt Ut
    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
    laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
    voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
    non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.⏎
⏎
'''

def check_paragraphs(docs_location: str):
    md_files_path: list = find_markdown_files(docs_location, ".md")

    files_containing_bad_paragraph: list = []
    for file_path in md_files_path:
        with open(file_path, "r") as file:
            file_content = file.read()

            lines: list[str] = file_content.splitlines()
            good_main_section: bool = False
            prev_line: str = "/"
            prev_prev_line: str = "/"
            prev_prev_prev_line: str = "/"
            i: int = 0
            for current_line in lines :
                if i >= 1 :
                    prev_line = lines[i-1]
                if i >= 2 :
                    prev_prev_line = lines[i-2]
                if i >= 3 :
                    prev_prev_prev_line = lines[i-3]

                if (current_line == ""
                   and prev_line.startswith("# ")
                   and prev_prev_line == ""
                   and prev_prev_prev_line == "") :
                    # "good" main section if:
                    # - current line is a line return after the main section
                    # - prev line is the main section declaration
                    # - prev prev line is a line return before the main section
                    # - prev prev prev line is a line return before the main section
                    good_main_section = True
                    break

                i += 1

            if not good_main_section :
                files_containing_bad_paragraph.append(file_path)

    if files_containing_bad_paragraph:
        log.error("The following files do not have a main section with the expected format " \
                  f"(or no main section at all):\n{files_containing_bad_paragraph}")
        log.error("The expected main section declaration format (note that line returns matter) " \
                  f"is: \n```{EXPECTEED_PARAGRAPH_INDENTATION}```\n")

    assert not files_containing_bad_paragraph

def check():
    log.debug("Checking paragraphs format...")
    check_paragraphs("./docs")
    log.info("Paragraph format checked!")

if __name__=="__main__":
    check()
