#!/bin/sh

# Check every cheat sheet has a TOC:
check=$(/bin/grep -rL 'vim-markdown-toc' --include="*.md" --exclude="tags.md" ../docs)
if [ -n "$check" ] ; then
    >&2 echo "❌ TOC sections"
    >&2 echo ""
    >&2 echo "The following files do not contain a table of contents,"
    >&2 echo "please add that TOC."
    >&2 echo ""
    >&2 echo "$check"
    >&2 echo ""
    return 1
fi

echo "✅ TOC sections"
