#!/bin/sh

check1=$( \
      /bin/grep -n -r --no-group-separator -A1 -e '^!!!' -e '^???' ./docs \
    | /bin/grep -v -e '!!!' -e '???' \
    | /bin/grep -v "\-$" \
    | /bin/grep -v "\-    \S"
)
check2=$( \
      /bin/grep -n -r --no-group-separator -A1 -e '^    !!!' -e '^    ???' ./docs \
    | /bin/grep -v -e '!!!' -e '???' \
    | /bin/grep -v "\-$" \
    | /bin/grep -v "\-        \S"
)
check3=$( \
      /bin/grep -n -r --no-group-separator -A1 -e '^        !!!' -e '^        ???' ./docs \
    | /bin/grep -v -e '!!!' -e '???' \
    | /bin/grep -v "\-$" \
    | /bin/grep -v "\-            \S"
)
check4=$( \
      /bin/grep -n -r --no-group-separator -A1 -e '^            !!!' -e '^            ???' ./docs \
    | /bin/grep -v -e '!!!' -e '???' \
    | /bin/grep -v "\-$" \
    | /bin/grep -v "\-                \S"
)
check5=$( \
      /bin/grep -n -r --no-group-separator -A1 -e '^                !!!' -e '^                ???' ./docs \
    | /bin/grep -v -e '!!!' -e '???' \
    | /bin/grep -v "\-$" \
    | /bin/grep -v "\-                    \S"
)


if [ -n "$check1" ]; then
    echo "$check1"
fi
if [ -n "$check2" ]; then
    echo "$check2"
fi
if [ -n "$check3" ]; then
    echo "$check3"
fi
if [ -n "$check4" ]; then
    echo "$check3"
fi
if [ -n "$check5" ]; then
    echo "$check5"
fi
