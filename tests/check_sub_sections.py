from tests.utils import log, find_markdown_files

expected_sub_section_declaration = '''

---
## The title of your sub section here

'''

def check_sub_sections(docs_location: str):
    md_files_path: list = find_markdown_files(docs_location, ".md")

    files_containing_bad_sub_section: list = []
    for file_path in md_files_path:
        with open(file_path, "r") as file:
            file_content = file.read()

            lines: list[str] = file_content.splitlines()
            good_sub_section: bool = False
            prev_line: str = "/"
            prev_prev_line: str = "/"
            prev_prev_prev_line: str = "/"
            i: int = 0
            for current_line in lines :
                if i >= 1 :
                    prev_line = lines[i-1]
                if i >= 2 :
                    prev_prev_line = lines[i-2]
                if i >= 3 :
                    prev_prev_prev_line = lines[i-3]

                if (current_line == ""
                   and prev_line.startswith("## ")
                   and prev_prev_line == "---"
                   and prev_prev_prev_line == "") :
                    # "good" sub section if:
                    # - current line is a line return after the sub section
                    # - prev line is the sub section declaration
                    # - prev prev line is a line separator before the sub section
                    # - prev prev prev line is a line return before the sub section
                    good_sub_section = True
                    break

                i += 1

            if not good_sub_section :
                files_containing_bad_sub_section.append(file_path)

    if files_containing_bad_sub_section:
        log.error("The following files do not have sub sections with the expected format " \
                  f"(or no sub sections at all):\n{files_containing_bad_sub_section}")
        log.error("The expected sub section declaration format (note that line returns matter) " \
                  f"is: \n```{expected_sub_section_declaration}```\n")

    assert not files_containing_bad_sub_section

def check():
    log.debug("Checking sub sections...")
    check_sub_sections("./docs")
    log.info("Sub sections checked!")

if __name__=="__main__":
    check()
