"""
Check Table Of Contents (TOC) formatting and integration
"""

import re

from tests.utils import find_markdown_files, log

# See https://regex101.com/r/9JGYay/2 for a detailed explanation of the below regex

REGEX_MATCHING_MAIN_SECTION_BEFORE_TOC = r"""
(?:^.*$\n)*(?#match anything on multiple lines)
(?=^\# .*$)^.*$(?#match section title)
(?:^.*$\n)*(?#match anything on multiple lines)
(?!^\#.*$)^.*$(?#make sure to not match any section between title and TOC)
^\-\-\-$
^\#\#\ Table\ of\ contents$
^$
^(?:<!\-\-\ vim\-markdown\-toc\ (?:GitLab\ )?\-\->)$
"""

REGEX_MATCHING_TOC = r"""
^$
^$
^\-\-\-$
^\#\#\ Table\ of\ contents$
^$
(?:^<!\-\-\ vim\-markdown\-toc\ GitLab\ \-\->$)
^$
(?:^\ *\*\ \[.*\]\(\#.*\)$\n)+
^<!\-\-\ vim\-markdown\-toc\ \-\->$
^$
^$
"""

REGEX_MATCHING_SUB_SECTION_AFTER_TOC = r"""
^$
^<!\-\-\ vim\-markdown\-toc\ \-\->$
^$
^$
^\-\-\-$
^\#\#\ .*$
^$
"""

FIRST_EXPECTED_TOC_DECLARATION_EXAMPLE = """
⏎
⏎
---⏎
## Table of contents⏎
⏎
<!-- vim-markdown-toc -->⏎
⏎
* [section name](#section-name)⏎
    * [first sub section name](#first-sub-section-name)⏎
    * [other sub section name](#other-sub-section-name)⏎
⏎
<!-- vim-markdown-toc -->⏎
⏎
⏎
"""

SECOND_EXPECTED_TOC_DECLARATION_EXAMPLE = """
⏎
⏎
---⏎
## Table of contents⏎
⏎
<!-- vim-markdown-toc Gitlab -->⏎
⏎
* [section name](#section-name)⏎
    * [first sub section name](#first-sub-section-name)⏎
    * [other sub section name](#other-sub-section-name)⏎
⏎
<!-- vim-markdown-toc -->⏎
⏎
⏎
"""


def check_toc(docs_location: str) -> None:
    """
    """
    md_files_path: list[str] = find_markdown_files(docs_location, ".md")

    files_containing_bad_toc: list[str] = []
    for file_path in md_files_path:
        with open(file_path, "r", encoding="utf-8") as file:

            file_content = file.read()
            # p = re.findall(regex_to_search, file_content, re.MULTILINE)
            # p = re.findall(complex_regex_to_search, file_content, re.MULTILINE)
            p = re.findall(REGEX_MATCHING_MAIN_SECTION_BEFORE_TOC, file_content, re.MULTILINE)
            if not p:
                files_containing_bad_toc.append(file_path)

            p = re.findall(REGEX_MATCHING_TOC, file_content, re.MULTILINE)
            if not p:
                files_containing_bad_toc.append(file_path)

            p = re.findall(REGEX_MATCHING_SUB_SECTION_AFTER_TOC, file_content, re.MULTILINE)
            if not p:
                files_containing_bad_toc.append(file_path)

    if files_containing_bad_toc:

        log.error("The following files do not conform to the expected TOC format: " \
                  f"format: \n{files_containing_bad_toc}")
        log.error("The expected TOC declaration format (note that line " \
                  "returns matter) is e.g.:" \
                  f"[green]{FIRST_EXPECTED_TOC_DECLARATION_EXAMPLE}[/green]",
                  extra={"markup": True, "highlighter": None})

    assert not files_containing_bad_toc

    # TODO: make sure the sections annonced in TOC are present in .md


def check() -> None:
    """
    """
    log.debug("Checking TOC...")
    check_toc("./tests/markdown-tests-files")
    log.info("TOC checked!")


if __name__ == "__main__":
    check()
